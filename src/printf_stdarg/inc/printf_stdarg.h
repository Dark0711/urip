#ifndef __PRINTF_STDARG_H_
#define __PRINTF_STDARG_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdarg.h>

int sprintf(char *out, const char *format, ...);
int snprintf( char *buf, unsigned int count, const char *format, ... );
//int printf(const char *format, ...);

int __print( char **out, const char *format, va_list args );


#ifdef __cplusplus
}
#endif

#endif
