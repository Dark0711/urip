/*
 * usb_addition.h
 *
 *  ������: 16 ���. 2015 �.
 *  �����:  �������� �.�
 */

#ifndef FATFS_COMMON_USB_ADDITION_H_
#define FATFS_COMMON_USB_ADDITION_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "diskio.h"
#include <stdint.h>

	void USB_TaskInit();

	void USB_disk_initialize();
	int USB_disk_read(void *buff, uint32_t secStart, uint32_t numSec);
	int USB_disk_write(void *buff, uint32_t secStart, uint32_t numSec);
	DRESULT USB_diskioctl(uint8_t cmd, void *buff);

#ifdef USB_TARCE
extern int usb_trace_mask;
enum
{
	USB_TRACE_MESSAGE = 1 << 0,
	USB_TRACE_ERROR = 2 << 0,
};;


#define  usb_trace(msk, fmt, ...) do { \
				if (usb_trace_mask & (msk)) \
				Debug_printf("usb: " fmt /*"\n"*/, ##__VA_ARGS__); \
						} while (0)



#else
#define  usb_trace(msk, fmt, ...)
#endif

#ifdef __cplusplus
}
#endif

#endif /* FATFS_COMMON_USB_ADDITION_H_ */
