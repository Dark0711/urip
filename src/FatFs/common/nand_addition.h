/*
 * nand_addition.h
 *
 *  ������: 27 ���. 2015 �.
 *  �����:  �������� �.�
 */

#ifndef FATFS_COMMON_NAND_ADDITION_H_
#define FATFS_COMMON_NAND_ADDITION_H_

#include "cnand.h"
#include "diskio.h"

#ifdef __cplusplus
extern "C" {
#endif


	cnand_dev* NAND_disk_initialize(uint32_t startblock, uint32_t endblock, uint32_t reserveblock);
	DRESULT NAND_diskioctl(cnand_dev* dev,uint8_t cmd, void *buff);
	//void NAND_Init();

// #ifndef WIN32
// #define NAND_Init() extern FATFS NAND_fs; \
// 	f_mount(&NAND_fs,"NAND:",1)
// 
// #define NANDRESORCE_Init() extern FATFS NANDRESORCE_fs; \
// 	f_mount(&NANDRESORCE_fs,"NANDRESORCE:",1)
// #else
// 
// #endif

#define NAND_Init() f_mount(&NAND_fs, "NAND:", 1)
#define NANDRESORCE_Init() f_mount(&NANDRESORCE_fs,"NANDRESORCE:",1)


#ifdef __cplusplus
}
#endif

#endif /* FATFS_COMMON_NAND_ADDITION_H_ */
