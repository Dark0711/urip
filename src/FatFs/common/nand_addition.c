/*
 * nand_addition.c
 *
 *  ������: 27 ���. 2015 �.
 *  �����:  �������� �.�
 */
#include "nand_addition.h"
#ifndef WIN32
#include "nandflash.h"
#else
#include "emulation_nandflash.h"
#endif
//#include "tlsf.h"
#include "diskio.h"
#include <string.h>
#include "TimeSync.h"
#include "stdlib.h"
#include "ff.h"

FATFS NAND_fs;
FATFS NANDRESORCE_fs;


cnand_dev* NAND_disk_initialize(uint32_t startblock, uint32_t endblock, uint32_t reserveblock)
{
	cnand_dev* dev = NULL;
	if (dev == NULL)
	{
		Init_nandflash();
		dev = calloc(1, sizeof(cnand_dev));
		//dev = (cnand_dev*)(malloc(sizeof(cnand_dev)));
		if (dev == NULL) return NULL;
		//memset(dev, 0, sizeof(cnand_dev));
		//dev->drv.drv_crc_fn = CRC_CalcBlockChecksum;
		dev->drv.drv_erase_fn = NandFlash_BlockErase;
		dev->drv.drv_read_page_fn = NandFlash_PageRead;
		dev->drv.drv_write_page_fn = NandFlash_PageProgram;

		dev->param.startblock = startblock;
		dev->param.reserveblock = reserveblock;
		dev->param.endblock =endblock;
		dev->param.pagesize = NANDFLASH_RW_PAGE_SIZE;
		dev->param.pagesparesize = NANDFLASH_SPARE_SIZE;
		dev->param.pageperblock = NANDFLASH_PAGE_PER_BLOCK;
		dev->param.clustersize = NANDFLASH_RW_PAGE_SIZE;
		init_crutch_nand(dev);
		return dev;
	}
	return NULL;
}

DRESULT NAND_diskioctl(cnand_dev* dev, uint8_t cmd, void *buff)
{
	DRESULT res = RES_ERROR;
	switch (cmd)
	{
		case CTRL_SYNC: /* Make sure that no pending write process */
		{
			//vTaskDelay(50);

			cnand_state resultnand = crutch_sync(dev);
			if (resultnand == CNAND_STATE_OK)
			{
				return RES_OK;
			}
			else if (resultnand == CNAND_STATE_ERROR_NOINIT)
			{
				return RES_NOTRDY;
			}
			else if (resultnand & (CNAND_STATE_ERROR_NOTALLOCATIONBLOCK | CNAND_STATE_ERROR_BADBLOCK))
			{
				return RES_ERROR;
			}
			res = RES_ERROR;
			break;
		}
		case GET_SECTOR_COUNT: /* Get number of sectors on the disk (DWORD) */
		{
			*(DWORD *) buff = dev->cacheblock.sectorinblock * (dev->param.numberblockfortable - dev->param.reserveblock);
			res = RES_OK;
			break;
		}
		case GET_SECTOR_SIZE: /* Get R/W sector size (WORD) */
		{
			*(WORD *) buff = dev->param.clustersize;
			res = RES_OK;
			break;
		}
		case GET_BLOCK_SIZE:/* Get erase block size in unit of sector (DWORD) */
		{
			*(DWORD *) buff = 1;
			res = RES_OK;
			break;
		}
		default:
			res = RES_PARERR;
			break;
	}
	return res;
}

