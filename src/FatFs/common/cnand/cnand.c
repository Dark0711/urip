#include "cnand.h"
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#ifndef WIN32
#include "debug.h"
#endif

static uint16_t crutch_nand_searchemptyblock(cnand_dev* dev);

int init_crutch_nand(cnand_dev* dev)
{
	cnand_trace(CNAND_TRACE_INIT, "�������������\n");
	if (dev->devstate != CNAND_DEV_STATE_NOINIT) return true;
	CNAND_CREATE_LOCK(dev->cnandmutex);
	//�������������� ������ ��� �������� ������
	if (dev->drv.drv_erase_fn == NULL || dev->drv.drv_read_page_fn == NULL || dev->drv.drv_write_page_fn == NULL)
	{
		return false;
	}
	if (dev->param.endblock < dev->param.startblock || (dev->param.endblock - dev->param.startblock) < 5)
	{
		return false;
	}
	dev->param.fullpagesize = dev->param.pagesize + dev->param.pagesparesize;
	dev->param.numberblockfortable = dev->param.endblock - dev->param.startblock;//���������� ������
	int sizeblock = (dev->param.fullpagesize)*dev->param.pageperblock;//������ ����� � ������
	//�������� ������ ��� ������ ���
	dev->statusblock = (cnand_block_state*)cnand_calloc(dev->param.numberblockfortable, sizeof(cnand_block_state));
	dev->overwrites = (uint32_t*)cnand_calloc(dev->param.numberblockfortable, sizeof(uint32_t));
	dev->cacheblock.cacheblock = (uint8_t*)cnand_calloc(sizeblock, sizeof(uint8_t));
	dev->blocktable = (uint16_t*)cnand_calloc(dev->param.numberblockfortable, sizeof(uint16_t));
	//dev->cacheblock.cacheblock = cnand_calloc(dev->param.fullpagesize, sizeof(uint8_t));
	//���������
	if (dev->statusblock == NULL || dev->overwrites == NULL || dev->cacheblock.cacheblock == NULL || dev->blocktable == NULL)
	{
		return false;
	}
	cnand_title* titleblock;//��������� ��������� �����
	cnand_searchsystemblock tempsystemblock = { 0, 0, 0 };//��������� ��� ���������� ���������� �����
	size_t flagreset = -1;
	//�������� ������ ��� ������� ������

	//���������� ������� ������� �������� ��������� ���������
	uint32_t byteforsystemteble = sizeof(cnand_title) + (sizeof(uint16_t)*(dev->param.numberblockfortable)); //���������� ���� ��� ��������� ���������
	uint32_t pageforforsystemteble = (uint32_t)(ceil(byteforsystemteble / (double)(dev->param.fullpagesize)));//���������� ������� ��� ��������� ���������
	dev->cacheblock.sectorforsystemtable = (uint32_t)(ceil(byteforsystemteble / (double)dev->param.clustersize));//���������� �������� ��� ��������� ���������
	//���������� ���������� ��������� LBA �������� � �����
	dev->cacheblock.sectorinblock = (sizeblock / dev->param.clustersize) - dev->cacheblock.sectorforsystemtable; //��� ��������� ������� 
	dev->cacheblock.currentblock = CNAND_ERROR_BLOCKNUM; //������� ������� ����

	//��������� ��� ������ NAND
	cnand_trace(CNAND_TRACE_INIT, "�������������: ������������ ��� �����\n");
	while (1)
	{
		flagreset++;
		//for (uint32_t i = dev->param.startblock; i < dev->param.endblock; i++)
		for (uint32_t i = 0; i < dev->param.numberblockfortable; i++)
		{
			CNAND_ACQUIRE_LOCK(dev->cnandmutex);
			if (dev->drv.drv_read_page_fn(i + dev->param.startblock, 0, dev->cacheblock.cacheblock)) //���� �������
			{
				CNAND_RELEASE_LOCK(dev->cnandmutex);
				if ((dev->statusblock[i] != CNAND_BLOCK_STATE_DEAD) && (dev->statusblock[i] != CNAND_BLOCK_STATE_BADSYSTEM) && (dev->statusblock[i] != CNAND_BLOCK_STATE_EMPTY))
				{
					//������� � �������� ������
					titleblock = (cnand_title*)(dev->cacheblock.cacheblock);
					if (titleblock->idblock == IDBLOCK) //��������
					{
						dev->overwrites[i] = titleblock->overwrites; //���������� ����������� �����
						if (tempsystemblock.createcounter == (UINT32_MAX - dev->param.numberblockfortable - 1) && flagreset == 0) //������� ��� ���� ������� �� ����, �� �������� ����� � ������
						{
							tempsystemblock.createcounteradd = (UINT32_MAX - dev->param.numberblockfortable);
							tempsystemblock.createcounter = 0;
							flagreset++;
							cnand_trace(CNAND_TRACE_INIT, "�������������: c������ ����� ������������ ��������, ��������� �����\n");
							break;
						}
						//������� ����� � ����� ����� �� �� ������
						if (tempsystemblock.createcounter < ((titleblock->createcounter < dev->param.numberblockfortable) ? (titleblock->createcounter + tempsystemblock.createcounteradd) : titleblock->createcounter))
						{
							tempsystemblock.createcounter = ((titleblock->createcounter < dev->param.numberblockfortable) ? (titleblock->createcounter + tempsystemblock.createcounteradd) : titleblock->createcounter);
							tempsystemblock.currentblock = i;
						}
						cnand_trace(CNAND_TRACE_INIT, "�������������: ���� �%u - ��������� ������� %u ������������ %u \n", i, titleblock->createcounter, (uint32_t)(tempsystemblock.createcounter));
						dev->statusblock[i] = CNAND_BLOCK_STATE_SCANNING; //������� ��� �������������
					}
					else
					{
						cnand_trace(CNAND_TRACE_INIT_EMPTYBLOCK, "�������������: ���� �%u - ������\n", i);
						dev->statusblock[i] = CNAND_BLOCK_STATE_EMPTY; //��� ������, � crc �� �������, ���� ���� � ������ � ������� �� ������.
						dev->blocktable[i] = CNAND_ERROR_BLOCKNUM;
					}
				}
			}
			else
			{
				CNAND_RELEASE_LOCK(dev->cnandmutex);
				cnand_trace(CNAND_TRACE_INIT, "�������������: ���� �%u - �������\n", i);
				dev->statusblock[i] = CNAND_BLOCK_STATE_DEAD;//����� ����
			}
		}
		if (flagreset == true) continue;
		//memset(dev->blocktable, CNAND_ERROR_BLOCKNUM, sizeof(uint16_t)*(dev->param.numberblockfortable));//������
		memset(dev->cacheblock.cacheblock, 0, dev->param.fullpagesize);//������
		//����� ���� ��� ��������� ��� ����� ������ ����� �����
		if (tempsystemblock.createcounter != 0)
		{
			tempsystemblock.createcounter = tempsystemblock.createcounter >= (UINT32_MAX - dev->param.numberblockfortable) ? tempsystemblock.createcounter - (UINT32_MAX - dev->param.numberblockfortable) : tempsystemblock.createcounter;
			cnand_trace(CNAND_TRACE_INIT, "�������������: ��������� ���� �%u ������� %u\n", tempsystemblock.currentblock, (uint32_t)(tempsystemblock.createcounter));
			uint32_t sizetable = sizeof(uint16_t)*(dev->param.numberblockfortable);
			uint16_t* ptable = dev->blocktable;
			//������ ��������(�) � �������� ����������
			CNAND_ACQUIRE_LOCK(dev->cnandmutex);
			if (!dev->drv.drv_read_page_fn(tempsystemblock.currentblock + dev->param.startblock, 0, dev->cacheblock.cacheblock))
			{
				CNAND_RELEASE_LOCK(dev->cnandmutex);
				flagreset = -1;
				dev->statusblock[tempsystemblock.currentblock] = CNAND_BLOCK_STATE_DEAD;
				continue;
			}
			CNAND_RELEASE_LOCK(dev->cnandmutex);
			//�������� �������
			if (pageforforsystemteble > 1)
			{
				memcpy(ptable, dev->cacheblock.cacheblock + sizeof(cnand_title), (dev->param.fullpagesize) - sizeof(cnand_title));
				ptable += (dev->param.fullpagesize) - sizeof(cnand_title);
				sizetable -= (dev->param.fullpagesize) - sizeof(cnand_title);
				for (uint32_t i = 1; i < pageforforsystemteble; i++)
				{
					CNAND_ACQUIRE_LOCK(dev->cnandmutex);
					if (!dev->drv.drv_read_page_fn(tempsystemblock.currentblock + dev->param.startblock, i, dev->cacheblock.cacheblock))
					{
						flagreset = -1;
						CNAND_RELEASE_LOCK(dev->cnandmutex);
						dev->statusblock[tempsystemblock.currentblock] = CNAND_BLOCK_STATE_DEAD;
						continue;
					}
					CNAND_RELEASE_LOCK(dev->cnandmutex);
					//�������� �������
					memcpy(ptable, dev->cacheblock.cacheblock, sizetable > (dev->param.fullpagesize) ? dev->param.fullpagesize : sizetable);
					ptable += (dev->param.fullpagesize) - sizeof(cnand_title);
				}
			}
			else
			{
				memcpy(ptable, dev->cacheblock.cacheblock + sizeof(cnand_title), sizeof(uint16_t)*(dev->param.numberblockfortable));
			}
			//dev->devstate = CNAND_DEV_STATE_READY;
			//����� ������� ���������� � ����� ������
			for (uint32_t blockcounter = 0; blockcounter < dev->param.numberblockfortable; blockcounter++)
			{
				if (dev->blocktable[blockcounter] & CNAND_BLOCK_STATE_DEAD)
				{
					dev->statusblock[blockcounter] = CNAND_BLOCK_STATE_DEAD;
					cnand_trace(CNAND_TRACE_INIT, "�������������: ���� �%u - �������\n", blockcounter);
				}
				//"�������� �������� �� ������ ����������"
				dev->blocktable[blockcounter] &= 0x7FFF;
				//������ ��������� ������� �����
				if ((dev->blocktable[blockcounter] != CNAND_ERROR_BLOCKNUM) && (dev->statusblock[dev->blocktable[blockcounter]] != CNAND_BLOCK_STATE_DEAD))
				{
					dev->statusblock[dev->blocktable[blockcounter]] = CNAND_BLOCK_STATE_FULL;
				}
			}
			dev->createcounter = (uint32_t)(tempsystemblock.createcounter);
			//�������� ������� ����(�����������) � ���, � ���� ���������� �������������
			dev->cacheblock.currentblock = 0;
			uint8_t* pcacheblock = dev->cacheblock.cacheblock;
			cnand_trace(CNAND_TRACE_INIT, "�������������: �������� �������� ����� ���������� %u ����������� %u  � ���\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
			for (uint32_t page = 0; page < dev->param.pageperblock; page++)
			{
				CNAND_ACQUIRE_LOCK(dev->cnandmutex);
				if (!dev->drv.drv_read_page_fn(dev->blocktable[dev->cacheblock.currentblock] + dev->param.startblock, page, pcacheblock))
				{
					CNAND_ACQUIRE_LOCK(dev->cnandmutex);
					cnand_trace(CNAND_TRACE_SYNC, "�������������: ������ �������� ����� ���������� %u ����������� %u\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
					//cnand_trace(CNAND_TRACE_SYNC, "Write block error: F%u V%u\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
					dev->statusblock[dev->blocktable[dev->cacheblock.currentblock]] = CNAND_BLOCK_STATE_DEAD;
					break;
				}
				CNAND_RELEASE_LOCK(dev->cnandmutex);
				pcacheblock += (dev->param.fullpagesize);
			}
			return true;
		}
		else //������ � ��� ������ �������
		{
			//dev->devstate = CNAND_DEV_STATE_EMPTY;
			/*for (uint32_t blockcounter = 0; blockcounter < dev->param.numberblockfortable; blockcounter++)
			{
			dev->blocktable[blockcounter] = CNAND_ERROR_BLOCKNUM;
			}*/
			return true;
		}
	}
	//return false;
}

cnand_state crutch_nand_read(cnand_dev* dev, uint8_t* buff, uint64_t sector, uint32_t count)
{
	if (dev == NULL) return CNAND_STATE_ERROR_NOINIT;

	uint32_t numblock = 0;
	uint32_t localsector;
	uint32_t shiftaddrsector;
	uint8_t* plocalsector;
	uint8_t* pbuff = buff;
	cnand_trace(CNAND_TRACE_WRITE, "������\n");
	for (uint64_t sectorcounter = sector; sectorcounter < sector + count; sectorcounter++)
	{
		//cnand_trace(CNAND_TRACE_READ, "Read sector %u\n", sector);
		//��������� ������ ��� ���� � �������� �� ��������
		numblock = sectorcounter / dev->cacheblock.sectorinblock; //����� �����
		localsector = (sectorcounter % dev->cacheblock.sectorinblock); //����� ������� � �����
		shiftaddrsector = (localsector + dev->cacheblock.sectorforsystemtable) * dev->param.clustersize;//sizeof(cnand_title) + (sizeof(uint16_t)*(dev->param.endblock - dev->param.startblock)) + (localsector*dev->param.clustersize); //�������� � ������
		plocalsector = dev->cacheblock.cacheblock + shiftaddrsector;//��������� �� ������ � ����(���� �� ��� ����)
		cnand_trace(CNAND_TRACE_READ, "������: ������� - %u ���� - %u ������� � ����� %u\n", (uint32_t)(sector), numblock, localsector);
		if ((dev->cacheblock.currentblock == numblock)) //���� ������� ���� � ����� ������ � ��� �� �����, �� ������ ������ �� ����
		{
			memcpy(pbuff, plocalsector, dev->param.clustersize);//�������� ������ �� ���� � �����
			pbuff += dev->param.clustersize;
			continue;
		}
		else //������ ��������
		{
			if (dev->blocktable[numblock] == CNAND_ERROR_BLOCKNUM)
			{
				//cnand_trace(CNAND_TRACE_READ, "Read sector %u error NOTALLOCATIONBLOCK\n", sector);
				cnand_trace(CNAND_TRACE_READ, "������: ���� %u �� ���������������\n", numblock);
				return CNAND_STATE_ERROR_NOTALLOCATIONBLOCK;
			}
			uint32_t startpage = (uint32_t)(floor(shiftaddrsector / (double)(dev->param.fullpagesize))); //����� ��������
			uint32_t plocalpage = shiftaddrsector % (dev->param.fullpagesize);//����� �� ��������
			//������� ������� �� �� �� ������� ��������
			uint16_t sizecopy;// = (dev->param.fullpagesize) - (plocalpage);
			uint16_t remainingbytes;// = (dev->param.clustersize) - sizecopy;

			if (plocalpage + dev->param.clustersize > dev->param.fullpagesize)
			{
				sizecopy = dev->param.fullpagesize - plocalpage;
				remainingbytes = plocalpage + dev->param.clustersize - dev->param.fullpagesize;
			}
			else
			{
				sizecopy = dev->param.clustersize;
				remainingbytes = 0;
			}

			CNAND_ACQUIRE_LOCK(dev->cnandmutex);
			if (dev->drv.drv_read_page_fn(dev->blocktable[numblock] + dev->param.startblock, startpage, dev->cacheblock.cacheblock))
			{
				CNAND_RELEASE_LOCK(dev->cnandmutex);
				memcpy(pbuff, (dev->cacheblock.cacheblock + plocalpage), sizecopy);//�������� ������ �� ���� � �����
				if (remainingbytes > 0)
				{
					pbuff += sizecopy;
					startpage++;
					CNAND_ACQUIRE_LOCK(dev->cnandmutex);
					if (dev->drv.drv_read_page_fn(dev->blocktable[numblock] + dev->param.startblock, startpage, dev->cacheblock.cacheblock))
					{
						CNAND_RELEASE_LOCK(dev->cnandmutex);
						memcpy(pbuff, dev->cacheblock.cacheblock, remainingbytes);//�������� ������ �� ���� � �����
						pbuff += remainingbytes;
						continue;
					}
					else
					{
						CNAND_RELEASE_LOCK(dev->cnandmutex);
						//cnand_trace(CNAND_TRACE_READ, "Read sector %u error STATE_DEAD\n", sector);
						cnand_trace(CNAND_TRACE_READ, "������: ������ ���� ���������� %u �������\n", dev->blocktable[numblock]);
						dev->statusblock[dev->blocktable[numblock]] = CNAND_BLOCK_STATE_DEAD;
						return CNAND_STATE_ERROR_BADBLOCK;
					}
				}
				pbuff += dev->param.clustersize;
				continue;
			}
			cnand_trace(CNAND_TRACE_READ, "������: ������ ���� ���������� %u �������\n", dev->blocktable[numblock]);
			//cnand_trace(CNAND_TRACE_READ, "Read sector %u error STATE_DEAD\n", sector);
			dev->statusblock[dev->blocktable[numblock]] = CNAND_BLOCK_STATE_DEAD;
			return CNAND_STATE_ERROR_BADBLOCK;
		}
	}
	//cnand_trace(CNAND_TRACE_READ, "Read sector %u STATE_OK\n", sector);
	cnand_trace(CNAND_TRACE_READ, "������: �����\n", dev->blocktable[numblock]);
	return CNAND_STATE_OK;
}

cnand_state crutch_nand_write(cnand_dev* dev, const uint8_t* buff, uint64_t sector, uint32_t count)
{
	if (dev == NULL) return CNAND_STATE_ERROR_NOINIT;

	uint32_t numblock;
	uint32_t localsector;
	uint8_t* plocalsector;
	uint8_t* pcacheblock;
	const uint8_t* pbuff = buff;
	cnand_trace(CNAND_TRACE_SYNC, "������\n");
	for (uint64_t sectorcounter = sector; sectorcounter < sector + count; sectorcounter++)
	{
		//��������� ������ ��� ���� � �������� �� ��������
		numblock = sectorcounter / dev->cacheblock.sectorinblock; //����� �����
		localsector = sectorcounter % dev->cacheblock.sectorinblock; //����� ������� � ����
		plocalsector = dev->cacheblock.cacheblock + ((localsector + dev->cacheblock.sectorforsystemtable) * dev->param.clustersize);//
		cnand_trace(CNAND_TRACE_WRITE, "������: ������� - %u ���� - %u ������� � ����� %u\n", (uint32_t)(sector), numblock, localsector);
		//������ ��������� ������
		//�������� ����� ���� ���� � ������.
		if ((dev->cacheblock.currentblock != numblock))
		{
			if (dev->cacheblock.currentblock != CNAND_ERROR_BLOCKNUM)
			{
				cnand_trace(CNAND_TRACE_WRITE, "������: ������ ����� ����, �������������\n");
				crutch_sync(dev); //����� ������ ����
				//������ ����� ��� � ���
				if (dev->blocktable[numblock] != CNAND_ERROR_BLOCKNUM) //����� ���� ���������������
				{
					cnand_trace(CNAND_TRACE_WRITE, "������: ������ ����� ���� � ���\n");
					pcacheblock = dev->cacheblock.cacheblock;
					for (uint32_t page = 0; page < dev->param.pageperblock; page++)
					{
						CNAND_ACQUIRE_LOCK(dev->cnandmutex);
						if (!dev->drv.drv_read_page_fn(dev->blocktable[numblock] + dev->param.startblock, page, pcacheblock))
						{
							CNAND_ACQUIRE_LOCK(dev->cnandmutex);
							cnand_trace(CNAND_TRACE_SYNC, "��������������: ������ ������ ���������� %u ����������� %u\n", dev->blocktable[numblock], numblock);
							//cnand_trace(CNAND_TRACE_SYNC, "Write block error: F%u V%u\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
							dev->statusblock[dev->blocktable[numblock]] = CNAND_BLOCK_STATE_DEAD;
							break;
						}
						CNAND_RELEASE_LOCK(dev->cnandmutex);
						pcacheblock += (dev->param.fullpagesize);
					}
				}
			}
			if (dev->blocktable[numblock] == CNAND_ERROR_BLOCKNUM) //���� �� ���������������
			{
				cnand_trace(CNAND_TRACE_WRITE, "������: ���� �� ���������������\n");
				dev->blocktable[numblock] = crutch_nand_searchemptyblock(dev);//���� ������ ����
				if (dev->blocktable[numblock] == CNAND_ERROR_BLOCKNUM) //���� �������� �� ���������������
				{
					cnand_trace(CNAND_TRACE_WRITE, "������: ������ �������������\n");
					return CNAND_STATE_ERROR_NEWBLOCK;
				}
				cnand_trace(CNAND_TRACE_WRITE, "������: ������������������ ���� %u\n", dev->blocktable[numblock]);
				memset(dev->cacheblock.cacheblock, 0, dev->param.fullpagesize);//������� ������� ����
				dev->statusblock[dev->blocktable[numblock]] = CNAND_BLOCK_STATE_FULL;
			}
			dev->cacheblock.currentblock = numblock;
		}
		memcpy(plocalsector, pbuff, dev->param.clustersize);//�������� ������ �� ������ � ���
		pbuff += dev->param.clustersize; //������� ���������
		continue;
	}
	cnand_trace(CNAND_TRACE_WRITE, "������: �������\n");
	return CNAND_STATE_OK;
}
/***************************************************************************
*   	��� �������: crutch_sync
*   	����������:  ������
*   	��������:	 ����� ������ �� ���� �� NAND Flash
****************************************************************************/
cnand_state crutch_sync(cnand_dev* dev)
{
	if (dev == NULL) return CNAND_STATE_ERROR_NOINIT;
	size_t flag = true;
	cnand_trace(CNAND_TRACE_SYNC, "��������������\n");
	cnand_title* titleblock = (cnand_title*)(dev->cacheblock.cacheblock);//��������� ��������� �����
	uint8_t* pcacheblock;
	cnand_state state = CNAND_STATE_ERROR_UNKOWN;
	uint32_t freeblock;

	uint16_t* pblocktable = (uint16_t*)(dev->cacheblock.cacheblock + sizeof(cnand_title));//��������� ��� �������
	//����������� ������ ����, ����������
	freeblock = dev->blocktable[dev->cacheblock.currentblock];//���������� ����� �������������� �����
	cnand_trace(CNAND_TRACE_SYNC, "��������������: ����������� ����, ���������� %u ����������� %u, ������� %u\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock, dev->createcounter);
	//cnand_trace(CNAND_TRACE_SYNC, "Block: F%u V%u STATE_FREE\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
	//���� ����� ���� ��� ������
	while (state == CNAND_STATE_ERROR_UNKOWN)
	{
		flag = true;
		pcacheblock = dev->cacheblock.cacheblock;
		dev->blocktable[dev->cacheblock.currentblock] = crutch_nand_searchemptyblock(dev);//���� ������ ���� ���� ����� ������
		//cnand_trace(CNAND_TRACE_SYNC, "New block: F%u V%u\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
		cnand_trace(CNAND_TRACE_SYNC, "��������������: ����� ����, ���������� %u ����������� %u\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
		if (dev->blocktable[dev->cacheblock.currentblock] == CNAND_ERROR_BLOCKNUM)
		{
			return CNAND_STATE_ERROR_NEWBLOCK;
		}
		dev->statusblock[freeblock] = CNAND_BLOCK_STATE_FREE; //����������� ������ ����;

		//����������� ����� ����
		CNAND_ACQUIRE_LOCK(dev->cnandmutex);
		if (!dev->drv.drv_erase_fn(dev->blocktable[dev->cacheblock.currentblock] + dev->param.startblock))
		{
			CNAND_RELEASE_LOCK(dev->cnandmutex);
			dev->statusblock[dev->blocktable[dev->cacheblock.currentblock]] = CNAND_BLOCK_STATE_DEAD;
			continue;
		}
		CNAND_RELEASE_LOCK(dev->cnandmutex);
		dev->statusblock[dev->blocktable[dev->cacheblock.currentblock]] = CNAND_BLOCK_STATE_FULL; //������ ������ �����
		dev->overwrites[dev->blocktable[dev->cacheblock.currentblock]]++;
		//��������� �������� ���������
		titleblock->overwrites = dev->overwrites[dev->blocktable[dev->cacheblock.currentblock]];
		if ((++dev->createcounter) > (UINT32_MAX - dev->param.numberblockfortable - 1)) dev->createcounter = 0;
		titleblock->createcounter = dev->createcounter;
		titleblock->idblock = IDBLOCK;
		//�������� ������� ������������ ������
		//memcpy(&titleblock->blocktable, &dev->blocktable, sizeof(uint16_t)*(dev->param.numberblockfortable));
		//������������
		for (uint32_t blockum = 0; blockum < dev->param.numberblockfortable; blockum++)
		{
			pblocktable[blockum] = dev->blocktable[blockum];
			if (dev->statusblock[blockum] == CNAND_BLOCK_STATE_DEAD)
			{
				pblocktable[blockum] |= CNAND_BLOCK_STATE_DEAD;
			}
		}
		//����� ������
		for (uint32_t page = 0; page < dev->param.pageperblock; page++)
		{
			CNAND_ACQUIRE_LOCK(dev->cnandmutex);
			if (!dev->drv.drv_write_page_fn(dev->blocktable[dev->cacheblock.currentblock] + dev->param.startblock, page, pcacheblock))
			{
				CNAND_ACQUIRE_LOCK(dev->cnandmutex);
				cnand_trace(CNAND_TRACE_SYNC, "��������������: ������ ������ ���������� %u ����������� %u\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
				//cnand_trace(CNAND_TRACE_SYNC, "Write block error: F%u V%u\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock);
				dev->statusblock[dev->blocktable[dev->cacheblock.currentblock]] = CNAND_BLOCK_STATE_DEAD;
				flag = false;
				break;
			}
			CNAND_RELEASE_LOCK(dev->cnandmutex);
			pcacheblock += (dev->param.fullpagesize);
		}
		if (flag == false) continue;
		//cnand_trace(CNAND_TRACE_SYNC, "Write block OK: F%u V%u\tBlock: F%u STATE_FREE\n", dev->blocktable[dev->cacheblock.currentblock], dev->cacheblock.currentblock, freeblock);
		cnand_trace(CNAND_TRACE_SYNC, "��������������: �������\n");
		state = CNAND_STATE_OK;
	}
	return state;
}

static uint16_t crutch_nand_searchemptyblock(cnand_dev* dev)
{
	if (dev == NULL) return CNAND_STATE_ERROR_NOINIT;

	uint32_t blocknum = 0;
	uint32_t min = UINT32_MAX;
	uint16_t blockmin = CNAND_ERROR_BLOCKNUM;
	//���� ���� ��� ������ ����� ������
	if (dev->searchblock.notempty != true)
	{

		for (blocknum = 0; blocknum < (dev->param.numberblockfortable); blocknum++)
		{
			if (dev->statusblock[blocknum] == CNAND_BLOCK_STATE_EMPTY)
			{
				break;
			}
		}
		if (blocknum != (dev->param.numberblockfortable))
		{
			return blocknum;
		}
		else
		{
			dev->searchblock.notempty = true;
		}
	}
	//����������� �� ������	
	for (blocknum = 0; blocknum < (dev->param.numberblockfortable); blocknum++)
	{
		if ((dev->statusblock[blocknum] == CNAND_BLOCK_STATE_DEAD) || (dev->statusblock[blocknum] == CNAND_BLOCK_STATE_FULL))
		{
			continue;
		}
		if (min > dev->overwrites[blocknum])
		{
			blockmin = blocknum;
			min = dev->overwrites[blocknum];
		}
	}
	if (blockmin != CNAND_ERROR_BLOCKNUM)
	{
		return blockmin;
	}
	return CNAND_ERROR_BLOCKNUM;
}

// #ifdef WIN32
// void dprintf(char * format, ...)
// {
// 	static char buf[1024] = { 0 };
// 	memset(buf, 0, sizeof(buf));
// 	va_list args;
// 	va_start(args, format);
// 	vsprintf(buf, format, args);
// 	va_end(args);
// 	OutputDebugStringA(buf);
// }
// #endif // WIN32
