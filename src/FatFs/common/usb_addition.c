/*
 * usb_addition.c
 *
 *  ������: 16 ���. 2015 �.
 *  �����:  �������� �.�
 */

#include "usb_addition.h"
#include "ff.h"
#include "UserConfig.h"

#ifndef WIN32
#include "MassStorageClass.h"
#include "debug.h"
#include "FreeRTOSInclude.h"

#endif
FATFS USB_fs;
static void usb_task(void* Param); //������ ��� ������ � USB
static TaskHandle_t usb_task_handle;
static MutexHandle_t USB_TaskMutex;
#ifndef WIN32
//#include ""
static USB_ClassInfo_MS_Host_t FlashDisk_MS_Interface;
static SCSI_Capacity_t DiskCapacity;

static void Mount_USB(); //������� ������������ �������� �������

#ifdef USB_TARCE
int usb_trace_mask = USB_TRACE_MESSAGE | USB_TRACE_ERROR;
#endif
/************************************************************************************************************/
void USB_TaskInit()
{
	USB_Init(0, USB_MODE_Host); //�������������� USB
	FlashDisk_MS_Interface.Config.DataINPipeNumber = 1;
	FlashDisk_MS_Interface.Config.DataINPipeDoubleBank = false;
	FlashDisk_MS_Interface.Config.DataOUTPipeNumber = 2;
	FlashDisk_MS_Interface.Config.DataOUTPipeDoubleBank = false;
	FlashDisk_MS_Interface.Config.PortNumber = 0;
	vMutexCreate(USB_TaskMutex);
	xTaskCreate(usb_task, "usb_task", __USBTaskStackSize, NULL, __USBTaskPriority, &usb_task_handle);
}
/************************************************************************************************************/
static void usb_task(void* Param)
{
	for (;;)
	{
		xMutexTake(USB_TaskMutex);
		{
			MS_Host_USBTask(&FlashDisk_MS_Interface);
			USB_USBTask(FlashDisk_MS_Interface.Config.PortNumber, USB_MODE_Host);
			Mount_USB();
		}
		xMutexGive(USB_TaskMutex);
		vTaskDelay(1);
	}
}
/************************************************************************************************************/
static void Mount_USB()
{
	static uint8_t oldUSB_HostState = HOST_STATE_Unattached; //

	//������� ��������� �� ������
	if (USB_HostState[FlashDisk_MS_Interface.Config.PortNumber] != oldUSB_HostState)
	{
		if (USB_HostState[FlashDisk_MS_Interface.Config.PortNumber] == HOST_STATE_Configured) //����� � ������
		{
			usb_trace(USB_TRACE_MESSAGE,"���� ���������� ��� �������� ����� �������...\r\n");
			for (;;)
			{
				uint8_t ErrorCode = MS_Host_TestUnitReady(&FlashDisk_MS_Interface, 0);

				if (!(ErrorCode))
					break;

				if (ErrorCode != MS_ERROR_LOGICAL_CMD_FAILED)
				{
					usb_trace(USB_TRACE_ERROR,"������ �������� ���������� ����������\r\n");

					USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
					return;
				}
			}
			usb_trace(USB_TRACE_MESSAGE,"����������� ������� ����������...\r\n");
			if (MS_Host_ReadDeviceCapacity(&FlashDisk_MS_Interface, 0, &DiskCapacity))
			{
				usb_trace(USB_TRACE_ERROR,"������ ��� ������� �������\r\n");
				USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
				return;
			}
			usb_trace(USB_TRACE_MESSAGE,"���-�� ������ %u �������� %u\r\n",DiskCapacity.Blocks,DiskCapacity.BlockSize);
			//��������� �������� �������
			usb_trace(USB_TRACE_MESSAGE,"��������� �������� �������...\r\n");
			f_mount(&USB_fs,"USB:",1);
		}
		else if (USB_HostState[FlashDisk_MS_Interface.Config.PortNumber] == HOST_STATE_Unattached) //��������� ������
		{
			usb_trace(USB_TRACE_MESSAGE,"���������� ���������...\r\n\r\n");
			f_mount(NULL,"USB:",1);

		}
	}
	oldUSB_HostState = USB_HostState[FlashDisk_MS_Interface.Config.PortNumber];
}
/************************************************************************************************************/
void USB_disk_initialize()
{
	/*usb_trace(USB_TRACE_MESSAGE,"����������� ������� ����������...\r\n");
	if (MS_Host_ReadDeviceCapacity(&FlashDisk_MS_Interface, 0, &DiskCapacity))
	{
	usb_trace(USB_TRACE_ERROR,"������ ��� ������� �������\r\n");
	USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
	}*/
}
/************************************************************************************************************/
int USB_disk_read(void *buff, uint32_t secStart, uint32_t numSec)
{
	uint8_t Result = 1;
	TaskHandle_t HolderTask = xGetMutexHolder(USB_TaskMutex);
	if(usb_task_handle !=  HolderTask)
	{
		xMutexTake(USB_TaskMutex);
	}
	if (MS_Host_ReadDeviceBlocks(&FlashDisk_MS_Interface, 0, secStart, numSec, DiskCapacity.BlockSize, buff))
	{
		usb_trace(USB_TRACE_ERROR,"Error reading device block.\r\n");
		USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
		//xMutexGive(USB_TaskMutex);
		Result = 0;
	}
	if(usb_task_handle !=  HolderTask)
	{
		xMutexGive(USB_TaskMutex);
	}
	return Result;
}
/************************************************************************************************************/
int USB_disk_write(void *buff, uint32_t secStart, uint32_t numSec)
{
	uint8_t Result = 1;
	TaskHandle_t HolderTask = xGetMutexHolder(USB_TaskMutex);
	if(usb_task_handle !=  HolderTask)
	{
		xMutexTake(USB_TaskMutex);
	}
	if (MS_Host_WriteDeviceBlocks(&FlashDisk_MS_Interface, 0, secStart, numSec, DiskCapacity.BlockSize, buff))
	{
		usb_trace(USB_TRACE_ERROR,"Error writing device block.\r\n");
		//xMutexGive(USB_TaskMutex);
		Result = 0;
	}
	if(usb_task_handle !=  HolderTask)
	{
		xMutexGive(USB_TaskMutex);
	}
	return Result;
}
/************************************************************************************************************/
DRESULT USB_diskioctl(uint8_t cmd, void *buff)
{
	DRESULT res = RES_ERROR;
	TaskHandle_t HolderTask = xGetMutexHolder(USB_TaskMutex);
	if(usb_task_handle !=  HolderTask)
	{
		xMutexTake(USB_TaskMutex);
	}
	switch (cmd)
	{
	case CTRL_SYNC: /* Make sure that no pending write process */
	{
		//vTaskDelay(50);
		res = RES_OK;
		break;
	}
	case GET_SECTOR_COUNT: /* Get number of sectors on the disk (DWORD) */
	{
		*(DWORD *) buff = DiskCapacity.Blocks;
		res = RES_OK;
		break;
	}
	case GET_SECTOR_SIZE: /* Get R/W sector size (WORD) */
	{
		*(WORD *) buff = DiskCapacity.BlockSize;
		res = RES_OK;
		break;
	}
	case GET_BLOCK_SIZE:/* Get erase block size in unit of sector (DWORD) */
	{
		*(DWORD *) buff = DiskCapacity.BlockSize;
		res = RES_OK;
		break;
	}
	default:
		res = RES_PARERR;
		break;
	}
	if(usb_task_handle !=  HolderTask)
	{
		xMutexGive(USB_TaskMutex);
	}
	return res;
}
/************************************************************************************************************/

/************************************************************************************************************/
/************************************************************************************************************/
/************************************************************************************************************/
/************************************************************************************************************/
/************************************************************************************************************/
/************************************************************************************************************/

void EVENT_USB_Host_DeviceEnumerationFailed(const uint8_t corenum,
	const uint8_t ErrorCode,
	const uint8_t SubErrorCode)
{
	usb_trace(USB_TRACE_MESSAGE,"Dev Enum Error\r\n"
		" -- Error port %d\r\n"
		" -- Error Code %d\r\n"
		" -- Sub Error Code %d\r\n"
		" -- In State %d\r\n",
		corenum, ErrorCode, SubErrorCode, USB_HostState[corenum]);
}
/************************************************************************************************************/
void EVENT_USB_Host_DeviceAttached(const uint8_t corenum)
{
	//usb_trace(USB_TRACE_MESSAGE,("Device Attached on port %d\r\n"), corenum);
}

void EVENT_USB_Host_DeviceUnattached(const uint8_t corenum)
{
	//usb_trace(USB_TRACE_MESSAGE,("\r\nDevice Unattached on port %d\r\n"), corenum);
}

void EVENT_USB_Host_HostError(const uint8_t corenum, const uint8_t ErrorCode)
{
	USB_Disable(corenum, USB_MODE_Host);

	usb_trace(USB_TRACE_MESSAGE,"Host Mode Error\r\n"
		" -- Error port %d\r\n"
		" -- Error Code %d\r\n", corenum, ErrorCode);

	for (;;)
	{
	}
}
/************************************************************************************************************/
void EVENT_USB_Host_DeviceEnumerationComplete(const uint8_t corenum)
{
	uint16_t ConfigDescriptorSize;
	uint8_t ConfigDescriptorData[512];

	usb_trace(USB_TRACE_MESSAGE,"���������� ����� ����������");
	if (USB_Host_GetDeviceConfigDescriptor(corenum, 1, &ConfigDescriptorSize, ConfigDescriptorData,
		sizeof(ConfigDescriptorData)) != HOST_GETCONFIG_Successful)
	{
		usb_trace(USB_TRACE_ERROR,"Error Retrieving Configuration Descriptor.\r\n");
		return;
	}

	FlashDisk_MS_Interface.Config.PortNumber = corenum;
	if (MS_Host_ConfigurePipes(&FlashDisk_MS_Interface,
		ConfigDescriptorSize, ConfigDescriptorData) != MS_ENUMERROR_NoError)
	{
		usb_trace(USB_TRACE_ERROR,"Attached Device Not a Valid Mass Storage Device.\r\n");
		return;
	}

	if (USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 1) != HOST_SENDCONTROL_Successful)
	{
		usb_trace(USB_TRACE_ERROR,"Error Setting Device Configuration.\r\n");
		return;
	}

	uint8_t MaxLUNIndex;
	if (MS_Host_GetMaxLUN(&FlashDisk_MS_Interface, &MaxLUNIndex))
	{
		usb_trace(USB_TRACE_ERROR,"Error retrieving max LUN index.\r\n");
		USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
		return;
	}

	//usb_trace(USB_TRACE_MESSAGE,("Total LUNs: %d - Using first LUN in device.\r\n"), (MaxLUNIndex + 1));

	if (MS_Host_ResetMSInterface(&FlashDisk_MS_Interface))
	{
		usb_trace(USB_TRACE_ERROR,"Error resetting Mass Storage interface.\r\n");
		USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
		return;
	}

	SCSI_Request_Sense_Response_t SenseData;
	if (MS_Host_RequestSense(&FlashDisk_MS_Interface, 0, &SenseData) != 0)
	{
		usb_trace(USB_TRACE_ERROR,"Error retrieving device sense.\r\n");
		USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
		MS_Host_ResetMSInterface(&FlashDisk_MS_Interface);
		return;
	}

	SCSI_Inquiry_Response_t InquiryData;
	if (MS_Host_GetInquiryData(&FlashDisk_MS_Interface, 0, &InquiryData))
	{
		usb_trace(USB_TRACE_ERROR,"Error retrieving device Inquiry data.\r\n");
		USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
		return;
	}

	//usb_trace(USB_TRACE_MESSAGE,"Vendor \"%.8s\", Product \"%.16s\"\r\n", InquiryData.VendorID, InquiryData.ProductID);
	//usb_trace(USB_TRACE_MESSAGE,"Mass Storage Device Enumerated.\r\n");
}
#else
#include <WinBase.h>
#include <Userenv.h>

#define INI_FILE_NAME "CONFIG_EMU.INI"
char DiskName[3];
static HANDLE openDevice(char* devicename);


// static void usb_task(void* Param)
// {
// 	for (;;)
// 	{
// 		xMutexTake(USB_TaskMutex);
// 		HANDLE handle = openDevice(DiskName);
// 		xMutexGive(USB_TaskMutex);
// 		if (handle != INVALID_HANDLE_VALUE)
// 		{
// 			f_mount(&USB_fs, "USB:", 1);
// 		}
// 		else
// 		{
// 			f_mount(NULL, "USB:", 1);
// 		}
// 		vTaskDelay(1000);		
// 	}
// }

void USB_TaskInit()
{
	static char FileName[1024];
	char TempFilePath[1024];
	char FilePath[1024];
	char Disk[3];

	//������� INI-File 
	HMODULE hModule = GetModuleHandle(NULL);
	GetModuleFileName(hModule, FilePath, sizeof(FilePath));
	_splitpath(FilePath, Disk, TempFilePath, NULL, NULL);
	sprintf(FileName, "%s%s%s", Disk, TempFilePath, INI_FILE_NAME);
	if (_access(FileName, 0) != 0) //��� �����
	{
		//��������
		FILE *fp = fopen(FileName, "wb");
		fclose(fp);
		WritePrivateProfileString("���������", "����� �����", "K:", FileName);
		//CreateProfile()
	}
	GetPrivateProfileString("���������", "����� �����", "K:", DiskName, sizeof(DiskName), FileName);
	//vMutexCreate(USB_TaskMutex);
	f_mount(&USB_fs, "USB:", 1);
	//�������� ������ ����������� USB
	
	//xTaskCreate(usb_task, "usb_task", __USBTaskStackSize, NULL, __USBTaskPriority, &usb_task_handle);

	
}

static HANDLE openDevice(char* devicename)
{

	HANDLE handle = INVALID_HANDLE_VALUE;
	/*if (device < 0 || device >99)
		return INVALID_HANDLE_VALUE;*/
	if (strlen(devicename)> 2)
	{
		return INVALID_HANDLE_VALUE;
	}

	char _devicename[20];
	sprintf(_devicename, "\\\\.\\%s", devicename);

	// Creating a handle to disk drive using CreateFile () function ..
	handle = CreateFile(/*"\\\\.\\I:"*/_devicename,  // ����������� ����������
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
		OPEN_EXISTING,
		FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,
		NULL);

	return handle;
}

static BOOL GetDriveGeometry(DISK_GEOMETRY *pdg)
{

	HANDLE hDevice;               // ���������� ������������ ���������� 
	BOOL bResult;                 // ������ ����������
	DWORD junk;                   // ���������� ����������

	// �� ���������� �������� �����
	hDevice = openDevice(DiskName);


	if (hDevice == INVALID_HANDLE_VALUE) // ���������� ������� ����������
	{
		return (FALSE);
	}

	bResult = DeviceIoControl(hDevice,  // ����������� ����������
		IOCTL_DISK_GET_DRIVE_GEOMETRY,  // ����������� ��������
		NULL, 0, // ������ ����� ���
		pdg, sizeof(*pdg),     // ����� ������
		&junk,                 // # ���������� ������
		(LPOVERLAPPED)NULL);  // ������������� �����/������ (I/O)

	CloseHandle(hDevice);

	return (bResult);
}

DISK_GEOMETRY DiskCapacity;
void USB_disk_initialize()
{
	//xMutexTake(USB_TaskMutex);
	GetDriveGeometry(&DiskCapacity);
	//xMutexGive(USB_TaskMutex);
}
int USB_disk_read(void *buff, uint32_t secStart, uint32_t numSec)
{
	//xMutexTake(USB_TaskMutex);
	DWORD dwBytesRead;
	DWORD dwError;
	HANDLE device = openDevice(DiskName);
	if (SetFilePointer(device, (secStart * DiskCapacity.BytesPerSector), NULL, FILE_BEGIN) != 0xFFFFFFFF)
	{
		if (ReadFile(device, buff, (numSec * DiskCapacity.BytesPerSector), &dwBytesRead, NULL))
		{
			CloseHandle(device);
			return 1;
		}
	}
	CloseHandle(device);
	//xMutexGive(USB_TaskMutex);
	return 0;
}
int USB_disk_write(void *buff, uint32_t secStart, uint32_t numSec)
{
	//xMutexTake(USB_TaskMutex);
	DWORD dwBytesWtite;
	HANDLE device = openDevice(DiskName);
	if (SetFilePointer(device, (secStart * DiskCapacity.BytesPerSector), NULL, FILE_BEGIN) != 0xFFFFFFFF)
	{
		if (WriteFile(device, buff, (numSec * DiskCapacity.BytesPerSector), &dwBytesWtite, NULL))
		{
			CloseHandle(device);
			return 1;
		}
	}
	CloseHandle(device);
	//xMutexGive(USB_TaskMutex);
	return 0;
}
DRESULT USB_diskioctl(uint8_t cmd, void *buff)
{
	DRESULT res = RES_ERROR;
	//xMutexTake(USB_TaskMutex);
	switch (cmd)
	{
	case CTRL_SYNC: 
	{
		//vTaskDelay(50);
		res = RES_OK;
		break;
	}
	case GET_SECTOR_COUNT: 
	{
		*(DWORD *)buff = DiskCapacity.Cylinders.LowPart*DiskCapacity.SectorsPerTrack*DiskCapacity.TracksPerCylinder;
		res = RES_OK;
		break;
	}
	case GET_SECTOR_SIZE: 
	{
		*(WORD *)buff = DiskCapacity.BytesPerSector;
		res = RES_OK;
		break;
	}
	case GET_BLOCK_SIZE:
	{
		*(DWORD *)buff = DiskCapacity.BytesPerSector;
		res = RES_OK;
		break;
	}
	default:
		res = RES_PARERR;
		break;
	}
	//xMutexGive(USB_TaskMutex);
	return res;
}
#endif // !WIN32