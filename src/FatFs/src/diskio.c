/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2014        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */


#include "nand_addition.h"
#include "cnand.h"
#include "nandflash.h"
#include "TimeSync.h"
#include <stdlib.h>
#include "usb_addition.h"


static cnand_dev* NAND_dev = NULL;
static cnand_dev* NANDRESORCE_dev = NULL;

#ifdef CNAND_TARCE
int cnand_trace_mask = CNAND_TRACE_READ | CNAND_TRACE_WRITE | CNAND_TRACE_SYNC | CNAND_TRACE_INIT;
#endif
enum
{
	USB = 0x0,
	NAND,
	NANDRESORCE,
};

DWORD get_fattime()
{
	struct tm Time;
	/* Get local time */
	Time = *CT_GetTime();

	/* Pack date and time into a DWORD variable */
	return ((DWORD) (Time.tm_year - 1980) << 25)
		   | ((DWORD) Time.tm_mon << 21)
		   | ((DWORD) Time.tm_mday << 16)
		   | ((DWORD) Time.tm_hour << 11)
		   | ((DWORD) Time.tm_min << 5)
		   | ((DWORD) Time.tm_sec >> 1);
}
/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(
		BYTE pdrv /* Physical drive number to identify the drive */
		)
{
	DSTATUS stat = 0;
	//int result;

	switch (pdrv)
	{
		case USB:
			case NAND:
			case NANDRESORCE:
			//result = USB_disk_status();
			return stat;

	}
	return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(
		BYTE pdrv /* Physical drive nmuber to identify the drive */
		)
{

	switch (pdrv)
	{

		case USB:
		{
			USB_disk_initialize();			
			return RES_OK;
		}
		case NAND:
		{
			if (NAND_dev == NULL) NAND_dev = NAND_disk_initialize(0,NANDFLASH_NUMOF_BLOCK-100,2);
			return RES_OK;
		}
		case NANDRESORCE:
		{
			if(NANDRESORCE_dev == NULL) NANDRESORCE_dev = NAND_disk_initialize(NANDFLASH_NUMOF_BLOCK-99,NANDFLASH_NUMOF_BLOCK,2);
			return RES_OK;
		}
	}
	return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(
		BYTE pdrv, /* Physical drive nmuber to identify the drive */
		BYTE *buff, /* Data buffer to store read data */
		DWORD sector, /* Sector address in LBA */
		UINT count /* Number of sectors to read */
		)
{

	switch (pdrv)
	{

		case USB:
			{
			if (USB_disk_read(buff, sector, count))
			{
				return RES_OK;
			}
			break;
		}
		case NAND:
		case NANDRESORCE:
		{
			cnand_state resultnand;
			if(pdrv == NAND)resultnand = crutch_nand_read(NAND_dev, buff, sector, count);
			else if(pdrv == NANDRESORCE) resultnand = crutch_nand_read(NANDRESORCE_dev, buff, sector, count);
			if (resultnand == CNAND_STATE_OK)
			{
				return RES_OK;
			}
			else if(resultnand == CNAND_STATE_ERROR_NOINIT)
			{
				return RES_NOTRDY;
			}
			else if(resultnand & (CNAND_STATE_ERROR_NOTALLOCATIONBLOCK | CNAND_STATE_ERROR_BADBLOCK))
			{
				return RES_ERROR;
			}
			break;
		}
	}

	return RES_PARERR;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write(
		BYTE pdrv, /* Physical drive nmuber to identify the drive */
		const BYTE *buff, /* Data to be written */
		DWORD sector, /* Sector address in LBA */
		UINT count /* Number of sectors to write */
		)
{
	switch (pdrv)
	{

		case USB:
		{
			if (USB_disk_write((void*) buff, sector, count))
			{
				return RES_OK;
			}
			break;
		}
		case NAND:
		{
			if (crutch_nand_write(NAND_dev, buff, sector, count) == CNAND_STATE_OK)
			{
				return RES_OK;
			}
			break;
		}
		case NANDRESORCE:
		{
			if (crutch_nand_write(NANDRESORCE_dev, buff, sector, count) == CNAND_STATE_OK)
			{
				return RES_OK;
			}
			break;
		}
	}

	return RES_PARERR;
}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl(
		BYTE pdrv, /* Physical drive nmuber (0..) */
		BYTE cmd, /* Control code */
		void *buff /* Buffer to send/receive control data */
		)
{
//DRESULT res;
//int result;
	switch (pdrv)
	{

		case USB:

			// Process of the command the USB drive
			return USB_diskioctl(cmd, buff);

		case NAND:
			return NAND_diskioctl(NAND_dev, cmd, buff);
		case NANDRESORCE:
			return NAND_diskioctl(NANDRESORCE_dev, cmd, buff);
	}

	return RES_PARERR;
}
#endif
