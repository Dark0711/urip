/*
 * ff_user.c
 *
 *  ������: 07 ���� 2015 �.
 *  �����:  �������� �.�
 */
#include "ff.h"
#include "ff_user.h"
#include "printf_stdarg.h"
#include "string.h"

FRESULT scan_files(char* path, char buff)
{
	FRESULT res;
	FILINFO fno;
	DIR dir;
	int i;
	char *fn; /* ���������������, ��� ������������ ��� Unicode. */
#if _USE_LFN
	static char lfn[_MAX_LFN + 1];
	fno.lfname = lfn;
	fno.lfsize = sizeof(lfn);
#endif
	static char filepath[]

	res = f_opendir(&dir, path); /* �������� ���������� */
	if (res == FR_OK)
	{
		i = strlen(path);
		for (;;)
		{
			res = f_readdir(&dir, &fno); /* ������ ������� ���������� */
			if (res != FR_OK || fno.fname[0] == 0)
				break; /* ������� ����� ��� ������ ��� ��� ����������
				 ����� ������ ���������� */
			if (fno.fname[0] == '.')
				continue; /* ������������� �������� '�����' */
#if _USE_LFN
			fn = *fno.lfname ? fno.lfname : fno.fname;
#else
			fn = fno.fname;
#endif
			if (fno.fattrib & AM_DIR)
			{ /* ��� ���������� */
				sprintf(&path[i], "/%s", fn);
				res = scan_files(path);
				if (res != FR_OK) break;
				path[i] = 0;
			}
			else
			{ /* ��� ����. */

				//CLI::UART0_Printf("%s/%s\n", path, fn);
			}
		}
	}
	return res;
}
