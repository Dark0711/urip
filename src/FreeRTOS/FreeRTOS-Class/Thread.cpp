#include "Thread.h"

extern "C" void pvTaskCode(void *pvParameters)
{
	(static_cast<CThread*>(pvParameters))->run();
}

CThread::CThread(const char *pcName, unsigned short usStackDepth, TThreadPriority uxPriority)
{
	try
	{
		if (xTaskCreate(pvTaskCode, pcName, usStackDepth, (void*) this, uxPriority, &pxCreatedTask) == pdFALSE)
			throw 0;

	}
	catch (int i)
	{

		while (1)
			;
	}
}
