/*
 * SemphrWrapper.h
 *
 *  ������: 18 ��� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef FREERTOS_FREERTOS_CLASS_SEMPHRWRAPPER_H_
#define FREERTOS_FREERTOS_CLASS_SEMPHRWRAPPER_H_

#include "FreeRTOSInclude.h"

class CSemaphore
{
	private:
		CSemaphore(const CSemaphore&);
	private:
		BaseType_t pxHigherPriorityTaskWoken;
		SemaphoreHandle_t xSemaphoreHandle;
		public:
		CSemaphore() :
				pxHigherPriorityTaskWoken(0)
		{
			xSemaphoreHandle = xSemaphoreCreateBinary();
		}
		~CSemaphore()
		{
			vSemaphoreDelete(xSemaphoreHandle);
		}
		BaseType_t take() const
		{
			return xSemaphoreTake(xSemaphoreHandle, portMAX_DELAY);
		}
		BaseType_t take(TickType_t xTicksToWait) const
		{
			return xSemaphoreTake(xSemaphoreHandle, xTicksToWait);
		}
		BaseType_t take_irq()
		{
			return xSemaphoreTakeFromISR(xSemaphoreHandle, &pxHigherPriorityTaskWoken);
		}
		BaseType_t give() const
		{
			return xSemaphoreGive(xSemaphoreHandle);
		}
		BaseType_t give_irq()
		{
			return xSemaphoreGiveFromISR(xSemaphoreHandle, &pxHigherPriorityTaskWoken);
		}
		BaseType_t give_hirq()
		{
			BaseType_t Status;
			Status = xSemaphoreGiveFromISR(xSemaphoreHandle, &pxHigherPriorityTaskWoken);
			portYIELD_FROM_ISR(pxHigherPriorityTaskWoken);
			return Status;
		}
};

#endif /* FREERTOS_FREERTOS_CLASS_SEMPHRWRAPPER_H_ */
