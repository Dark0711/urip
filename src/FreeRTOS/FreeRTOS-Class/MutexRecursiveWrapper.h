/*
 * MutexRecursiveWrapper.h
 *
 *  ������: 04 ���� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef FREERTOS_FREERTOS_CLASS_MUTEXRECURSIVEWRAPPER_H_
#define FREERTOS_FREERTOS_CLASS_MUTEXRECURSIVEWRAPPER_H_

#include "FreeRTOSInclude.h"

class CRecursiveMutex
{
	private:
		MutexHandle_t xMutexHandle;
	public:
		CRecursiveMutex(){xMutexHandle = xSemaphoreCreateRecursiveMutex();}
		~CRecursiveMutex(){vMutexDelete(xMutexHandle);}
		inline BaseType_t give(){ return xSemaphoreGiveRecursive(xMutexHandle); }
		inline BaseType_t take(){ return xSemaphoreTakeRecursive(xMutexHandle, portMAX_DELAY); }
		inline BaseType_t take(TickType_t xTicksToWait){ return xSemaphoreTakeRecursive(xMutexHandle, xTicksToWait); }
};




#endif /* FREERTOS_FREERTOS_CLASS_MUTEXRECURSIVEWRAPPER_H_ */
