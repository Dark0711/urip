/*
 * FileReadWriteTest.c
 *
 *  ������: 16 ���. 2015 �.
 *  �����:  �������� �.�
 */
#include <CustomCommands.h>
#include "FileReadWriteTest.h"
#include "printf_stdarg.h"
#include "TimeSync.h"
#include <string.h>
#include "class_cli.hpp"

TFileReadWriteTest::TFileReadWriteTest() :
		CThread("ReadWriteTest", stackDepth, priority)
{

}



void TFileReadWriteTest::run()
{
	FRESULT Status;
	FIL file_usb;
	FIL file_nand;
	uint8_t Buff[255];
	UINT FileRead;
	UINT FileWrite;

	for (;;)
	{
		Status = f_mount(&fsnand, "NAND:", 1);
		//Status = f_mkfs("NAND:", 0, 0);

		CLI::UART0_Printf("��������� ���� �� usb\n");
		do
		{
			Status = f_open(&file_usb, "USB:1.pdf", FA_READ);
			delay(1000);
		}
		while (Status);

		CLI::UART0_Printf("������ ���� �� NAND Flash\n");
		do
		{
			Status = f_open(&file_nand, "NAND:1.pdf", FA_READ | FA_CREATE_ALWAYS | FA_WRITE);
			delay(1000);
		}
		while (Status);

		CLI::UART0_Printf("�������� ���� �� NAND FLASH\n");
		for (int i = 0; i < f_size(&file_usb); i++)
		{
			Status = f_read(&file_usb, Buff, sizeof(Buff), &FileRead);
			if (Status != FR_OK)
			{
				CLI::UART0_Printf("������ ������ �����\n");
				break;
			}
			CLI::UART0_Printf("_%02x",i);
			//-----------------------------------------------------------------
			Status = f_write(&file_nand, Buff, FileRead, &FileWrite);
			if (Status != FR_OK)
			{
				CLI::UART0_Printf("������ ������ �����\n");
				break;
			}
			if (FileWrite == 0)
			{
				CLI::UART0_Printf("����������� ��������� ����� �� ������\n");
				break;
			}
			if (f_eof(&file_usb))
			{
				CLI::UART0_Printf("����� �� ����� �����\n");
				break;
			}
		}
		CLI::UART0_Printf("\n");
		Status = f_close(&file_usb);
		Status = f_close(&file_nand);
		CLI::UART0_Printf("\n");
		CLI::UART0_Printf("������ ���� �� USB \n");
		do
		{
			Status = f_open(&file_usb, "USB:1 copy.pdf", FA_READ | FA_CREATE_NEW | FA_WRITE);
			delay(1000);
		}
		while (Status);

		CLI::UART0_Printf("��������� ���� �� NAND FLASH\n");
		do
		{
			Status = f_open(&file_nand, "NAND:1.pdf", FA_READ);
			delay(1000);
		}
		while (Status);

		CLI::UART0_Printf("�������� ���� �� USB\n");
		for (int i = 0; i < f_size(&file_nand); i++)
		{
			Status = f_read(&file_nand, Buff, sizeof(Buff), &FileRead);
			if (Status != FR_OK)
			{
				CLI::UART0_Printf("������ ������ �����\n");
				break;
			}
			CLI::UART0_Printf("_%02x",i);
			//-----------------------------------------------------------------
			Status = f_write(&file_usb, Buff, FileRead, &FileWrite);
			if (Status != FR_OK)
			{
				CLI::UART0_Printf("������ ������ �����\n");
				break;
			}
			if (FileWrite == 0)
			{
				CLI::UART0_Printf("����������� ��������� ����� �� ������\n");
				break;
			}
			if (f_eof(&file_nand))
			{
				CLI::UART0_Printf("����� �� ����� �����\n");
				break;
			}
		}

		CLI::UART0_Printf("\n");
		Status = f_close(&file_usb);
		Status = f_close(&file_nand);
		CLI::UART0_Printf("�����\n");
		//delay(10000);
		Status = f_mount(NULL, "NAND:", 0);
	}
}

FRESULT TFileReadWriteTest::scan_files(char* path)
{
	FRESULT res;
	FILINFO fno;
	DIR dir;
	int i;
	char *fn; /* ���������������, ��� ������������ ��� Unicode. */
#if _USE_LFN
	static char lfn[_MAX_LFN + 1];
	fno.lfname = lfn;
	fno.lfsize = sizeof(lfn);
#endif
	res = f_opendir(&dir, path); /* �������� ���������� */
	if (res == FR_OK)
	{
		i = strlen(path);
		for (;;)
		{
			res = f_readdir(&dir, &fno); /* ������ ������� ���������� */
			if (res != FR_OK || fno.fname[0] == 0)
				break; /* ������� ����� ��� ������ ��� ��� ����������
				 ����� ������ ���������� */
			if (fno.fname[0] == '.')
				continue; /* ������������� �������� '�����' */
#if _USE_LFN
			fn = *fno.lfname ? fno.lfname : fno.fname;
#else
			fn = fno.fname;
#endif
			if (fno.fattrib & AM_DIR)
			{ /* ��� ���������� */
				sprintf(&path[i], "/%s", fn);
				res = scan_files(path);
				if (res != FR_OK) break;
				path[i] = 0;
			}
			else
			{ /* ��� ����. */
				CLI::UART0_Printf("%s/%s\n", path, fn);
			}
		}
	}
	return res;
}
