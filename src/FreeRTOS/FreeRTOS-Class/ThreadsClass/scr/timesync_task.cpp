/*
 * spi_task.cpp
 *
 *  ������: 21 ��� 2015 �.
 *  �����:  �������� �.�
 */

#include "timesync_task.hpp"
#include "lpc177x_8x_gpdma.h"
#include "lpc177x_8x_pinsel.h"
#include "lpc177x_8x_gpio.h"
#include "gpdma.h"
#include "debug.h"
#include "class_cli.hpp"

CSemaphore TimeSyncTask::FlagSemaphore;
uint32_t TimeSyncTask::TimerCounter;
bool TimeSyncTask::Sync = false;
cTIMER TimeSyncTask::Timer(LPC_TIM0, cTIMER::COUTCONTROL_HIGH, cTIMER::CAPTURE_NUM1);

TimeSyncTask::TimeSyncTask() :
		CThread("SPI", stackDepth, priority)
{
	PINSEL_ConfigPin(3, 23, 3); //T0_CAP0
	PINSEL_ConfigPin(3, 24, 3); //T0_CAP1
	PINSEL_ConfigPin(3, 25, 3); //T0_MAT0

	Timer.ConfigCapture(cTIMER::CAPTURE_NUM0, false, true, true);
	Timer.ConfigMatch(cTIMER::MATCH_NUM0, false, false, false, cTIMER::TOGGLE, 1000);

	Timer.SetPriorityIRQ(configTIMER_INTERRUPT_PRIORITY);
	Timer.EnableIRQ();
	Timer.Enable();

}

void TimeSyncTask::TIMER0_IRQHandler(void)
{
	if (Timer.GetIntStatus(cTIMER::CR0_Interrupt))
	{
		TimerCounter = Timer.GetTimerCounter(); //Timer->TC;
		//������������� �����
		if (!Timer.GetFallingEdge(cTIMER::CAPTURE_NUM0) && Timer.GetRisingEdge(cTIMER::CAPTURE_NUM0))
		{
			Timer.InvertRisingFallingEdges(cTIMER::CAPTURE_NUM0);
			Timer.ResetCounter();
		}
		else if (Timer.GetFallingEdge(cTIMER::CAPTURE_NUM0) && !(Timer.GetRisingEdge(cTIMER::CAPTURE_NUM0)))
		{
			//��������� �����
			if ((TimerCounter > 25) && (TimerCounter < 35)) //��������� �����
			{
			}
			else if ((TimerCounter > 95) && (TimerCounter < 105)) //�������� �����
			{
				Sync = true;
			}
			else if ((TimerCounter > 295) && (TimerCounter < 305)) //�����������
			{
				Sync = true;
			}
			Timer.InvertRisingFallingEdges(cTIMER::CAPTURE_NUM0);
		}
		FlagSemaphore.give_hirq();
		Timer.ClearIntStatus(cTIMER::CR0_Interrupt);
	}
	Timer.ClearAllIntStatus();
}

void TimeSyncTask::run()
{
	for (;;)
	{
		if (FlagSemaphore.take())
		{
			//CLI::UART0_Printf("TimerCounter: %u\n", TimerCounter);
			//CLI::UART0_SendString("TimerCounter: %u\n");

		}
	}
}
