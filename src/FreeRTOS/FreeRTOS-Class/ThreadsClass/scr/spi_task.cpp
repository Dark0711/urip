/*
 * spi_task.cpp
 *
 *  ������: 21 ��� 2015 �.
 *  �����:  �������� �.�
 */

#include "spi_task.hpp"
#include "string.h"
#include "lpc177x_8x_gpdma.h"
#include "gpdma.h"

CSemaphore SpiTask::FlagSemaphore;
uint32_t SpiTask::DMA_CHANAL;

SpiTask::SpiTask():CThread("SPI", stackDepth, priority),SSP_ID(SSP_0)
{
	SSP_ConfigStructInit(&SSP_ConfigStruct);
	SSP_ConfigStruct.CPHA = SSP_CPHA_SECOND;
	SSP_ConfigStruct.CPOL = SSP_CPOL_LO;
	SSP_ConfigStruct.Databit = SSP_DATABIT_16;
	SSP_Init(SSP_ID,&SSP_ConfigStruct);
	SSP_Cmd(SSP_ID, ENABLE);
	//SSP_DATA_SettingDma(0);
}


void SpiTask::run()
{

	for(;;)
	{
		//memset(Buff,0x65,sizeof(Buff));
		//SSP_DATA_SettingDma(0);
		//FlagSemaphore.take();
		//delay(1);
		/*for(int i=0;i<100;i++)
		{
			//SSP_SendData(SSP_ID,0xAAAA);
		}*/
		//delay(1000);
		delay(10);

		memset(Buff,0xAf,sizeof(Buff));
		dataCfg.length = sizeof(Buff);
		dataCfg.tx_data = Buff;
		SSP_ReadWrite(SSP_ID,&dataCfg,SSP_TRANSFER_POLLING);
		//delay(1000);

	}
}

void SpiTask::SSP_DATA_SettingDma(uint32_t ChannelNum)
{
	 GPDMA_Channel_CFG_Type GPDMACfg;
	 DMA_CHANAL = ChannelNum;
	  GPDMACfg.ChannelNum = ChannelNum;
	  /* Source memory - not used*/
	  GPDMACfg.SrcMemAddr = (uint32_t)&Buff;
	  /* Destination memory - Not used*/
	  GPDMACfg.DstMemAddr = 0;
	  /* Transfer size*/
	  GPDMACfg.TransferSize = sizeof(Buff)/2;
	  /* Transfer width - not used*/
	  GPDMACfg.TransferWidth = 0;
	  /* Transfer type*/
	  GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_M2P;
	  /* Source connection */
	  GPDMACfg.SrcConn = GPDMA_CONN_MAT0_1;
	  /* Destination connection - not used*/
	  GPDMACfg.DstConn = GPDMA_CONN_SSP0_Tx;
	  /* Linker List Item - unused*/
	  GPDMACfg.DMALLI = 0;
	  // Setup channel with given parameter
	  GPDMA_Setup(&GPDMACfg);
	  LPC_GPDMA->Sync |= (1 << 1);
	  // Enable GPDMA channel
	  GPDMA_SetIRQAddr(ChannelNum,SSP2_DMA_IRQHandler);//���������� SPI ��� DMA
	  SSP_DMACmd (SSP_ID,SSP_DMA_TX, ENABLE);
	  GPDMA_ChannelCmd(ChannelNum, ENABLE);
	 // (DMA_IRQn,configDMA_INTERRUPT_PRIORITY);
	  /* Enable GPDMA interrupt */
	  NVIC_EnableIRQ(DMA_IRQn);
}

void SpiTask::SSP2_DMA_IRQHandler()
{
  /* check GPDMA interrupt on channel 1 */
  if (GPDMA_IntGetStatus(GPDMA_STAT_INT, DMA_CHANAL))
  {
	/* Check counter terminal status */
	if (GPDMA_IntGetStatus(GPDMA_STAT_INTTC, DMA_CHANAL))
    {
	  /* Clear terminate counter Interrupt pending */
	  GPDMA_ClearIntPending (GPDMA_STATCLR_INTTC, DMA_CHANAL);
	  //����� �������
	  //xSemaphoreGiveFromISR(SSPData_SemaphoreHandle,&pxHigherPriorityTaskWoken);
      //portYIELD_FROM_ISR(pxHigherPriorityTaskWoken);

	}

	/* Check error terminal status */
	if (GPDMA_IntGetStatus(GPDMA_STAT_INTERR, DMA_CHANAL)) {
	  /* Clear error counter Interrupt pending */
	  GPDMA_ClearIntPending (GPDMA_STATCLR_INTERR, DMA_CHANAL);
	}
	FlagSemaphore.give_irq();
  }

}
