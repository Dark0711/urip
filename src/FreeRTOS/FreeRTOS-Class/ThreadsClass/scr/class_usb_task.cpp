/*
 * class_usb_task.c
 *
 *  Created on: 10 ���. 2015 �.
 *      Author: �����
 */
#include <class_usb_task.h>
#include "debug.h"
#include <ctype.h>

TUSBTask* USBTask;

bool TUSBTask::OnliOneClass;

TUSBTask::TUSBTask(USB_ClassInfo_MS_Host_t* FlashDisk) :
		Thread("USBTask", stackDepth, priority)
{
	if (OnliOneClass) //����� ������ ���� � ������������ ����������
	{
		while (1)
			;
	}
	else
	{
		FlashDisk_MS_Interface = FlashDisk;
		OnliOneClass = true;
		USB_Init(0, USB_MODE_Host); //������������� USB
	}

}

void TUSBTask::run()
{
	for (;;)
	{
		MassStorageHost_Task();
		MS_Host_USBTask(FlashDisk_MS_Interface);
		USB_USBTask(FlashDisk_MS_Interface->Config.PortNumber, USB_MODE_Host);
		//delay(20);
	}
}

void TUSBTask::MassStorageHost_Task(void)
{
	if (USB_HostState[FlashDisk_MS_Interface->Config.PortNumber] != HOST_STATE_Configured)
		return;

	Debug_printf("Waiting until ready...\r\n");

	for (;;)
	{
		uint8_t ErrorCode = MS_Host_TestUnitReady(FlashDisk_MS_Interface, 0);

		if (!(ErrorCode))
			break;

		if (ErrorCode != MS_ERROR_LOGICAL_CMD_FAILED)
		{
			Debug_printf(("Error waiting for device to be ready.\r\n"));

			USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface->Config.PortNumber, 0);
			return;
		}
	}

	Debug_printf("Retrieving Capacity...\r\n");

	SCSI_Capacity_t DiskCapacity;
	if (MS_Host_ReadDeviceCapacity(FlashDisk_MS_Interface, 0, &DiskCapacity))
	{
		Debug_printf(("Error retrieving device capacity.\r\n"));
		USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface->Config.PortNumber, 0);
		return;
	}

	Debug_printf("%u blocks of %u bytes.\r\n", DiskCapacity.Blocks, DiskCapacity.BlockSize);
	static uint8_t BlockBuffer[512];
	static int CounterBlock = DiskCapacity.Blocks;
	static int CounterBlock2 = 1;
	while (CounterBlock--)
	{
		Debug_printf("\r\nContents of block %u:\r\n",CounterBlock2);
		if (MS_Host_ReadDeviceBlocks(FlashDisk_MS_Interface, 0, CounterBlock2*512, 1, DiskCapacity.BlockSize, BlockBuffer))
		{
			Debug_printf(("Error reading device block.\r\n"));
			USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface->Config.PortNumber, 0);
			return;
		}
		CounterBlock2++;
		for (uint16_t Chunk = 0; Chunk < (DiskCapacity.BlockSize >> 4); Chunk++)
		{
			uint8_t* ChunkPtr = &BlockBuffer[Chunk << 4];

			/* Print out the 16 bytes of the chunk in HEX format */
			for (uint8_t ByteOffset = 0; ByteOffset < (1 << 4); ByteOffset++)
			{
				char CurrByte = *(ChunkPtr + ByteOffset);
				Debug_printf("%02X ", CurrByte);
			}

			Debug_printf("    ");

			/* Print out the 16 bytes of the chunk in ASCII format */
			/*for (uint8_t ByteOffset = 0; ByteOffset < (1 << 4); ByteOffset++)
			{
				//char CurrByte = *(ChunkPtr + ByteOffset);
				//putchar(isprint(CurrByte) ? CurrByte : '.');
			}*/

			Debug_printf("\r\n");
		}
		delay(20);
	}
	Debug_printf("__________END________________\r\n");
	//USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber, 0);
}

void EVENT_USB_Host_DeviceEnumerationFailed(const uint8_t corenum,
		const uint8_t ErrorCode,
		const uint8_t SubErrorCode)
{
	Debug_printf(("Dev Enum Error\r\n"
			" -- Error port %d\r\n"
			" -- Error Code %d\r\n"
			" -- Sub Error Code %d\r\n"
			" -- In State %d\r\n"),
			corenum, ErrorCode, SubErrorCode, USB_HostState[corenum]);
}

void EVENT_USB_Host_DeviceAttached(const uint8_t corenum)
{
	Debug_printf(("Device Attached on port %d\r\n"), corenum);
}

void EVENT_USB_Host_DeviceUnattached(const uint8_t corenum)
{
	Debug_printf(("\r\nDevice Unattached on port %d\r\n"), corenum);
}

void EVENT_USB_Host_HostError(const uint8_t corenum, const uint8_t ErrorCode)
{
	USB_Disable(corenum, USB_MODE_Host);

	Debug_printf(("Host Mode Error\r\n"
			" -- Error port %d\r\n"
			" -- Error Code %d\r\n"), corenum, ErrorCode);

	for (;;)
	{
	}
}

void EVENT_USB_Host_DeviceEnumerationComplete(const uint8_t corenum)
{
	uint16_t ConfigDescriptorSize;
	uint8_t ConfigDescriptorData[512];
	portENTER_CRITICAL();
	if (USB_Host_GetDeviceConfigDescriptor(corenum, 1, &ConfigDescriptorSize, ConfigDescriptorData,
			sizeof(ConfigDescriptorData)) != HOST_GETCONFIG_Successful)
	{
		Debug_printf("Error Retrieving Configuration Descriptor.\r\n");
		return;
	}

	USBTask->FlashDisk_MS_Interface->Config.PortNumber = corenum;
	if (MS_Host_ConfigurePipes(USBTask->FlashDisk_MS_Interface,
			ConfigDescriptorSize, ConfigDescriptorData) != MS_ENUMERROR_NoError)
	{
		Debug_printf("Attached Device Not a Valid Mass Storage Device.\r\n");
		return;
	}

	if (USB_Host_SetDeviceConfiguration(USBTask->FlashDisk_MS_Interface->Config.PortNumber, 1) != HOST_SENDCONTROL_Successful)
	{
		Debug_printf("Error Setting Device Configuration.\r\n");
		return;
	}

	uint8_t MaxLUNIndex;
	if (MS_Host_GetMaxLUN(USBTask->FlashDisk_MS_Interface, &MaxLUNIndex))
	{
		Debug_printf("Error retrieving max LUN index.\r\n");
		USB_Host_SetDeviceConfiguration(USBTask->FlashDisk_MS_Interface->Config.PortNumber, 0);
		return;
	}

	Debug_printf(("Total LUNs: %d - Using first LUN in device.\r\n"), (MaxLUNIndex + 1));

	if (MS_Host_ResetMSInterface(USBTask->FlashDisk_MS_Interface))
	{
		Debug_printf("Error resetting Mass Storage interface.\r\n");
		USB_Host_SetDeviceConfiguration(USBTask->FlashDisk_MS_Interface->Config.PortNumber, 0);
		return;
	}

	SCSI_Request_Sense_Response_t SenseData;
	if (MS_Host_RequestSense(USBTask->FlashDisk_MS_Interface, 0, &SenseData) != 0)
	{
		Debug_printf("Error retrieving device sense.\r\n");
		USB_Host_SetDeviceConfiguration(USBTask->FlashDisk_MS_Interface->Config.PortNumber, 0);
		MS_Host_ResetMSInterface(USBTask->FlashDisk_MS_Interface);
		return;
	}

	//  if (MS_Host_PreventAllowMediumRemoval(&USBTask->FlashDisk_MS_Interface, 0, true)) {
	//      Debug_printf("Error setting Prevent Device Removal bit.\r\n");
	//      USB_Host_SetDeviceConfiguration(USBTask->FlashDisk_MS_Interface.Config.PortNumber, 0);
	//      return;
	//  }

	SCSI_Inquiry_Response_t InquiryData;
	if (MS_Host_GetInquiryData(USBTask->FlashDisk_MS_Interface, 0, &InquiryData))
	{
		Debug_printf("Error retrieving device Inquiry data.\r\n");
		USB_Host_SetDeviceConfiguration(USBTask->FlashDisk_MS_Interface->Config.PortNumber, 0);
		return;
	}

	Debug_printf("Vendor \"%.8s\", Product \"%.16s\"\r\n", InquiryData.VendorID, InquiryData.ProductID);

	Debug_printf("Mass Storage Device Enumerated.\r\n");
	portEXIT_CRITICAL();
}

