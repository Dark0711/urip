/*
 * FileReadWriteTest.h
 *
 *  ������: 16 ���. 2015 �.
 *  �����:  �������� �.�
 */

#ifndef FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_FILEREADWRITETEST_H_
#define FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_FILEREADWRITETEST_H_

#include "ff.h"
#include "Thread.h"

class TFileReadWriteTest:public CThread
{
	public:
		static const TThreadPriority priority = eLow;
	private:
		static const unsigned short stackDepth = configMINIMAL_STACK_SIZE*10;
		FATFS fsnand;
		FATFS fsusb;
		FRESULT scan_files (char* path );
	public:
		void run();
		TFileReadWriteTest();
};



#endif /* FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_FILEREADWRITETEST_H_ */
