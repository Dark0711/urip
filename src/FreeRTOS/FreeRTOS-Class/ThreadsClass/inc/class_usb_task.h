/*
 * class_usb_task.h
 *
 *  Created on: 10 ���. 2015 �.
 *      Author: �����
 */

#ifndef FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_CLASS_USB_TASK_H_
#define FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_CLASS_USB_TASK_H_

#include "Thread.h"
#include "MassStorageClass.h"

class CUSBTask: public CThread
{
	public:
		static const char priority = PRIORITY_NORMAL;
		static const unsigned short stackDepth = configMINIMAL_STACK_SIZE*4;
	private:
		friend void EVENT_USB_Host_DeviceEnumerationComplete(const uint8_t corenum);
		static bool OnliOneClass;
		USB_ClassInfo_MS_Host_t* FlashDisk_MS_Interface;
		void MassStorageHost_Task(void);
	public:
		void run();
		CUSBTask(USB_ClassInfo_MS_Host_t* FlashDisk);
};

extern CUSBTask* USBTask;

#endif /* FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_CLASS_USB_TASK_H_ */
