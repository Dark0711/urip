/*
 * spi_task.hpp
 *
 *  ������: 21 ��� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_TIMESYNC_TASK_HPP_
#define FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_TIMESYNC_TASK_HPP_

#include "Thread.h"
#include "SemphrWrapper.h"
#include "QueueWrapper.h"
#include "lpc177x_8x_ssp.h"
#include "cIRQ.hpp"
#include "cTIMER.hpp"

extern cIRQ_Management IRQ_Management;

class TimeSyncTask: public CThread
{
	public:
		static cTIMER Timer;
		static CSemaphore FlagSemaphore;
		static uint32_t TimerCounter;
		static bool Sync;
	public:
		static const TThreadPriority priority = eVeryhigh;
		static const unsigned short stackDepth = configMINIMAL_STACK_SIZE * 6;
	public:
		TimeSyncTask();
		void run();
	public:
		//void IRQ(void);
		static void TIMER0_IRQHandler(void);
};

#endif /* FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_TIMESYNC_TASK_HPP_ */
