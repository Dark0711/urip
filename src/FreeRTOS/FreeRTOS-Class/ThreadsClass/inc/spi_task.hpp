/*
 * spi_task.hpp
 *
 *  ������: 21 ��� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_SPI_TASK_HPP_
#define FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_SPI_TASK_HPP_

#include "Thread.h"
#include "SemphrWrapper.h"
#include "QueueWrapper.h"
#include "lpc177x_8x_ssp.h"

#include <vector>

class SpiTask : public CThread
{
	private:
		SSP_CFG_Type SSP_ConfigStruct;//��������� SSP
		SSP_ID_Type SSP_ID;
		SSP_DATA_SETUP_Type dataCfg;
		char Buff[4];
		void SSP_DATA_SettingDma(uint32_t ChannelNu);
	private:

	public:
		static std::vector<uint32_t> StatisticsTimer;
		static void SSP2_DMA_IRQHandler();
		static CSemaphore FlagSemaphore;
		static uint32_t DMA_CHANAL;
	public:
		static const TThreadPriority priority = eVeryhigh;
		static const unsigned short stackDepth = configMINIMAL_STACK_SIZE*5;
	public:
		SpiTask();
		void run();
};


#endif /* FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_SPI_TASK_HPP_ */
