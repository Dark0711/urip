/*
 * MutexWrapper.h
 *
 *  ������: 18 ��� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef FREERTOS_FREERTOS_CLASS_THREADSCLASS_MUTEXWRAPPER_H_
#define FREERTOS_FREERTOS_CLASS_THREADSCLASS_MUTEXWRAPPER_H_

#include "FreeRTOSInclude.h"

class CMutex
{
	private:
		CMutex(const CMutex&);
	private:
		MutexHandle_t xMutexHandle;
		public:
		CMutex()
		{
			vMutexCreate(xMutexHandle);
		}
		~CMutex()
		{
			vMutexDelete(xMutexHandle);
		}
		bool give() const
		{
			return xMutexGive(xMutexHandle);
		}
		bool give(TaskHandle_t TaskHandle) const
				{
			if (xGetMutexHolder(xMutexHandle) != TaskHandle)
			return give();
			else return pdTRUE;
		}
		bool take() const
		{
			return xMutexTake(xMutexHandle);
		}
		bool take(TaskHandle_t TaskHandle) const
				{
			if (xGetMutexHolder(xMutexHandle) != TaskHandle)
			return take();
			else return pdTRUE;
		}
		bool take(TickType_t xTicksToWait) const
				{
			return xSemaphoreTake(xMutexHandle, xTicksToWait);
		}
};

#endif /* FREERTOS_FREERTOS_CLASS_THREADSCLASS_MUTEXWRAPPER_H_ */
