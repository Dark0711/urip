
#ifndef MUTEX_H
#define MUTEX_H

#include "semphr.h"
#include "FreeRTOSInclude.h"


typedef QueueHandle_t MutexHandle_t;

#define vMutexCreate(Mutex) 	Mutex = xSemaphoreCreateMutex()
#define xMutexGive(Mutex) 		xSemaphoreGive(Mutex)
#define xMutexTake(Mutex) 		xSemaphoreTake(Mutex,portMAX_DELAY)
#define vMutexDelete(Mutex) 	vSemaphoreDelete(Mutex)
#define xGetMutexHolder(Mutex)  xSemaphoreGetMutexHolder(Mutex)

#define vRecursiveMutexCreate(Mutex) 	Mutex = xSemaphoreCreateRecursiveMutex()
#define xRecursiveMutexGive(Mutex) 		xSemaphoreGiveRecursive(Mutex)
#define xRecursiveMutexTake(Mutex) 		xSemaphoreTake(Mutex,portMAX_DELAY)
#define vRecursiveMutexDelete(Mutex) 	vSemaphoreDelete(Mutex)


#endif
