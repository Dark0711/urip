#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

#include "PrioritiesTask.h"

#define configUSE_PREEMPTION		1
#define configUSE_IDLE_HOOK			0
#define configMAX_PRIORITIES		( PRIORITY_CRITICAL )
#define configUSE_TICK_HOOK			0

#define configCPU_CLOCK_HZ			( ( unsigned long ) 120000000 )

#define configTICK_RATE_HZ						(( portTickType ) 1000 )
#define configMINIMAL_STACK_SIZE				(120)
#define configTOTAL_HEAP_SIZE					(( size_t ) (20 * 1024) )
#define configMAX_TASK_NAME_LEN					( 16 )

#define configUSE_TRACE_FACILITY				0
#define configUSE_STATS_FORMATTING_FUNCTIONS 	0

#define configUSE_16_BIT_TICKS					0
#define configIDLE_SHOULD_YIELD					0
#define configUSE_CO_ROUTINES 					1
#define configUSE_MUTEXES						1
#define configMAX_CO_ROUTINE_PRIORITIES         ( 2 )

#define configUSE_COUNTING_SEMAPHORES 	        0
#define configUSE_ALTERNATIVE_API 		        0
#ifndef WIN32
#define configCHECK_FOR_STACK_OVERFLOW	       	0
#else
#define configCHECK_FOR_STACK_OVERFLOW	       	0
#endif


#define configUSE_RECURSIVE_MUTEXES		        1
#define configQUEUE_REGISTRY_SIZE		        30
#define configGENERATE_RUN_TIME_STATS	        0
#define configUSE_TIMERS                        1



/* Set the following definitions to 1 to include the API function, or zero
 to exclude the API function. */
#define INCLUDE_xEventGroupSetBitFromISR      		1
#define INCLUDE_xTimerPendFunctionCall          	1
#define INCLUDE_xTimerPendFunctionCallFromISR  		0
#define INCLUDE_vTaskPrioritySet			        1
#define INCLUDE_uxTaskPriorityGet			        0
#define INCLUDE_vTaskDelete					        1
#define INCLUDE_vTaskCleanUpResources		        0
#define INCLUDE_vTaskSuspend				        1
#define INCLUDE_vTaskDelayUntil				        0
#define INCLUDE_vTaskDelay					        1
#define INCLUDE_uxTaskGetStackHighWaterMark	        0
#define INCLUDE_xTaskGetIdleTaskHandle              1
#define INCLUDE_xTaskGetSchedulerState				0
#define INCLUDE_xSemaphoreGetMutexHolder			1

#define configTIMER_TASK_PRIORITY					PRIORITY_CRITICAL
#define configTIMER_QUEUE_LENGTH					20
#define configTIMER_TASK_STACK_DEPTH				( configMINIMAL_STACK_SIZE * 1 )

/* Use the system definition, if there is one */
#ifdef __NVIC_PRIO_BITS
#define configPRIO_BITS       __NVIC_PRIO_BITS
#else
#define configPRIO_BITS       5        /* 32 priority levels */
#endif

/* The lowest priority. */
#define configKERNEL_INTERRUPT_PRIORITY 	    ( 31 << (8 - configPRIO_BITS) )
#define configMAX_SYSCALL_INTERRUPT_PRIORITY 	( 5 << (8 - configPRIO_BITS) )

#define configDMA_INTERRUPT_PRIORITY              6
#define configUART1_INTERRUPT_PRIORITY            6  
#define configUART0_INTERRUPT_PRIORITY            10
#define configGPIO_INTERRUPT_PRIORITY             8

#define configTIMER_INTERRUPT_PRIORITY             10

#endif /* FREERTOS_CONFIG_H */
