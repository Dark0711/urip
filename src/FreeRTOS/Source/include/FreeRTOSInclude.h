#ifndef _FREERTOSINCLUDE_H
#define _FREERTOSINCLUDE_H

#ifndef WIN32
#include <stdbool.h>
#endif
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "PrioritiesTask.h"
#include "semphr.h"
#include "mutex.h"
#include "event_groups.h"
#include "portmacro.h"
#ifndef WIN32
extern bool StartFreeRTOS;
#endif
#endif
