#ifndef __PRIORITIESTASK_H
#define __PRIORITIESTASK_H


#define PRIORITY_IDLE           (0)
#define PRIORITY_VERY_LOW       (1)
#define PRIORITY_LOW            (2)
#define PRIORITY_NORMAL         (3)
#define PRIORITY_HIGH           (4)
#define PRIORITY_VERY_HIGH      (5)
#define PRIORITY_CRITICAL       (6)

#endif
