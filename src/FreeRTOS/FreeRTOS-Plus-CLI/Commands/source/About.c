#include "UserConfig.h"
#include "RegisterUserCommand.h"
#include "printf_stdarg.h"
/************************************************************************/
/*                ������� about                                         */
/************************************************************************/

static CLI_STATUS prvAboutCommand( uint8_t *pcWriteBuffer, uint16_t xWriteBufferLen, const uint8_t *pcCommandString );
const CLI_Command_Definition_t xAboutCommand =
{
	"about",
	"������:\"about\"\r\n��������:������� ���������� � ����������� �����������\r\n",
	prvAboutCommand,
	0
};

static CLI_STATUS prvAboutCommand( uint8_t *pcWriteBuffer, uint16_t xWriteBufferLen, const uint8_t *pcCommandString )
{
	CLI_STATUS xReturn = CLI_UNKNOWNERROR;  //������
	if(pcWriteBuffer != NULL)
	{
		if(xWriteBufferLen != 0) 
		{
			sprintf(( char * ) (pcWriteBuffer),"%s",BootLoaderCompileVersion);
			xReturn = CLI_COMPLITE;		
		}
	}
	return xReturn;
}
