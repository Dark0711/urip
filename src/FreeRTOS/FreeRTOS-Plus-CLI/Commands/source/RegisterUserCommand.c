#include "RegisterUserCommand.h"


extern const CLI_Command_Definition_t xAboutCommand;
extern const CLI_Command_Definition_t xClsCommand;
extern const CLI_Command_Definition_t xMoveCursorCommand;
extern const CLI_Command_Definition_t xLogCommand;

void RegisterUserCommand(void)
{
	FreeRTOS_CLIRegisterCommand(&xAboutCommand);
	FreeRTOS_CLIRegisterCommand(&xClsCommand);
	//FreeRTOS_CLIRegisterCommand(&xMoveCursorCommand);
	//FreeRTOS_CLIRegisterCommand(&xLogCommand);
}
