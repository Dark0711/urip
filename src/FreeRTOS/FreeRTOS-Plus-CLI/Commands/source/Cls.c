#include "UserConfig.h"
#include "RegisterUserCommand.h"
#include "printf_stdarg.h"

#define CLSNUM 50
/************************************************************************/
/*                ������� about                                         */
/************************************************************************/

static CLI_STATUS prvClsCommand( uint8_t *pcWriteBuffer, uint16_t xWriteBufferLen, const uint8_t *pcCommandString );
const CLI_Command_Definition_t xClsCommand =
{
	"cls",
	"������:\"cls\"\r\n��������:������ ����� �������\r\n",
	prvClsCommand,
	0
};

static CLI_STATUS prvClsCommand( uint8_t *pcWriteBuffer, uint16_t xWriteBufferLen, const uint8_t *pcCommandString )
{
	CLI_STATUS xReturn = CLI_UNKNOWNERROR;  //������
	if(pcWriteBuffer != NULL)
	{
		if(xWriteBufferLen != 0)
		{
			*pcWriteBuffer = '\t';
			for(uint8_t i =0; i< CLSNUM; i++)
			{
				*++pcWriteBuffer = '\n';
			}
			xReturn = CLI_COMPLITE;
		}
	}
	return xReturn;
}
