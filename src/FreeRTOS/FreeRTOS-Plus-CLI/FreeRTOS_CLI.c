/* Standard includes. */
#include <string.h>

//#include <stdio.h>
//#include <stdlib.h>
#include "printf_stdarg.h"

#include "FreeRTOS.h"
#include "task.h"

/* Utils includes. */
#include "FreeRTOS_CLI.h"

typedef struct xCOMMAND_INPUT_LIST //�������� �������
{
	const CLI_Command_Definition_t *pxCommandLineDefinition;
	struct xCOMMAND_INPUT_LIST *pxNext;
} CLI_Definition_List_Item_t;

/*
 * Return the number of parameters that follow the command name.
 */
static uint8_t prvGetNumberOfParameters(const uint8_t* pcCommandString);

/************************************************************************/
/*                ���������� ������������ help                          */
/************************************************************************/
static CLI_STATUS prvHelpCommandExpansion(uint8_t *pcWriteBuffer, uint16_t xWriteBufferLen, const uint8_t *pcCommandString);
static const CLI_Command_Definition_t xHelpCommandExpansion =
		{
				"help",
				"������:\"help\" ��� \"help [�������� 1] ... [�������� N]\"\r\n��������:���������� ���������� �� ���������\r\n",
				prvHelpCommandExpansion,
				-1
		};

static CLI_Definition_List_Item_t xRegisteredCommands =
		{
				&xHelpCommandExpansion,
				NULL
		};

//static uint8_t cOutputBuffer[configCOMMAND_INT_MAX_OUTPUT_SIZE]; //����� ��� ������

/*-----------------------------------------------------------*/

CLI_STATUS FreeRTOS_CLIRegisterCommand(const CLI_Command_Definition_t* const pxCommandToRegister)
{
	static CLI_Definition_List_Item_t *pxLastCommandInList = &xRegisteredCommands;
	CLI_Definition_List_Item_t *pxNewListItem;
	CLI_STATUS xReturn = CLI_NOCOMMAND;

	/* Check the parameter is not NULL. */
	configASSERT( pxCommandToRegister );

	/* Create a new list item that will reference the command being registered. */
	pxNewListItem = (CLI_Definition_List_Item_t *) pvPortMalloc(sizeof(CLI_Definition_List_Item_t));
	configASSERT( pxNewListItem );

	if (pxNewListItem != NULL)
	{
		taskENTER_CRITICAL();
		{
			/* Reference the command being registered from the newly created
			 list item. */
			pxNewListItem->pxCommandLineDefinition = pxCommandToRegister;

			/* The new list item will get added to the end of the list, so
			 pxNext has nowhere to point. */
			pxNewListItem->pxNext = NULL;

			/* Add the newly created list item to the end of the already existing
			 list. */
			pxLastCommandInList->pxNext = pxNewListItem;

			/* Set the end of list marker to the new list item. */
			pxLastCommandInList = pxNewListItem;
		}
		taskEXIT_CRITICAL();
		xReturn = CLI_REGISTEROK;
	}
	return xReturn;
}
/*-----------------------------------------------------------*/

CLI_STATUS FreeRTOS_CLIProcessCommand(const uint8_t* const pcCommandInput, uint8_t * pcWriteBuffer, uint16_t xWriteBufferLen)
{
	const CLI_Definition_List_Item_t *pxCommand = NULL;
	CLI_STATUS xReturn = CLI_UNCOWN;
	const uint8_t *pcRegisteredCommandString;
	size_t xCommandStringLength;

	/* Note:  This function is not re-entrant.  It must not be called from more
	 thank one task. */

	if (pxCommand == NULL)
	{
		/* Search for the command string in the list of registered commands. */
		for (pxCommand = &xRegisteredCommands; pxCommand != NULL;
				pxCommand = pxCommand->pxNext)
		{
			pcRegisteredCommandString = pxCommand->pxCommandLineDefinition->pcCommand;
			xCommandStringLength = strlen((const char *) pcRegisteredCommandString);

			/* To ensure the string lengths match exactly, so as not to pick up
			 a sub-string of a longer command, check the byte after the expected
			 end of the string is either the end of the string or a space before
			 a parameter. */
			if ((pcCommandInput[xCommandStringLength] == ' ') || (pcCommandInput[xCommandStringLength] == 0x00))
			{
				if (strncmp((const char *) pcCommandInput, (const char *) pcRegisteredCommandString, xCommandStringLength) == 0)
				{
					/* The command has been found.  Check it has the expected
					 number of parameters.  If cExpectedNumberOfParameters is -1,
					 then there could be a variable number of parameters and no
					 check is made. */

					//���������� �������� ����� ��������� ��� � �� ������������
					if (pxCommand->pxCommandLineDefinition->cExpectedNumberOfParameters == 0 && prvGetNumberOfParameters(pcCommandInput) > 0)
					{
						xReturn = CLI_MANYPARAM;
					}
					//��������� ������������
					else if (pxCommand->pxCommandLineDefinition->cExpectedNumberOfParameters > 0)
					{

						if (prvGetNumberOfParameters(pcCommandInput) < pxCommand->pxCommandLineDefinition->cExpectedNumberOfParameters)
						{
							xReturn = CLI_NOPARAM; //���������� �� ���������� ��� �� �������
						}
						else if (prvGetNumberOfParameters(pcCommandInput) > pxCommand->pxCommandLineDefinition->cExpectedNumberOfParameters)
						{
							xReturn = CLI_MANYPARAM; //����� ����������
						}

					}
					break;
				}
			}
		}
	}

	if ((pxCommand != NULL) && (xReturn == CLI_NOPARAM)) //���������� �� ���������� ��� �� �������
	{
		snprintf((char *) pcWriteBuffer, xWriteBufferLen, "--->�� ������� ���������� ������� \"%s\"\r\n������:%s", pxCommand->pxCommandLineDefinition->pcCommand, pxCommand->pxCommandLineDefinition->pcHelpString);
		pxCommand = NULL;
	}
	else if ((pxCommand != NULL) && (xReturn == CLI_MANYPARAM))
	{
		if (pxCommand->pxCommandLineDefinition->cExpectedNumberOfParameters) //��������� ����
		{
			snprintf((char *) pcWriteBuffer, xWriteBufferLen, "--->������� ����� ���������� ������� \"%s\"\r\n������:%s", pxCommand->pxCommandLineDefinition->pcCommand, pxCommand->pxCommandLineDefinition->pcHelpString);
		}
		else
		{
			snprintf((char *) pcWriteBuffer, xWriteBufferLen, "--->������� %s �� ���������� ����������\r\n", pxCommand->pxCommandLineDefinition->pcCommand);
		}
		pxCommand = NULL;
	}
	else if (pxCommand != NULL)
	{
		/* Call the callback function that is registered to this command. */
		xReturn = pxCommand->pxCommandLineDefinition->pxCommandInterpreter(pcWriteBuffer, xWriteBufferLen, pcCommandInput);

		/* If xReturn is pdFALSE, then no further strings will be returned
		 after this one, and	pxCommand can be reset to NULL ready to search
		 for the next entered command. */
		if (xReturn == CLI_ERRORPARAM) //��������� ���������
		{
			snprintf((char *) pcWriteBuffer, xWriteBufferLen, "--->��������(�) ��������� ��� ��������:\"%s\"\r\n%s", pxCommand->pxCommandLineDefinition->pcCommand, pxCommand->pxCommandLineDefinition->pcHelpString);
		}
		else if (xReturn == CLI_UNKNOWNERROR) //�������������� ������
		{
			strncpy((char *) pcWriteBuffer, "--->�������������� ������\r\n", xWriteBufferLen);
		}
		else if (xReturn == CLI_COMPLITE) //���������
		{

		}
		else
		{
			strncpy((char *) pcWriteBuffer, "--->����������� ������ ����������!!!\r\n", xWriteBufferLen);
		}
		pxCommand = NULL; //�������� ���������
	}
	else
	{
		/* pxCommand was NULL, the command was not found. */
		strncpy((char *) pcWriteBuffer, (const char *) "--->������� �� ����������. ������� \"help\" ����� ����������� ������ ��������� ������.\r\n", xWriteBufferLen);
		xReturn = CLI_NOCOMMAND; //������� ������������
	}

	return xReturn;
}
/*-----------------------------------------------------------*/

/*uint8_t *FreeRTOS_CLIGetOutputBuffer(void)
{
	return cOutputBuffer;
}*/
/*-----------------------------------------------------------*/

const uint8_t *FreeRTOS_CLIGetParameter(const uint8_t *pcCommandString, uint8_t uxWantedParameter, uint16_t *pxParameterStringLength)
{
	uint8_t uxParametersFound = 0;
	const uint8_t *pcReturn = NULL;

	*pxParameterStringLength = 0;

	while (uxParametersFound < uxWantedParameter)
	{
		/* Index the character pointer past the current word.  If this is the start
		 of the command string then the first word is the command itself. */
		while (((*pcCommandString) != 0x00) && ((*pcCommandString) != ' '))
		{
			pcCommandString++;
		}

		/* Find the start of the next string. */
		while (((*pcCommandString) != 0x00) && ((*pcCommandString) == ' '))
		{
			pcCommandString++;
		}

		/* Was a string found? */
		if (*pcCommandString != 0x00)
		{
			/* Is this the start of the required parameter? */
			uxParametersFound++;

			if (uxParametersFound == uxWantedParameter)
			{
				/* How long is the parameter? */
				pcReturn = pcCommandString;
				while (((*pcCommandString) != 0x00) && ((*pcCommandString) != ' '))
				{
					(*pxParameterStringLength)++;
					pcCommandString++;
				}

				if (*pxParameterStringLength == 0)
				{
					pcReturn = NULL;
				}

				break;
			}
		}
		else
		{
			break;
		}
	}

	return pcReturn;
}
/*-----------------------------------------------------------*/

static uint8_t prvGetNumberOfParameters(const uint8_t * pcCommandString)
{
	uint8_t cParameters = 0;
	CLI_STATUS xLastCharacterWasSpace = CLI_NOCOMMAND;

	/* Count the number of space delimited words in pcCommandString. */
	while (*pcCommandString != 0x00)
	{
		if ((*pcCommandString) == ' ')
		{
			if (xLastCharacterWasSpace != CLI_OK)
			{
				cParameters++;
				xLastCharacterWasSpace = CLI_OK;
			}
		}
		else
		{
			xLastCharacterWasSpace = CLI_NOCOMMAND;
		}

		pcCommandString++;
	}

	/* If the command string ended with spaces, then there will have been too
	 many parameters counted. */
	if (xLastCharacterWasSpace == CLI_OK)
	{
		cParameters--;
	}

	/* The value returned is one less than the number of space delimited words,
	 as the first word should be the command itself. */
	return cParameters;
}

/*************************************************************************
 * ��� �������:         prvHelpCommandExpansion
 * ���������:           int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString
 * ����������:          FreeRTOS_CLI_STATUS - ������
 * ��������:            ������� ����������� �������� HELP
 *************************************************************************/
static CLI_STATUS prvHelpCommandExpansion(uint8_t *pcWriteBuffer, uint16_t xWriteBufferLen, const uint8_t *pcCommandString)
{
	CLI_STATUS xReturn = CLI_UNKNOWNERROR;  //������
	const CLI_Definition_List_Item_t * pxCommand = NULL;
	int NumberOfCommands = 0, Counter = 0; // ���������� �������,������� ������
	uint16_t LenghtStr = 0, lParameterStringLength = 0; //����� ������, ����� ��������
	int8_t *pcParameter; //�������� ��������
	uint8_t i;
	//������� ���� �� ��������� ?
	NumberOfCommands = prvGetNumberOfParameters(pcCommandString); //���������� �������
	pxCommand = &xRegisteredCommands; //������� �������

	if (pxCommand != NULL)
	{
		if (NumberOfCommands) //���� ��������
		{
			for (i = 0; i < NumberOfCommands; i++)
			{
				pcParameter = (int8_t *) FreeRTOS_CLIGetParameter(pcCommandString, i + 1, &lParameterStringLength);
				if (pcParameter != NULL)
				{
					for (pxCommand = &xRegisteredCommands; pxCommand != NULL;
							pxCommand = pxCommand->pxNext)
					{
						if (strncmp((const char *) pxCommand->pxCommandLineDefinition->pcCommand, (char const*) pcParameter, strlen((const char *) pxCommand->pxCommandLineDefinition->pcCommand)) == 0) //����� ���� ��������
						{
							Counter++;
							LenghtStr += snprintf((char *) (pcWriteBuffer + LenghtStr), xWriteBufferLen - LenghtStr, "%u.%s\r\n", Counter, pxCommand->pxCommandLineDefinition->pcCommand);
							LenghtStr += snprintf((char *) (pcWriteBuffer + LenghtStr), xWriteBufferLen - LenghtStr, "%s\r\n", pxCommand->pxCommandLineDefinition->pcHelpString);
							xReturn = CLI_COMPLITE;
							break;
						}
						if (pxCommand->pxNext == NULL)
						{
							Counter++;
							LenghtStr += snprintf((char *) (pcWriteBuffer + LenghtStr), xWriteBufferLen - LenghtStr, "%u.�� ��������� ���������\r\n", Counter);
							xReturn = CLI_COMPLITE;
						}
					}
				}
			}
		}
		else
		{
			//���������� ������ �������
			LenghtStr += snprintf((char *) pcWriteBuffer, xWriteBufferLen, "������ �������:\r\n");
			for (pxCommand = &xRegisteredCommands; pxCommand != NULL;
					pxCommand = pxCommand->pxNext)
			{
				Counter++;
				LenghtStr += snprintf((char *) (pcWriteBuffer + LenghtStr), xWriteBufferLen - LenghtStr, "%u.%s\r\n", Counter, pxCommand->pxCommandLineDefinition->pcCommand);
			}
			if (pxCommand == NULL)
			{
				xReturn = CLI_COMPLITE;
			}
			else
			{
				xReturn = CLI_UNKNOWNERROR;
			}
		}
	}
	return xReturn;
}
