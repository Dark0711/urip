/*
 * Memory.c
 *
 *  ������: 17 ���. 2015 �.
 *  �����:  �������� �.�
 */

#include "Memory.h"
#include <stdint.h>
#include "tlsf.h"
#include "UserConfig.h"
#include "FreeRTOSInclude.h"

#include <string.h>

#define POOL_SIZE 1024 * 1024* 5//10 Mb

#pragma location="TLSF_MEMORY"
static __no_init uint8_t pool[POOL_SIZE];

static void InitMemoryTask(void* P);

void Init_Memory()
{
	xTaskCreate(InitMemoryTask, "InitMemoryTask", __MemoryInitTaskStackSize, NULL, __MemoryInitTaskPriority, NULL);
}

static void InitMemoryTask(void* P)
{
	for(;;)
	{
		init_memory_pool(POOL_SIZE, pool); //�������������� ������
		vTaskDelete(NULL);
	}
}


char* tlsf_strdup(const char *Text)
{
	int LenghtText = strlen(Text);
	if(LenghtText == 0 ) return NULL;
	char* CopyText = tlsf_malloc((LenghtText+1)*sizeof(char));
	memcpy(CopyText,Text,LenghtText);
	return CopyText;
}
