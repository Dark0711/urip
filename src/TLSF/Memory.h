/*
 * Memory.h
 *
 *  ������: 17 ���. 2015 �.
 *  �����:  �������� �.�
 */

#ifndef TLSF_MEMORY_H_
#define TLSF_MEMORY_H_

#ifdef __cplusplus
extern "C" {
#endif

	void Init_Memory();
	char* tlsf_strdup(const char *Text); // �������� ������ � �������� ���� �����, ��������� ���������

#ifdef __cplusplus
}
#endif

#endif /* TLSF_MEMORY_H_ */
