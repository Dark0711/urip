#ifndef _TARGET_H_
#define _TARGET_H_

#include <FreeRTOSInclude.h>

#define TLSF_MLOCK_T            	MutexHandle_t
#define TLSF_CREATE_LOCK(Mutex)     vMutexCreate(*Mutex)
#define TLSF_DESTROY_LOCK(Mutex)    vMutexDelete(*Mutex)
#define TLSF_ACQUIRE_LOCK(Mutex)    if(*Mutex != 0) xMutexTake(*Mutex)
#define TLSF_RELEASE_LOCK(Mutex)    if(*Mutex != 0) xMutexGive(*Mutex)

#endif
