/*************************************************************************
 * 		���: WindowsInterface.c
 *
 * 		��������
 *		������������� ������������ ����������
 *
 *		�����
 *		�������� �.�
 *
 ****************************************************************************/
//===========================================================================
//===========================������==========================================
//===========================================================================
#include "WindowsInterface.hpp"
#include "tft_lcd.h"
#include "initperefirial.h"


#include "nand_addition.h"
#include "ff.h"
#include "usb_addition.h"
#ifndef WIN32

#include "class_cli.hpp"
#include "CustomCommands.h"
#endif
//===========================================================================
//===========================���������=======================================
//===========================================================================

//---------------------------------------------------------------------------
//---------------------------��������� ����������----------------------------
//---------------------------------------------------------------------------
static TaskHandle_t xGUI_Task_Handle;
#ifndef WIN32
CLI CommadLineInterface; //����� �������
#endif

#ifndef WIN32
#pragma location="USER_RAM"
static __no_init uint8_t TempText[1024];
#else
static uint8_t TempText[1024];
static uint8_t Compare[1024];
#endif
//---------------------------------------------------------------------------
//---------------------------��������� �������-------------------------------
//---------------------------------------------------------------------------
static void StartGUI_Task(void* Param);
static void GUI_Task(void *pvParameters);
static void DrawLogo(void);
static void FontInit_Task(void *pvParameters);
//---------------------------------------------------------------------------
//---------------------------���������� ����������---------------------------
//---------------------------------------------------------------------------
WM_HWIN hCerrentWindows;
QueueHandle_t WindowEvent_QueueHandle;
TaskHandle_t xTooch_Task_Handle;
#ifndef WIN32
extern tHelp HelpCommand;
extern tAbout AboutCommand;
#endif

GUI_FONT Font_Small;
GUI_FONT Font_Averge;
GUI_FONT Font_Big;

static GUI_TTF_CS CsFont_Small;
static GUI_TTF_CS CsFont_Averge;
static GUI_TTF_CS CsFont_Big;

static GUI_TTF_DATA Data;
//---------------------------------------------------------------------------
//---------------------------������� ����������---------------------------
//---------------------------------------------------------------------------
extern "C" GUI_CONST_STORAGE GUI_BITMAP bmp_logovector;
extern "C" FATFS NAND_fs;
extern "C" FATFS NANDRESORCE_fs;
/***************************************************************************
 *   	��� �������: DrawLogo_and_StartGUI
 *   	����������:  void
 *      ���������:   void
 *   	��������:    ������ ��������
 ****************************************************************************/
static void DrawLogo(void)
{
	GUI_SetBkColor(GUI_WHITE);
	GUI_Clear();
	GUI_DrawBitmap(&bmp_logovector, (LCD_WIDTH / 2) - bmp_logovector.XSize / 2, (LCD_HEIGHT / 2) - bmp_logovector.YSize / 2);
	GUI_SetColor(GUI_BLACK);
	GUI_SetBkColor(GUI_WHITE);
}
/***************************************************************************
 *   	��� �������: CreateWindowsInterface
 *		���������:   void
 *   	����������:  void
 *   	��������:    ������ ������ ������������� GUI
 ****************************************************************************/
void CreateWindowsInterface()
{
	xTaskCreate(StartGUI_Task, "StartGUI_Task", __StartGUITaskStackSize, NULL, __StartGUITaskPriority, NULL); //������ ������ � GUI
}
/***************************************************************************
 *   	��� ������: StartGUI_Task
 *   	��������:   ������ ������������� GUI
 ****************************************************************************/
static void StartGUI_Task(void* Param)
{
	//DEVICE_RESULT Result;QueueWindowEvent_AddMessage
	for (;;)
	{
#ifndef WIN32		
		init_perefirial(); //�������� ���������
		
		CommadLineInterface.RegisterCommand(HelpCommand); //����������� ����� ������ �������������� UART0
		CommadLineInterface.RegisterCommand(AboutCommand);
#endif
		GUI_Init();
		WM_MULTIBUF_Enable(TRUE);
		WM_SetCreateFlags(WM_CF_MEMDEV);
		GUI_SetFont(GUI_FONT_START);
		GUI_Clear(); //������ �����
		DrawLogo(); //����
		GUI_DispString("������ ������������ �����������: "USER_VERSION"\n");
		GUI_DispString("������������� �������� �������...\n");

		USB_TaskInit();	  //USB-Host � �������� �������		
		NAND_Init();	  //NAND Flash
		NANDRESORCE_Init();//NAND FLASH ��������� ����� � �������� � ������ �������

		//f_mkfs("NANDRESORCE:", 0, 0);
		//f_mkfs("NAND:", 0, 0);
		//��������� ������ ������������� ������� ������ �������� � ������ � ���������� �����������
		xTaskCreate(FontInit_Task, "GUI", __FontInitTasStackSize, NULL, __FontInitTaskPriority, NULL); //������ ������ � GUI
		vTaskDelete(NULL);
	}
}
/***************************************************************************
 *   	��� �������: FontInit_Task
 *		���������:   void
 *   	����������:  void *pvParameters
 *   	��������:    ������ ������������� �������
 ****************************************************************************/
static void FontInit_Task(void *pvParameters)
{
	FRESULT Result;
	FIL file_usb;
	FIL file_nand;
	UINT FileRead;
	UINT FileWrite;
	uint8_t* pBuff;

	for (;;)
	{
		GUI_DispString("������������� �������...\n");
		//��������� ������� ����� �� ���������� ��������
		//f_mkfs("NAND:", 0, 0);
		//f_mkfs("NANDRESORCE:", 0, 0);

		//while (1);
		Result = f_open(&file_nand, "NANDRESORCE:Font.ttf", FA_READ);
		if (Result != FR_OK) //��� ������ ����� ����������� ��� � ������
		{
			GUI_DispString("������ �� ����������\n");
			GUI_DispString("�������� ������� �������� � ����� Font.ttf ��� �����������\n");
			do
			{
				Result = f_open(&file_usb, "USB:Font.ttf", FA_READ);
			}
			while (Result != FR_OK);
			//������ ���� �� NAND Flash
			do
			{
				Result = f_open(&file_nand, "NANDRESORCE:Font.ttf", FA_READ | FA_CREATE_ALWAYS | FA_WRITE);
			}
			while (Result != FR_OK);
			GUI_DispString("�����������...\n");
			for (;;)
			{
				Result = f_read(&file_usb, TempText, sizeof(TempText), &FileRead);
				if (Result != FR_OK)
				{
					GUI_DispString("\n������ ������ �����\n");
					break;
				}
				Result = f_write(&file_nand, TempText, FileRead, &FileWrite);
				if (Result != FR_OK)
				{
					GUI_DispString("\n������ ������ �����\n");
					break;
				}
				if (FileWrite == 0)
				{
					GUI_DispString("\n����������� ��������� ����� �� ������\n");
					break;
				}
				if (f_eof(&file_usb))
				{
					GUI_DispString("������\n");
					break;
				}
			}
 			Result = f_close(&file_usb);
 			Result = f_close(&file_nand);
 			/*GUI_DispString("��������...\n");
 			do
 			{
 				Result = f_open(&file_usb, "USB:Font.ttf", FA_READ);
 			} 
 			while (Result != FR_OK);
 			//������ ���� �� NAND Flash
 			do
 			{
 				Result = f_open(&file_nand, "NANDRESORCE:Font.ttf", FA_READ | FA_WRITE);
 			}
 			while (Result != FR_OK);
 			//��������
 			for (;;)
 			{
 				Result = f_read(&file_usb, TempText, sizeof(TempText), &FileRead);
 				if (Result != FR_OK)
 				{
 					GUI_DispString("\n������ ������ �����\n");
 					break;
 				}
 				Result = f_read(&file_nand, Compare, sizeof(Compare), &FileWrite);
 				for (int ii = 0; ii < FileWrite;ii++)
 				{
 					if (Compare[ii] != TempText[ii])
 					{
 						GUI_DispString("\n������ �������� �����\n");
 						while (true);
 					}
 				}
 				if (FileWrite != FileRead)
 				{
 					GUI_DispString("\n������ �������� �����\n");
 					while (true);
 				}
 				if (f_eof(&file_usb))
 				{
 					GUI_DispString("������\n");
 					break;
 				}
 
 			}*/
			continue;
		}
		else
		{
			//�������� ����� � ������
			GUI_DispString("�������� ����� � ������...\n");
			Data.pData = calloc(f_size(&file_nand), sizeof(uint8_t));
			Data.NumBytes = f_size(&file_nand);
			pBuff = (uint8_t*) (Data.pData);
			for (;;)
			{
				Result = f_read(&file_nand, TempText, sizeof(TempText), &FileRead);
				if (Result != FR_OK)
				{
					GUI_DispString("������ ������ �����\n");
					break;
				}
				memcpy(pBuff, TempText, FileRead);
				pBuff += FileRead;
				if (f_eof(&file_nand))
				{
					break;
				}
			}
			Result = f_close(&file_nand);
		}
		CsFont_Small.pTTF = &Data;
		CsFont_Small.PixelHeight = 15;
		CsFont_Small.FaceIndex = 0;
// 
 		CsFont_Averge.pTTF = &Data;
 		CsFont_Averge.PixelHeight = 20;
		CsFont_Averge.FaceIndex = 0;
// 
 		CsFont_Big.pTTF = &Data;
 		CsFont_Big.PixelHeight = 30;
 		CsFont_Big.FaceIndex = 0;

		GUI_TTF_CreateFontAA(&Font_Small, &CsFont_Small);
 		GUI_TTF_CreateFontAA(&Font_Averge, &CsFont_Averge);
 		GUI_TTF_CreateFontAA(&Font_Big, &CsFont_Big);
// 
//		GUI_SetFont(&Font_Big);
		GUI_DispString("������");
//		BUTTON_SetDefaultFont(GUI_FONT_SMALL);
//	    DROPDOW+N_SetDefaultFont(GUI_FONT_AVERAGE);
		GUI_UC_SetEncodeUTF8();
		xTaskCreate(GUI_Task, "GUI", __GUITaskStackSize, NULL, __GUITaskPriority, &xGUI_Task_Handle); //������ ������ � GUI
		hCerrentWindows = CreateMainWinsow();

		vTaskDelete(NULL);
	}
}
/***************************************************************************
 *   	��� �������: GUI_Task
 *		���������:   void
 *   	����������:  void *pvParameters
 *   	��������:    ������ ���������� GUI
 ****************************************************************************/
static void GUI_Task(void *pvParameters)
{
	for (;;)
	{
		GUI_Exec();
		GUI_X_ExecIdle();
	}
}
/***************************************************************************
 *   	��� �������: FRAMEWIN_SetText_User
 *		���������:   (WM_MESSAGE * pMsg, char* Text
 *   	����������:  void
 *   	��������:    ������� ���� � �������������� �����������
 ****************************************************************************/
void FRAMEWIN_SetText_User(WM_HWIN hItem, const char* Text)
{
	FRAMEWIN_SetClientColor(hItem, GUI_BLACK);
	FRAMEWIN_SetFont(hItem, GUI_FONT_AVERAGE);
	FRAMEWIN_SetTextAlign(hItem, GUI_TA_HCENTER | GUI_TA_VCENTER);
	FRAMEWIN_SetText(hItem, Text);
}

uint8_t * AsciitoUTF8(uint8_t * InText, uint8_t * Out)
{
	uint8_t* pText = InText;
	unsigned int Counter = 0;
//DEBUG_I("AsciitoUTF8\n");
	memset(TempText, 0, sizeof(TempText));
	if (0 == strlen((char*) InText)) return TempText;
	do
	{
		//DEBUG_I("0x%02X->",*pText);
		if (((*pText) >= 0xC0) && ((*pText) <= 0xEF))
		{
			TempText[Counter++] = 0xd0;
			TempText[Counter++] = (*pText) - 0x30;

		}
		else if (*pText >= 0xEF /*&& *pText <= 0xFF*/)
		{
			TempText[Counter++] = 0xd1;
			TempText[Counter++] = (*pText) - 0x70;
		}
		else if ((*pText) == 0xA8) //�
		{
			TempText[Counter++] = 0xd0;
			TempText[Counter++] = 0x81;
		}
		else if ((*pText) == 0xB8) //�
		{
			TempText[Counter++] = 0xd1;
			TempText[Counter++] = 0x91;
		}
		else
		{
			TempText[Counter++] = (*pText);
		}
		//DEBUG_I("0x%02X ",TempText[Counter-1]);
		//pText++;
	}
	while (*(pText++));

	if (Out != NULL)
	{
		memset(Out, 0, strlen((char*) Out));
		memcpy(Out, TempText, strlen((char*) TempText));
	}
	return TempText;

//DEBUG_I("\n");
}
/*****************************����� �����****************************************/

