/*
 * cTetsDialogWindow.cpp
 *
 *  ������: 09 ���� 2015 �.
 *  �����:  �������� �.�
 */

#include <Windows/cTetsDialogWindow.h>

enum
{
	ID_FRAMEWIN_0 = (GUI_ID_USER + 0x00),
	ID_BUTTON_0,
	ID_BUTTON_1,
	ID_TEXT_0,
	ID_TEXT_1,
	ID_PROGRESSBAR_0,
	ID_PROGRESSBAR_1,
	ID_TIMER_0,
};
//extern WM_HWIN hMainWinsows,hOperatingModeWindow;
//***************************************************************************/
/****************************��������������� ���������**********************/
/***************************************************************************/
#define BUTTON_Y_POS 176
#define TEXT_Y_POS BUTTON_Y_POS + 128
//---------------------------------------------------------------------------
//---------------------------��������� ����������----------------------------
//---------------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] =
		{
				{ FRAMEWIN_CreateIndirect, "MainWinsows", ID_FRAMEWIN_0, 0, 0, 800, 480, 0, 100, 0 },
				{ BUTTON_CreateIndirect, "", ID_BUTTON_0, 208, BUTTON_Y_POS, 128, 128, 0, 0, 0 },
				{ BUTTON_CreateIndirect, "", ID_BUTTON_1, 464, BUTTON_Y_POS, 128, 128, 0, 0, 0 },
				{ TEXT_CreateIndirect, "Text", ID_TEXT_0, 208, TEXT_Y_POS, 128, 45, 0, 0, 0 },
				{ TEXT_CreateIndirect, "Text", ID_TEXT_1, 464, TEXT_Y_POS, 128, 45, 0, 0, 0 },
				{ PROGBAR_CreateIndirect, "", ID_PROGRESSBAR_0, 108, 30, 250, 45, 0, 0, 0 },
				{ PROGBAR_CreateIndirect, "", ID_PROGRESSBAR_1, 108, 100, 250, 45, 0, 0, 0 },

		};

cTetsDialogWindow::cTetsDialogWindow() :
		cWindow()
{
	CreateWindow(_aDialogCreate, ID_PROGRESSBAR_1);
}
void cTetsDialogWindow::Callback(WM_MESSAGE *pMsg)
{
	WM_HWIN hItem;
	int Id, NCode;
	static int ProgressCounter = 0;
	switch (pMsg->MsgId)
	{
		case WM_INIT_DIALOG:
			{

			FRAMEWIN_SetText_User(pMsg->hWin, "������� ����");
			hItem = WM_GetDialogItem(pMsg->hWin, ID_TEXT_0);
			TEXT_SetFont(hItem, GUI_FONT_SMALL);
			TEXT_SetTextColor(hItem, GUI_RED);
			TEXT_SetTextAlign(hItem, GUI_TA_HCENTER | GUI_TA_VCENTER);
			TEXT_SetText(hItem, "����� ������\n������");
			hItem = WM_GetDialogItem(pMsg->hWin, ID_TEXT_1);
			TEXT_SetFont(hItem, GUI_FONT_SMALL);
			TEXT_SetTextColor(hItem, GUI_RED);
			TEXT_SetTextAlign(hItem, GUI_TA_HCENTER | GUI_TA_VCENTER);
			TEXT_SetText(hItem, "���������\n�������");

			hItem = WM_GetDialogItem(pMsg->hWin, ID_PROGRESSBAR_0);
			PROGBAR_SetMinMax(hItem, 0, 100);
			PROGBAR_SetSkin(hItem, PROGBAR_SKIN_FLEX);
			hItem = WM_GetDialogItem(pMsg->hWin, ID_PROGRESSBAR_1);
			PROGBAR_SetMinMax(hItem, 0, 200);

			WM_CreateTimer(WM_GetClientWindow(pMsg->hWin), ID_TIMER_0, 100, 0);

			//hItem = WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0);
			//WM_SetFocus(hItem);

			break;
		}
			/*case WM_POST_PAINT:
			 {

			 break;
			 }*/
		case WM_TIMER:
			{
			hItem = WM_GetDialogItem(pMsg->hWin, ID_PROGRESSBAR_0);
			PROGBAR_SetValue(hItem, ProgressCounter++);
			hItem = WM_GetDialogItem(pMsg->hWin, ID_PROGRESSBAR_1);
			PROGBAR_SetValue(hItem, ProgressCounter);
			if (ProgressCounter == 100) ProgressCounter = 0;
			WM_RestartTimer(pMsg->Data.v, 100);
			break;
		}
		case WM_NOTIFY_PARENT:
			{
			Id = WM_GetId(pMsg->hWinSrc);
			NCode = pMsg->Data.v;
			switch (Id)
			{
				case ID_BUTTON_0:
					{
					if (NCode == WM_NOTIFICATION_RELEASED)
					{
						WM_DeleteWindow(pMsg->hWin); //������� ������
						//hCerrentWindows = CreateOperatingModeWindow(); //������ ���� ������ ������
					}
					break;
				}
				case ID_BUTTON_1:
					{
					if (NCode == WM_NOTIFICATION_RELEASED)
					{
						WM_DeleteWindow(pMsg->hWin); //������� ������
						//hCerrentWindows = CreateUserWindow(USERWIN_STATE_EEPROMEDIT, NULL); //������ ���� ������ ������
					}
					break;
				}

			}
			break;
		}

		default:
			WM_DefaultProc(pMsg);
			break;
	}

}
