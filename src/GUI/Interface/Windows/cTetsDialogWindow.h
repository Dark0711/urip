/*
 * cTetsDialogWindow.h
 *
 *  ������: 09 ���� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef GUI_INTERFACE_WINDOWS_CTETSDIALOGWINDOW_H_
#define GUI_INTERFACE_WINDOWS_CTETSDIALOGWINDOW_H_

#include <WindowsInterface.hpp>
#include "cWindow.hpp"
#include "DIALOG_Intern.h"


class cTetsDialogWindow: public cWindow
{
	private:


	public:
		void Callback( WM_MESSAGE * pMsg);
		cTetsDialogWindow();
		~cTetsDialogWindow()
		{
		}
		;
};

#endif /* GUI_INTERFACE_WINDOWS_CTETSDIALOGWINDOW_H_ */
