/*
 * cWindowDialog.hpp
 *
 *  ������: 09 ���� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef GUI_CPP_CWINDOW_HPP_
#define GUI_CPP_CWINDOW_HPP_

#include "GUI.h"
#include "WM_Intern.h"
#include "DIALOG_Intern.h"

class cWindow
{

	private:
		WM_HWIN hParent;
		WM_HWIN hIteamDialog;
	public:
		cWindow();
		void CreateWindow(const GUI_WIDGET_CREATE_INFO *paWidget,  int NumsWidgets);
		virtual void Callback( WM_MESSAGE * pMsg)=0;
		virtual ~cWindow(){}
};

#endif /* GUI_CPP_CWINDOW_HPP_ */
