/*
 * cWindow.cpp
 *
 *  ������: 09 ���� 2015 �.
 *  �����:  �������� �.�
 */

#include "cWindow.hpp"
#include "tft_lcd.h"

void cWindow_CALLBACK( WM_MESSAGE * pMsg)
{
	uint32_t pDest;
	cWindow* pWindow;
	WM_GetUserData(pMsg->hWinSrc,&pDest,sizeof(uint32_t));
	pWindow = reinterpret_cast<cWindow*>(pDest);
	pWindow->Callback(pMsg);
}

cWindow::cWindow():hIteamDialog(0)
{
	uint32_t Addr = reinterpret_cast<uint32_t>(reinterpret_cast<uint32_t*>(this));
	hParent = WM_CreateWindow(0,0,LCD_WIDTH, LCD_HEIGHT,WM_CF_SHOW,NULL,sizeof(uint32_t));
	WM_SetUserData(hParent,&Addr,sizeof(uint32_t));

}
void cWindow::CreateWindow(const GUI_WIDGET_CREATE_INFO *paWidget,  int NumsWidgets)
{
	hIteamDialog = GUI_CreateDialogBox(paWidget,NumsWidgets,cWindow_CALLBACK,hParent,0,0);
}
