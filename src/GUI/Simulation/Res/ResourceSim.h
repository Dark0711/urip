/*********************************************************************
*                SEGGER Microcontroller GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2013  SEGGER Microcontroller GmbH & Co. KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

** emWin V5.22 - Graphical user interface for embedded applications **
All  Intellectual Property rights  in the Software belongs to  SEGGER.
emWin is protected by  international copyright laws.  Knowledge of the
source code may not be used to write a similar product.  This file may
only be used in accordance with the following terms:

The software has been licensed to  NXP Semiconductors USA, Inc.  whose
registered  office  is  situated  at 411 E. Plumeria Drive, San  Jose,
CA 95134, USA  solely for  the  purposes  of  creating  libraries  for
NXPs M0, M3/M4 and  ARM7/9 processor-based  devices,  sublicensed  and
distributed under the terms and conditions of the NXP End User License
Agreement.
Full source code is available at: www.segger.com

We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : ResourceSim.h
Purpose     : Resource ID definitions for GUI Simulation
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*      Menu IDs
*/
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDR_MAINFRAME                   128

#define ID_EDIT_COPY                    500
#define ID_VIEW_LCD_0                   510
#define ID_VIEW_LCD_1                   511
#define ID_VIEW_LCD_2                   512
#define ID_VIEW_LCD_3                   513
#define ID_VIEW_LCD_4                   514
#define ID_VIEW_LCD_5                   515
#define ID_VIEW_LCDINFO_0               520
#define ID_VIEW_LCDINFO_1               521
#define ID_VIEW_LCDINFO_2               522
#define ID_VIEW_LCDINFO_3               523
#define ID_VIEW_LCDINFO_4               524
#define ID_VIEW_LCDINFO_5               525
#define ID_FILE_STARTAPPLICATION        530
#define ID_FILE_STOPAPPLICATION         540
#define ID_FILE_CONTINUEAPPLICATION     550
#define ID_VIEW_SYSINFO                 560
#define ID_VIEW_LOG                     570

/*********************************************************************
*
*      Menu / accelerator resource IDs
*/
#define IDC_SIMULATION                  109
#define IDC_SIMULATION_POPUP            110

/*********************************************************************
*
*      Control IDs
*/
#define IDC_COPYRIGHT                   1000
#define IDC_VERSION                     1001
#define IDC_APPNAME                     1002
#define IDC_STATIC                      -1

/*********************************************************************
*
*      Dialog resource IDs
*/
#define IDD_ABOUTBOX                    103

/*********************************************************************
*
*      Bitmap resource IDs
*/
#define IDB_LOGO                        133
#define IDB_DEVICE                      145
#define IDB_DEVICE1                     146

#define IDB_FRAME_N                     150
#define IDB_FRAME_S                     151
#define IDB_FRAME_O                     152
#define IDB_FRAME_W                     153
#define IDB_FRAME_NW                    154
#define IDB_FRAME_SW                    155
#define IDB_FRAME_SO                    156
#define IDB_FRAME_NO                    157

#define IDB_FRAME_OFF                   160
#define IDB_FRAME_OFF1                  161

/**************************** END-OF-FILE ***************************/
