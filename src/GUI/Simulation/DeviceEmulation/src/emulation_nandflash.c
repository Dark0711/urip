#include "emulation_nandflash.h"
#include <stdio.h>
#include <string.h>
#include "printf_stdarg.h"
#include <windows.h>
#include <io.h>

static char FileName[1024];

#define NANDFLASH_FULLPAGESIZE ((NANDFLASH_RW_PAGE_SIZE) + (NANDFLASH_SPARE_SIZE))

void Init_nandflash()
{
	char TempFilePath[1024];
	char FilePath[1024];
	char Disk[3];
	HMODULE hModule = GetModuleHandle(NULL);
	GetModuleFileName(hModule, FilePath, sizeof(FilePath));
	_splitpath(FilePath, Disk, TempFilePath, NULL, NULL);
	sprintf(FileName, "%s%s%s", Disk, TempFilePath, NAND_FILE_NAME);
	if (_access(FileName, 0) != 0) //��� �����
	{
		//��������
		FILE *fp = fopen(FileName, "wb");
		fseek(fp, NANDFLASH_NUMOF_BLOCK*NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE+1, SEEK_SET);
		fputc(0, fp);
		fclose(fp);
	}
}

uint32_t NandFlash_PageRead(const uint16_t blockNum, const uint8_t PageNum, uint8_t* Buff)
{
	FILE *fp = fopen(FileName, "rb+");
	if (fp == NULL) return false;
	if (fseek(fp, (blockNum*NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE) + (PageNum*NANDFLASH_FULLPAGESIZE), SEEK_SET) == 0)
	{
		if (fread(Buff, NANDFLASH_FULLPAGESIZE, 1, fp) > 0)
		{
			fclose(fp);
			return true;
		}
		fclose(fp);
		return false;
	}
	fclose(fp);
	return false;
}

uint32_t NandFlash_PageProgram(const uint16_t blockNum, const uint8_t PageNum, const uint8_t* Buff)
{
	long Seek = (blockNum*NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE) + (PageNum*NANDFLASH_FULLPAGESIZE);
	FILE *fp = fopen(FileName, "rb+");
	if (fp == NULL) return false;

	if (fseek(fp, Seek, SEEK_SET) == 0)
	{
		if (fwrite(Buff, NANDFLASH_FULLPAGESIZE, 1, fp) > 0)
		{
			fclose(fp);
			return true;
		}
		fclose(fp);
		return false;
	}
	fclose(fp);
	return false;
}

uint32_t NandFlash_BlockErase(const uint16_t blockNum)
{
	static char buff[NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE];
	FILE *fp = fopen(FileName, "rb+");
	if (fp == NULL) return false;
	if (fseek(fp, (blockNum*NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE), SEEK_SET) == 0)
	{
		memset(buff, 0x00, NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE);
		if (fwrite(buff, NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE, 1, fp) > 0)
		{
			fclose(fp);
			return true;
		}
		fclose(fp);
		return false;
	}
	return false;
}