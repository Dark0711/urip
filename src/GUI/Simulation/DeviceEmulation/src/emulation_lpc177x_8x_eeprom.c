#include "emulation_lpc177x_8x_eeprom.h"
#include <stdio.h>
#include <string.h>
#include "printf_stdarg.h"
#include <windows.h>
#include <io.h>

static char FileName[1024];

void EEPROM_Init(void)
{
	char FilePath[1024];
	char TempFilePath[1024];
	char Disk[3];
	HMODULE hModule = GetModuleHandle(NULL);
	GetModuleFileName(hModule, FilePath, sizeof(FilePath));
	_splitpath(FilePath, Disk, TempFilePath, NULL, NULL);
	sprintf(FileName, "%s%s%s", Disk, TempFilePath, EEPROM_FILE_NAME);
	if (_access(FileName, 0) != 0) //��� �����
	{
		//��������
		FILE *fp = fopen(FileName,"wb");
		fclose(fp);
	}
}

void EEPROM_Write(uint16_t page_offset, uint16_t page_address, void* data, EEPROM_Mode_Type mode, uint32_t size)
{
	FILE *fp = fopen(FileName, "rb+");
	if (fp == NULL) return;
	if (fseek(fp, (page_address*EEPROM_PAGE_SIZE) + (page_offset), SEEK_SET) == 0)
	{
		if (fwrite(data, size, 1, fp) > 0)
		{
			fclose(fp);
			return;
		}
		fclose(fp);
		return;
	}
	fclose(fp);
	return;
}

void EEPROM_Read(uint16_t page_offset, uint16_t page_address, void* data, EEPROM_Mode_Type mode, uint32_t size)
{
	//��������� ���� ��� ������

	FILE *fp;
	fp = fopen(FileName, "rb+");
	if (fp == NULL) return;
	if (fseek(fp, (page_address*EEPROM_PAGE_SIZE) + (page_offset), SEEK_SET) == 0)
	{
		if (fread(data, size, 1, fp) > 0)
		{
			fclose(fp);
			return;
		}
		fclose(fp);
		return;
	}
	fclose(fp);
	return;
}
void EEPROM_Erase(uint16_t page_offset)
{
	static FILE *fp;
	static char buff[EEPROM_PAGE_SIZE];
	fp = fopen(FileName, "rb+");
	if (fp == NULL) return;
	if (fseek(fp, (page_offset*EEPROM_PAGE_SIZE), SEEK_SET) == 0)
	{
		memset(buff, 0xff, EEPROM_PAGE_SIZE);
		if (fwrite(buff, EEPROM_PAGE_SIZE, 1, fp) > 0)
		{
			fclose(fp);
			return;
		}
		fclose(fp);
		return;
	}
	return;
}