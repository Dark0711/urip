#ifndef __NANDFLASH
#define __NANDFLASH

#include <stdint.h>
#include <stdbool.h>

#define NAND_FILE_NAME "NAND.emul"


#define NANDFLASH_RW_PAGE_SIZE		2048
#define NANDFLASH_PAGE_PER_BLOCK	64
#define NANDFLASH_SPARE_SIZE		64
#define NANDFLASH_NUMOF_BLOCK		1024


void Init_nandflash();
uint32_t NandFlash_PageRead(const uint16_t blockNum, const uint8_t PageNum, uint8_t* Buff);
uint32_t NandFlash_PageProgram(const uint16_t blockNum, const uint8_t PageNum, const uint8_t* Buff);
uint32_t NandFlash_BlockErase(const uint16_t blockNum);
#define NandFlash_ReadId() 0xDDCCBBAA 

#endif