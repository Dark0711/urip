#ifndef __LPC177X_8X_EEPROM_H_
#define __LPC177X_8X_EEPROM_H_

#include <stdint.h>

#define EEPROM_PAGE_SIZE				64
#define EEPROM_PAGE_NUM					63


#define EEPROM_FILE_NAME "EEPROM.emul"

typedef enum
{
	MODE_8_BIT = 0,
	MODE_16_BIT,
	MODE_32_BIT
}EEPROM_Mode_Type;

void EEPROM_Init(void);
void EEPROM_Write(uint16_t page_offset, uint16_t page_address, void* data, EEPROM_Mode_Type mode, uint32_t size);
void EEPROM_Read(uint16_t page_offset, uint16_t page_address, void* data, EEPROM_Mode_Type mode, uint32_t size);
void EEPROM_Erase(uint16_t address);

#endif /* __LPC177X_8X_EEPROM_H_ */