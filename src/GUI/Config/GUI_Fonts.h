#ifndef __GUI_FONTS_H
#define __GUI_FONTS_H

#include "GUI.h"

extern "C" GUI_CONST_STORAGE GUI_FONT GUI_FontUbuntu10;

extern "C" GUI_FONT Font_Small;
extern "C" GUI_FONT Font_Averge;
extern "C" GUI_FONT Font_Big;

#define GUI_FONT_SMALL		&Font_Small
#define GUI_FONT_AVERAGE	&Font_Averge
#define GUI_FONT_BIG		&Font_Big
#define GUI_FONT_START		&GUI_FontUbuntu10



#endif
