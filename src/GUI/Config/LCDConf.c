#include "GUI.h"
#include "GUIDRV_Lin.h"
#include "LCD.h"
#include "tft_lcd.h"
#ifndef WIN32
#include "LPC177x_8x.h"
#include "lpc177x_8x_gpio.h"
#else
#include <stdint.h>
#endif
/*********************************************************************
 *
 *       Layer configuration (to be modified)
 *
 **********************************************************************
 */


#define FB_XSIZE  LCD_WIDTH
#define FB_YSIZE  LCD_HEIGHT
//
// Physical display size
//
#define XSIZE_PHYS LCD_WIDTH
#define YSIZE_PHYS LCD_HEIGHT

//
// Virtual display size
//
#define VXSIZE_PHYS (XSIZE_PHYS)
#define VYSIZE_PHYS (YSIZE_PHYS)
//
// Color conversion
//
#define COLOR_CONVERSION GUICC_M565

//
// Display driver
//
#define DISPLAY_DRIVER GUIDRV_LIN_16

#define PIXEL_WIDTH 2
//
// Buffers / VScreens
//
#ifndef WIN32
#define NUM_BUFFERS  3 // Number of multiple buffers to be used
#else // !WIN32
#define NUM_BUFFERS  1 // Number of multiple buffers to be used
#endif

#define NUM_VSCREENS 1 // Number of virtual screens to be used

#ifndef WIN32
#pragma location="VRAM"
static __no_init U32 _aVRAM[NUM_BUFFERS * FB_XSIZE * FB_YSIZE / (4 / PIXEL_WIDTH)];
#else
static U32 _aVRAM[NUM_BUFFERS*FB_XSIZE * FB_YSIZE / (4 / PIXEL_WIDTH)];
#endif
#if GUI_SUPPORT_TOUCH
#ifndef GUI_TOUCH_ADS7846
static uint16_t _TouchY = 0, _TouchX = 0;
static volatile U8 _IsInited = 0;
#endif
#endif
/*********************************************************************
 *
 *       Configuration checking
 *
 **********************************************************************
 */
#define VRAM_ADDR_PHYS  (U32)&_aVRAM[0] // TBD by customer: This has to be the frame buffer start address

#ifndef   XSIZE_PHYS
#error Physical X size of display is not defined!
#endif
#ifndef   YSIZE_PHYS
#error Physical Y size of display is not defined!
#endif
#ifndef   COLOR_CONVERSION
#error Color conversion not defined!
#endif
#ifndef   DISPLAY_DRIVER
#error No display driver defined!
#endif
#ifndef   NUM_VSCREENS
#define NUM_VSCREENS 1
#else
#if (NUM_VSCREENS <= 0)
#error At least one screeen needs to be defined!
#endif
#endif
#if (NUM_VSCREENS > 1) && (NUM_BUFFERS > 1)
#error Virtual screens and multiple buffers are not allowed!
#endif
#ifndef   VRAM_ADDR_VIRT
#define VRAM_ADDR_VIRT  0
#endif

#if MULTIBUFF_USE_ISR
void LCD_IRQHandler(void);
#endif

#if MULTIBUFF_USE_ISR
/***********************************************************
 * LCD IRQ Handler
 * This is base register update interrupt
 ***********************************************************/

static int32_t _PendingBuffer;

void LCD_IRQHandler(void)
{
	unsigned long Addr, BufferSize;
	//Check of base update interrupt
	if ((LPC_LCD->INTSTAT) & (1 << 2))
	{
		if (_PendingBuffer >= 0)
		{
			//
			// Calculate address of the given buffer
			//
			BufferSize = XSIZE_PHYS * YSIZE_PHYS * PIXEL_WIDTH;
			Addr = VRAM_ADDR_PHYS + BufferSize * _PendingBuffer;
			//
			// Make the given buffer visible
			//
			LPC_LCD->UPBASE = Addr;
			//
			// Send a confirmation that the buffer is visible now
			//
			GUI_MULTIBUF_Confirm(_PendingBuffer);
			_PendingBuffer = -1;
		}
		LPC_LCD->INTCLR |= 1 << 2;
	}
}
#endif

static void _LCD_CopyBuffer(int LayerIndex, int IndexSrc, int IndexDst)
{
	unsigned long BufferSize, AddrSrc, AddrDst;
	//
	// Calculate the size of one frame buffer
	//
	BufferSize = XSIZE_PHYS * YSIZE_PHYS * PIXEL_WIDTH;
	//
	// Calculate source- and destination address
	//
	AddrSrc = VRAM_ADDR_PHYS + BufferSize * IndexSrc;
	AddrDst = VRAM_ADDR_PHYS + BufferSize * IndexDst;
	GUI__memcpy((void *) AddrDst, (void *) AddrSrc, BufferSize);
}

void LCD_X_Config(void)
{
	//
	// At first initialize use of multiple buffers on demand
	//
#if (NUM_BUFFERS > 1)
	GUI_MULTIBUF_Config(NUM_BUFFERS);
#endif

#ifndef WIN32
	if ((FB_XSIZE * FB_YSIZE) < (VXSIZE_PHYS * VYSIZE_PHYS))
	{
		while (1)
			;  // Error, framebuffer too small
	}
#endif
#ifndef WIN32
	GUI_DEVICE_CreateAndLink(DISPLAY_DRIVER, COLOR_CONVERSION, 0, 0);
#else
	GUI_DEVICE_CreateAndLink(GUIDRV_WIN32, COLOR_CONVERSION, 0, 0);
#endif
	LCD_SetDevFunc(0, LCD_DEVFUNC_COPYBUFFER, (void (*)(void)) _LCD_CopyBuffer);
	LCD_SetPosEx(0, 0, 0);
	if (LCD_GetSwapXYEx(0))
	{
		LCD_SetSizeEx(0, YSIZE_PHYS, XSIZE_PHYS);
		LCD_SetVSizeEx(0, VYSIZE_PHYS, VXSIZE_PHYS);
	}
	else
	{
		LCD_SetSizeEx(0, XSIZE_PHYS, YSIZE_PHYS);
		LCD_SetVSizeEx(0, VXSIZE_PHYS, VYSIZE_PHYS);
	}
	LCD_SetVRAMAddrEx(0, (void*) VRAM_ADDR_VIRT);
	//
	// Set user palette data (only required if no fixed palette is used)
	//
#if defined(PALETTE)
	LCD_SetLUTEx(0, PALETTE);
#endif
}

int LCD_X_DisplayDriver(unsigned LayerIndex, unsigned Cmd, void * pData)
{
	int r;
#ifndef WIN32
#if (!MULTIBUFF_USE_ISR)
	unsigned long Addr, BufferSize;
#endif
#endif // !WIN32
	GUI_USE_PARA(LayerIndex);
	switch (Cmd)
	{
		case LCD_X_INITCONTROLLER:
			{
#ifndef WIN32
#if MULTIBUFF_USE_ISR
			_PendingBuffer = -1;
#endif

#if GUI_SUPPORT_TOUCH
			Init_tcs2046();
#ifdef GUI_TOUCH_ADS7846
			GUITDRV_ADS7846_CONFIG pConfig;

			pConfig.pfSendCmd = tcs2046_SendCMD;
			pConfig.pfGetResult = tcs2046_GetResult;
			pConfig.pfGetBusy = tcs2046_GetBusy;
			pConfig.pfSetCS = tcs2046_setCS;
			pConfig.Orientation = (GUI_MIRROR_X * LCD_GetMirrorXEx(0)) | (GUI_MIRROR_Y * LCD_GetMirrorYEx(0)) | (GUI_SWAP_XY * LCD_GetSwapXYEx(0));
			pConfig.xLog0 = 0;
			pConfig.xLog1 = XSIZE_PHYS;
			pConfig.xPhys0 = TOUCH_BOARD_AD._AD_.AD_LEFT;
			pConfig.xPhys1 = TOUCH_BOARD_AD._AD_.AD_RIGHT;

			pConfig.yLog0 = 0;
			pConfig.yLog1 = YSIZE_PHYS;
			pConfig.yPhys0 = TOUCH_BOARD_AD._AD_.AD_TOP;
			pConfig.yPhys1 = TOUCH_BOARD_AD._AD_.AD_BOTTOM;

			pConfig.pfGetPENIRQ = tcs2046_GetPENIRQ;
			pConfig.PressureMin = 0;
			pConfig.PressureMax = 9000;
			pConfig.PlateResistanceX = 245;

			GUITDRV_ADS7846_Config(&pConfig);
#endif

#endif
			Init_lcd(VRAM_ADDR_PHYS);
#if MULTIBUFF_USE_ISR
			NVIC_SetPriority(LCD_IRQn, 1);
			NVIC_ClearPendingIRQ(LCD_IRQn);
			NVIC_EnableIRQ(LCD_IRQn);
			LPC_LCD->INTMSK = 1 << 2;
#endif
			LCD_SetSizeEx(XSIZE_PHYS, YSIZE_PHYS, LayerIndex);
			LCD_SetVSizeEx(VXSIZE_PHYS, VYSIZE_PHYS, LayerIndex);
			LCD_SetVRAMAddrEx(LayerIndex, (void*) VRAM_ADDR_PHYS);
#if GUI_SUPPORT_TOUCH
#ifndef GUI_TOUCH_ADS7846
			U32 TouchOrientation;
			TouchOrientation = (GUI_MIRROR_X * LCD_GetMirrorXEx(0)) |
			(GUI_MIRROR_Y * LCD_GetMirrorYEx(0)) |
			(GUI_SWAP_XY * LCD_GetSwapXYEx(0));
			GUI_TOUCH_SetOrientation(TouchOrientation);
			GUI_TOUCH_Calibrate(GUI_COORD_X, 0, XSIZE_PHYS, TOUCH_BOARD_AD._AD_.AD_LEFT, TOUCH_BOARD_AD._AD_.AD_RIGHT);
			GUI_TOUCH_Calibrate(GUI_COORD_Y, 0, YSIZE_PHYS, TOUCH_BOARD_AD._AD_.AD_TOP, TOUCH_BOARD_AD._AD_.AD_BOTTOM);
			_IsInited = 1;
#endif
			//GUI_CURSOR_Show();
#endif
#endif
			return 0;
		}
		case LCD_X_SETORG:
			{
#ifndef WIN32
			LCD_X_SETORG_INFO * p;
			LCD_X_SETVRAMADDR_INFO Info;

			p = (LCD_X_SETORG_INFO *) pData;
			Info.pVRAM = (void *) (VRAM_ADDR_PHYS + (p->yPos * XSIZE_PHYS * PIXEL_WIDTH));
			LCD_X_DisplayDriver(LayerIndex, LCD_X_SETVRAMADDR, &Info);
#endif
			return 0;
		}
#ifndef WIN32
		case LCD_X_SETVRAMADDR:
			{
			LCD_X_SETVRAMADDR_INFO * p;
			p = (LCD_X_SETVRAMADDR_INFO *) pData;
			LCD_SetBaseAddress(LCD_PANEL_UPPER, (uint32_t) p->pVRAM);
			return 0;
		}

		case LCD_X_SHOWBUFFER:
			{
			LCD_X_SHOWBUFFER_INFO * p;
			p = (LCD_X_SHOWBUFFER_INFO *) pData;

#if MULTIBUFF_USE_ISR
			_PendingBuffer = p->Index;
#else

			BufferSize = XSIZE_PHYS * YSIZE_PHYS * PIXEL_WIDTH;
			Addr = VRAM_ADDR_PHYS + BufferSize * p->Index;
			LCD_SetBaseAddress(LCD_PANEL_UPPER, Addr);
			GUI_MULTIBUF_Confirm(p->Index);
#endif
			return 0;
		}
			/*  case LCD_X_SETLUTENTRY:
			 {
			 // LCD_X_SETLUTENTRY_INFO * p;
			 //  p = (LCD_X_SETLUTENTRY_INFO *)pData;

			 return 0;
			 }*/
		case LCD_X_ON:
			{
			LCD_Enable(TRUE);
			return 0;
		}
		case LCD_X_OFF:
			{
			LCD_Enable(FALSE);
			return 0;
		}
#endif
		default:
			r = -1;
	}
	return r;
}
#if GUI_SUPPORT_TOUCH
#ifndef GUI_TOUCH_ADS7846
/*********************************************************************
 *
 *       GUI_TOUCH_X_MeasureY()
 *
 * Function description:
 *   Called from GUI, if touch support is enabled.
 *   Measures voltage of Y-axis.
 */
int GUI_TOUCH_X_MeasureY(void)
{
	return _TouchY;
}

/*********************************************************************
 *
 *       GUI_TOUCH_X_MeasureX()
 *
 * Function description:
 *   Called from GUI, if touch support is enabled.
 *   Measures voltage of X-axis.
 */
int GUI_TOUCH_X_MeasureX(void)
{
	return _TouchX;
}

void GUI_TOUCH_X_ActivateX(void)
{}
void GUI_TOUCH_X_ActivateY(void)
{}

void ExecTouch(void)
{
#ifndef WIN32
	static char _PenIsDown = 0;
	GUI_PID_STATE State;

	if (_IsInited == 0) return;
	if (tcs2046_GetXY(&_TouchX, &_TouchY))
	{
		_PenIsDown = 1;
		GUI_TOUCH_Exec();
		//GUI_TOUCH_Exec();
		//GUI_TOUCH_Exec();
	}
	else if (_PenIsDown)
	{
		_TouchX = 0;
		_TouchY = 0;
		_PenIsDown = 0;
		GUI_PID_GetState(&State);
		State.Pressed = 0;
		GUI_PID_StoreState(&State);
		GUI_TOUCH_Exec();
	}
#endif
}

#endif
#endif
/*************************** End of file ****************************/
