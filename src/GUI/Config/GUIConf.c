#include "GUI.h"



#define GUI_NUMBYTES  1024*1024*5 //5Mb

#ifndef WIN32
#pragma location="GUI_RAM"
static __no_init U32 _aMemory[GUI_NUMBYTES / 4];
#else
static U32 _aMemory[GUI_NUMBYTES / 4];
#endif



void GUI_X_Config(void)
{
  GUI_ALLOC_AssignMemory(_aMemory, GUI_NUMBYTES);
  GUITASK_SetMaxTask(30);
}

/*************************** End of file ****************************/
