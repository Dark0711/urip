/*
 * �ustom�ommands.h
 *
 *  ������: 20 ��� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef CLI_COMMANDS_CUSTOMCOMMANDS_H_
#define CLI_COMMANDS_CUSTOMCOMMANDS_H_


#include <class_cli.hpp>

typedef tCCommandLineInput<0> tHelp;
typedef tCCommandLineInput<1> tAbout;
typedef tCCommandLineInput<2> tFileSystem;

#endif /* CLI_COMMANDS_CUSTOMCOMMANDS_H_ */
