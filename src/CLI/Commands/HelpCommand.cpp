/*
 * HelpCommand.cpp
 *
 *  ������: 20 ��� 2015 �.
 *  �����:  �������� �.�
 */

#include <class_cli.hpp>
#include <CustomCommands.h>
#include "GUI.h"

#define COMMANDHELP "help"
#define COMMANDHELPSTRING "������:\"help\" ��� \"help [�������� 1] ... [�������� N]\"\r\n��������:���������� ���������� �� ���������\r\n"

#define ABOUT_AUTHOR "author"
#define ABOUT_COMPANY "company"
#define ABOUT_VERSION "version"
#define ABOUT_VERSION_GUI "version_gui"
#define ABOUT_VERSION_OS "version_os"

#define COMMANDABOUT "about"
#define COMMANDABOUTSTRING "������:\"about\" ��� \"about [�������� 1] ... [�������� N]\"\r\n" \
						"���������:\r\n" \
						"\t"ABOUT_AUTHOR"		 - ����� ���������\r\n" \
						"\t"ABOUT_COMPANY"		 - �������� ��������\r\n"\
						"\t"ABOUT_VERSION"		 - ������ ��������\r\n"\
						"\t"ABOUT_VERSION_GUI"	 - ������ ����������� ����������\r\n"\
						"\t"ABOUT_VERSION_OS"	 - ������ ������������ �������\r\n" \
						"��������:������� ���������� � ����������� �����������\r\n"

tHelp HelpCommand(COMMANDHELP, COMMANDHELPSTRING);
tAbout AboutCommand(COMMANDABOUT, COMMANDABOUTSTRING, -1);

template<>
TCliStatus tHelp::CallBack(string &CommandIn, string &CommandOut, CLI *cli)
{
	list<CCommandLineInput*>::iterator itCommand;
	uint32_t NumsParam = GetNumberOfParameters(CommandIn);
	bool FindCommand = false;
	if (NumsParam == 0)
	{
		itCommand = cli->CommandLineInput.begin(); //�������� �������
		CommandOut = "������ ��������� �������:\n";
		for (itCommand = cli->CommandLineInput.begin(); itCommand != cli->CommandLineInput.end(); itCommand++)
		{
			CommandOut += (*itCommand)->Command;
			CommandOut += "\n";
		}
	}
	else
	{
		string sParam = "";
		//itCommand = cli->CommandLineInput.begin(); //�������� �������
		for (uint32_t i = 0; i < NumsParam; i++)
		{
			FindCommand = false;
			sParam = GetParameters(CommandIn, i);
			if (sParam == "") break;
			for (itCommand = cli->CommandLineInput.begin(); itCommand != cli->CommandLineInput.end(); itCommand++)
			{
				if (sParam == (*itCommand)->Command)
				{
					CommandOut += (*itCommand)->HelpString;
					//CommandOut += "\n";
					FindCommand = true;
					break;
				}
			}
			if (!FindCommand)
			{
				CommandOut += "�������� ";
				CommandOut += sParam;
				CommandOut += " �� ����������������\r\n";

			}
		}
	}
	return eStatusOk;
}

#include "version.h"
template<>
TCliStatus tAbout::CallBack(string &CommandIn, string &CommandOut, CLI *cli)
{
	uint32_t NumsParam = GetNumberOfParameters(CommandIn);

	if (NumsParam > 0)
	{
		string sParam = "";
		for (uint32_t i = 0; i < NumsParam; i++)
		{
			sParam = GetParameters(CommandIn, i);
			if (sParam == ABOUT_AUTHOR)
			{
				CommandOut += "�����: "AUTOR"\r\n";
			}
			else if(sParam == ABOUT_COMPANY)
			{
				CommandOut += "��������: "COMPANY"\r\n";
			}
			else if(sParam == ABOUT_VERSION)
			{
				CommandOut += "������ ������������ �����������: "USER_VERSION"\r\n";
			}
			else if(sParam == ABOUT_VERSION_GUI)
			{
				CommandOut += "������ ����������� ����������: ";
				CommandOut += GUI_GetVersionString();
				CommandOut += "\r\n";
			}
			else if(sParam == ABOUT_VERSION_OS)
			{
				CommandOut += "������ ������������ �������: FreeRTOS "tskKERNEL_VERSION_NUMBER"\r\n";
			}
			else
			{
				CommandOut += "�������� ��������\r\n";
			}

		}
	}
	else
	{
		CommandOut = "������ ������������ �����������: "USER_VERSION"\r\n";
	}

	return eStatusOk;
}
