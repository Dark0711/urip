/*
 * class_cli.c
 *
 *  ������: 18 ��� 2015 �.
 *  �����:  �������� �.�
 */

#include "class_cli.hpp"
#ifndef WIN32
#include "lpc177x_8x_uart.h"
#endif

#include "printf_stdarg.h"

string CLI::CommandIn;
CSemaphore CLI::CommandReadyFlag;
CRecursiveMutex CLI::UARTStatusMutex;
bool CLI::TransmitFlag;
/***************************************************************************
 *   	��� ������ ������:	IRQ
 *   	��������:			���������� ����������,�� UART
 ****************************************************************************/
void CLI::UART0_IRQHandler()
{
#ifndef WIN32
	static uint8_t IIRValue, LSRValue;
	static uint8_t Dummy;
	static bool StatusISR = false;
	static bool SendByte = false;

	StatusISR = false;
	IIRValue = LPC_UART0->IIR; //������� ������ ����������

	//������� ������ ����������
	if (IIRValue & UART_IIR_INTID_RLS) //Receive Line Status (RLS).
	{
		LSRValue = LPC_UART0->LSR; //Line status register

		if (LSRValue & (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE | UART_LSR_BI | UART_LSR_RXFE)) //������
		{
			Dummy = LPC_UART0->RBR; //������ �������
			return;
		}
		if (LSRValue & UART_LSR_RDR) StatusISR = true; //Receive Data Ready (RDR)

	}
	else if (IIRValue & UART_IIR_INTID_RDA) StatusISR = true; //Receive Data Available

	if (StatusISR)
	{
		SendByte = true;
		Dummy = LPC_UART0->RBR;

		if (CommandIn.empty())
		{
			TransmitFlag = true;
			//UART_SendByte(UART_0, '\n'); //Ehho
			//UART_SendByte(UART_0, '\r'); //Ehho
		}

		if (Dummy == '\r') //����� ������
		{
			UART_SendByte(UART_0, '\n'); //Ehho
			CommandReadyFlag.give_irq();
			TransmitFlag = false;
			return;
		}
		else if (Dummy == 0x08) //������� ��������� ������
		{
			if (!CommandIn.empty())
			{
				CommandIn.erase(CommandIn.length() - 1, 1);
			}
		}
		else
		{
			CommandIn += Dummy;
		}
	}
	if (SendByte)
	{
		UART_SendByte(UART_0, Dummy);					//Ehho
	}
	return;
#endif
}
/***************************************************************************
 *   	��� ������ ������:	UART0_SendString
 *		���������:			CommandIn - ������� ��������, CommandOut - ���������
 *   	����������:			uint32_t ���������� ���������� ����
 *   	��������:			���������� ������ � UART0
 ****************************************************************************/
uint32_t CLI::UART0_SendString(char const *txbuf)
{
	static uint32_t BytesSend;
	UARTStatusMutex.take();
	if (!TransmitFlag)
	{
#ifndef WIN32
		BytesSend = UART_Send(UART_0, (uint8_t*) txbuf, strlen(txbuf), BLOCKING);
#endif
	}
	UARTStatusMutex.give();
	return BytesSend;
}
/***************************************************************************
 *   	��� ������ ������:	UART0_SendString
 *		���������:			format - ������ ������ ������, ... - ���������
 *   	��������:			���������� ������ � UART0
 ****************************************************************************/
void CLI::UART0_Printf(char const *format, ...)
{
#ifndef WIN32
	static __no_init char Debug_printf_buff[512];
#else
	static char Debug_printf_buff[512];
#endif
	va_list args;
	char *pDebug_buff = Debug_printf_buff;
	va_start(args, format);
	__print(&pDebug_buff, format, args);
	//xSemaphoreTakeRecursive(Debug_Mutex, portMAX_DELAY);
	UART0_SendString(Debug_printf_buff);
}
/***************************************************************************
 *   	��� ������ ������:	run
 *   	��������:			������ ���������� �������
 ****************************************************************************/
void CLI::run()
{
	UART0_SendString(">>");
	for (;;)
	{
		CommandReadyFlag.take();
		ProcessCommand(CommandIn, CommandOut);
		CommandIn.erase();
		if (!CommandOut.empty())
		{
			UARTStatusMutex.take();
			UART0_SendString(CommandOut.c_str());
			UART0_SendString(">>");
			CommandOut.erase();
			UARTStatusMutex.give();
		}
	}
}
/***************************************************************************
 *   	��� ������ ������:	CLI
 *   	��������:			����������� ������
 ****************************************************************************/
CLI::CLI() :
		CThread("CLI", stackDepth, priority)
{
#ifndef WIN32
	UART_InitDefault(UART_0);
	//����������
	UART_IntConfig(UART_0, UART_INTCFG_RBR, ENABLE);					// Enable UART Rx interrupt
	UART_IntConfig(UART_0, UART_INTCFG_RLS, ENABLE);					// Enable UART line status interrupt

	NVIC_SetPriority(UART0_IRQn, configUART0_INTERRUPT_PRIORITY);
	NVIC_EnableIRQ(UART0_IRQn);
#endif
}
/***************************************************************************
 *   	��� ������ ������:	ProcessCommand
 *		���������:			CommandIn - ������� ��������, CommandOut - ���������
 *   	����������:			TCliStatus - ������ ��������
 *   	��������:			��������� �������� ������ � ���� ���� �������,������ ����������
 ****************************************************************************/
TCliStatus CLI::ProcessCommand(string &_CommandIn, string &_CommandOut)
{
	list<CCommandLineInput*>::iterator itCommand; //�������� �������
	std::size_t found; //��������� ������
	if (!_CommandIn.empty() && !CommandLineInput.empty()) //��������� �����
	{
		for (itCommand = CommandLineInput.begin(); itCommand != CommandLineInput.end(); itCommand++)
		{
			//������ ���� ��������
			found = _CommandIn.find((*itCommand)->Command); //���� ��������
			if (found == std::string::npos) continue; //���-�� �����?
			//��������
			if (_CommandIn[(*itCommand)->Command.length()] != 0x00 && _CommandIn[(*itCommand)->Command.length()] != ' ') continue;
			return (*itCommand)->CallBack(_CommandIn, _CommandOut, this);
		}
		_CommandOut = "������� � ����� ������ �� ����������������\r\n";
	}
	return eStatusNocommand;
}
/***************************************************************************
 *   	��� ������ ������:	RegisterCommand
 *		���������:			Command - ����� �������� ��� ���������
 *   	����������:			TCliStatus - ������ ��������
 *   	��������:			������������ ��������
 ****************************************************************************/
TCliStatus CLI::RegisterCommand(CCommandLineInput &Command)
{
	CommandLineInput.push_back(&Command);
	return eStatusRegisterok;
}
/***************************************************************************
 *   	��� ������ ������:	CCommandLineInput
 *		���������:			Command - ��������, HelpString - �������� ��������, NumberOfParameters - ��������� ����������
 *   	��������:			������������ ����� ���, ������ ��������, �����������
 ****************************************************************************/
CCommandLineInput::CCommandLineInput(char *Command, char *HelpString, int NumberOfParameters)
{
	CCommandLineInput::Command = Command;
	CCommandLineInput::HelpString = HelpString;
	CCommandLineInput::NumberOfParameters = NumberOfParameters;
}
/***************************************************************************
 *   	��� ������ ������: 	GetNumberOfParameters
 *		���������:   		CommandIn - ������� ��������
 *   	����������:			uint32_t - ���������
 *   	��������:			����������� ���������� ���������� � ������ ����� ��������
 ****************************************************************************/
uint32_t CCommandLineInput::GetNumberOfParameters(string &CommandIn)
{
	std::size_t found = -1;			//��������� ������
	uint32_t result = 0;
	while (1)
	{
		found = CommandIn.find(' ', found + 1);			//���� ������
		if (found == std::string::npos) break;
		result++;
	}
	return result;
}
/***************************************************************************
 *   	��� ������ ������: 	GetParameters
 *		���������:   		CommandIn - ������� ��������, NumParam - ����� ��������
 *   	����������:			uint32_t - ���������
 *   	��������:			����������� ��������� �������� � ������ ����� ��������
 ****************************************************************************/
string CCommandLineInput::GetParameters(string &CommandIn, uint32_t NumParam)
{
	std::size_t found = -1, foundafter;			//��������� ������
	uint32_t result = 0;
	string Str;

	Str = "";

	while (1)
	{
		found = CommandIn.find(' ', found + 1);			//���� ������
		if (found == std::string::npos) return Str;
		if (result == NumParam) break;
		result++;
	}
//���� �������� �������� ����� ��������� ����� ��������
	found++;
	foundafter = CommandIn.find(' ', found);			//���� ������
	if (foundafter == std::string::npos)
	{
		Str = CommandIn.substr(found, CommandIn.length());
	}
	else
	{
		Str = CommandIn.substr(found, foundafter - found);
	}

	return Str;
}
