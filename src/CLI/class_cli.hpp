/*
 * class_cli.h
 *
 *  ������: 15 ��� 2015 �.
 *  �����:  �������� �.�
 */
/*************************************************************************
* 		���: class_cli.h
*
* 		��������:
*		������ ��� ���������� �������
*
*		�����
*		�������� �.�
*
****************************************************************************/
#ifndef FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_CLASS_CLI_H_
#define FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_CLASS_CLI_H_

#include "Thread.h"
#include "SemphrWrapper.h"
#include "MutexRecursiveWrapper.h"


#include <list>
#include <string>
using namespace std;
class CLI;

enum TCliStatus
{
	eStatusNocommand = 0x00,		//������� ����������� ��� ����� ������
	eStatusOk,						//�������� ���������
	eStatusManyparam,				//����� ����������
	//������� ������������ ������������� ������
	eStatusNoparam,					//�������� �������� ��������� �����������
	eStatusComplite,				//������� ������� ���������
	eStatusUncownerror,				//���������� ������ � �������� ���������� �������
	//������� ���������� ����� ��������
	eStatusRegisterok,				//������� ������� ����������������
	//������ ����������������
	eStatusUncown,					//���������� ������
};

/***************************************************************************
 *   	��� ������: CCommandLineInput
 *   	��������:   ������������ ����� ��� �������� �������
****************************************************************************/
class CCommandLineInput
{
	public:
		//void* operator new(size_t);
		//void operator delete(void*);
	public:
		string Command;
		string HelpString;
		int    NumberOfParameters;
	public:
		CCommandLineInput(char *Command, char *HelpString, int NumberOfParameters);
		uint32_t GetNumberOfParameters(string &CommandIn);
		string GetParameters(string &CommandIn,uint32_t NumParam);
		virtual TCliStatus CallBack(string &CommandIn, string &CommandOut, CLI *cli) = 0;
		virtual ~CCommandLineInput(){};
};
/***************************************************************************
 *   	��� ������: tCCommandLineInput
 *   	��������:   ������ ��� ��������, �������� ������� CallBack
****************************************************************************/
template<int id>
class tCCommandLineInput : public CCommandLineInput
{

	public:
		tCCommandLineInput(char *Command = NULL, char *HelpString = NULL, int NumberOfParameters = -1):CCommandLineInput(Command, HelpString, NumberOfParameters){};
		TCliStatus CallBack(string &CommandIn, string &CommandOut, CLI *cli);
};
/***************************************************************************
 *   	��� ������: CLI
 *   	��������:   ����� ��� ������ � ��������
****************************************************************************/
class CLI: public CThread
{
	private:
		static const TThreadPriority priority = eVerylow;
		static const unsigned short stackDepth = configMINIMAL_STACK_SIZE * 6;
	private:
		static string CommandIn;
		string CommandOut;
		static CSemaphore CommandReadyFlag;
		static CRecursiveMutex UARTStatusMutex;
		static bool TransmitFlag;
	public:
		static void UART0_IRQHandler();//���������� �� UART
		static uint32_t UART0_SendString(char const *txbuf);
		static void UART0_Printf(char const *format, ...);
	public:
		list<CCommandLineInput*> CommandLineInput; //������ �������
		TCliStatus ProcessCommand(string &_CommandIn, string &_CommandOut);//��������� �����
		TCliStatus RegisterCommand(CCommandLineInput &Command);//��������� �������� � ������
		void run();
	public:

	public:
		CLI();
};

#endif /* FREERTOS_FREERTOS_CLASS_THREADSCLASS_INC_CLASS_CLI_H_ */
