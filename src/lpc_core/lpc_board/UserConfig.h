/*����� ������� �������� ���� �������� �������*/
#ifndef __USERCONFIG_H
#define __USERCONFIG_H

#ifdef __cplusplus
extern "C"
{
#endif

//#include "FreeRTOSInclude.h"
#include "version.h"
//#include "uart.h"

#ifndef WIN32
#endif

#ifndef WIN32
#define UART_SEND_DEBUG(X) UART_SendString(UART_0,X)
#else
#define UART_SEND_DEBUG(X) OutputDebugString(X)
#endif
#define BootLoaderCompileVersion "\nAPK URIP Project Ver." USER_VERSION "Bild:"__DATE__ " "__TIME__" Autor: Tarshikov A.O\n\r"

//#define USER_VERSION "2.0.0"

#define WDT_TIMEOUT					5000000

#define __StartGUITaskPriority		PRIORITY_CRITICAL							//��������� ������ �������������
#define __StartGUITaskStackSize		configMINIMAL_STACK_SIZE * 10				//������ ����� ��� ������ �������������

#define __GUITaskDelay				500                                             //�������� ������ ����
#define __GUITaskPriority			PRIORITY_NORMAL                                 //��������� ������ ��������� GUI
#define __GUITaskStackSize			configMINIMAL_STACK_SIZE * 10                   //������ ����� ��� ������ ��������� GUI

#define __USBTaskPriority			PRIORITY_NORMAL                               //��������� ������ ������ � USB
#define __USBTaskStackSize			configMINIMAL_STACK_SIZE * 4                   //������ ����� ��� ������ ������ � USB

#define __FontInitTaskPriority		PRIORITY_NORMAL                               //��������� ������ ������������� �������
#define __FontInitTasStackSize		configMINIMAL_STACK_SIZE * 10                  //������ ������ ������������� �������



#ifdef __cplusplus
}
#endif

#endif
