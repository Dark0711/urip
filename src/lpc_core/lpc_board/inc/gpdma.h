#ifndef __GPDMA_H_
#define __GPDMA_H_
#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

void GPDMA_SetIRQAddr(uint8_t NumChanal,void Func(void));//��������� IRQ ��� DMA

enum
{
  GPDMA_MCI_WRITE_CHANNEL = 0x00,
  GPDMA_MCI_READ_CHANNEL,
  GPDMA_SSP2_WRITE_CHANNEL,
  GPDMA_SSP2_READ_CHANNEL,
};

#ifdef __cplusplus
}
#endif
#endif
