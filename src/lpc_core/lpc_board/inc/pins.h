#ifndef __PINS_H_
#define __PINS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define PLIS_TIME_SYNC_PORT 0
#define PLIS_TIME_SYNC_PIN	13

void Init_pins(void);

#ifdef __cplusplus
}
#endif

#endif //_PINS_H_
