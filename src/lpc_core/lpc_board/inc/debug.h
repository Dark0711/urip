#ifndef __DEBUG_H__
#define __DEBUG_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WIN32
void Debug_init();
#else
#define Debug_init() 
#endif

#define INTTOSTR_STR(s) __STR(s)
#define __STR(s) #s

void Debug_printf(const char *format, ...);

#define DEBUGOUT(fmt,args ...) Debug_printf(__STR(__FILE__)""__STR(__LINE__)""fmt,##args);

//void printf(char *out, const char *format, ...);
#ifdef __cplusplus
}
#endif

#endif
