/*
 * cRTC.hpp
 *
 *  ������: 29 ��� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef LPC_CORE_LPC_BOARD_INC_CRTC_HPP_
#define LPC_CORE_LPC_BOARD_INC_CRTC_HPP_

#include "LPC177x_8x.h"

class cTime
{
	public:
		static const uint32_t _DAY_SEC = (24 * 60 * 60);
		static const uint32_t _YEAR_SEC = (365 * _DAY_SEC);
		static const uint32_t _FOUR_YEAR_SEC = (1461 * _DAY_SEC);
		static const uint32_t _DEC_SEC = 315532800;
		static const uint32_t _BASE_YEAR = 70;
		static const uint32_t _BASE_DOW = 4;
		static const uint32_t _LEAP_YEAR_ADJUST = 17;
		static const uint32_t _MAX_YEAR = 1100;

	public:
		uint32_t Minute;
		uint32_t Second;
		uint32_t Hour;
		uint32_t Day;
		uint32_t DayOfYear;
		uint32_t Mount;
		uint32_t Year;
	public:
		uint64_t Time;
		const cTime& operator++()
		{
			this->Time++;
			return *this;
		}
		const cTime& operator--()
		{
			this->Time--;
			return *this;
		}
};

class cRTC
{
	private:
		static const uint32_t SEC_MASK = (0x0000003F);
		static const uint32_t MIN_MASK = (0x0000003F);
		static const uint32_t HOUR_MASK = (0x0000001F);
		static const uint32_t DOM_MASK = (0x0000001F);
		static const uint32_t DOW_MASK = (0x00000007);
		static const uint32_t DOY_MASK = (0x000001FF);
		static const uint32_t MONTH_MASK = (0x0000000F);
		static const uint32_t YEAR_MASK = (0x00000FFF);

		static const uint32_t SECOND_MAX = 59;
		static const uint32_t MINUTE_MAX = 59;
		static const uint32_t HOUR_MAX = 23;
		static const uint32_t MONTH_MIN = 1;
		static const uint32_t MONTH_MAX = 12;
		static const uint32_t DAYOFMONTH_MIN = 1;
		static const uint32_t DAYOFMONTH_MAX = 31;
		static const uint32_t DAYOFWEEK_MAX = 6;
		static const uint32_t DAYOFYEAR_MIN = 1;
		static const uint32_t DAYOFYEAR_MAX = 366;
		static const uint32_t YEAR_MAX = 4095;

	private:
		enum TIMETYPE
		{
			TIMETYPE_SECOND = 0,
			TIMETYPE_MINUTE = 1,
			TIMETYPE_HOUR = 2,
			TIMETYPE_DAYOFWEEK = 3,
			TIMETYPE_DAYOFMONTH = 4,
			TIMETYPE_DAYOFYEAR = 5,
			TIMETYPE_MONTH = 6,
			TIMETYPE_YEAR = 7,
		};
		private:
		void SetTime(TIMETYPE Timetype, uint32_t TimeValue);
		uint32_t GetTime(uint32_t Timetype) const;
		static bool InitFlag; //���� �������������
		LPC_RTC_TypeDef *RTCx;
		public:
		cTime GetTime();
		void SetTime(cTime&);
		//void SetTime()
		cRTC();
		~cRTC();

};

#endif /* LPC_CORE_LPC_BOARD_INC_CRTC_HPP_ */
