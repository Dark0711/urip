#ifndef __LCD_H_
#define __LCD_H_

#ifndef WIN32
#include "lpc177x_8x_lcd.h"
#endif

#define LCD_WIDTH  800
#define LCD_HEIGHT 480

#ifndef WIN32
LCD_RET_CODE Init_lcd(uint32_t LCDRamBuff);
void RGB_Test(uint32_t Color, uint32_t *LCDRamBuff);
//void RGB_Test(uint32_t Color);
#endif


#endif
