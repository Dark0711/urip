/*
 * gpio_pin.hpp
 *
 *  ������: 28 ��� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef LPC_CORE_LPC_BOARD_INC_GPIO_PIN_HPP_
#define LPC_CORE_LPC_BOARD_INC_GPIO_PIN_HPP_

#include <stdint.h>
#include "LPC177x_8x.h"
/***************************************************************************
 *   	��� ������: gpio_pin
 *   	��������:   �������� ����� GPIO, � ������ �������� � ���
****************************************************************************/
template<uint8_t portNum, uint32_t pinNum, uint8_t dir >
class gpio_pin
{
	private:
		uint8_t 	port;
		uint32_t 	pin;
		uint8_t 	Direction;
		LPC_GPIO_TypeDef *pGPIO;
	private:

	public:
		gpio_pin();
		inline void set()
		{
			pGPIO->SET |= 1 << pin;
		}
		inline void clear()
		{
			pGPIO->CLR |= 1 << pin;
		}
		inline bool read()
		{
			return pGPIO->PIN & (~(1 << pin));
		}

};



#endif /* LPC_CORE_LPC_BOARD_INC_GPIO_PIN_HPP_ */
