#ifndef EEPROM_USERDATA_H_
#define EEPROM_USERDATA_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum
{
	EEPROM_PAGE_NUMBER = 0,
	EEPROM_PAGE_OPERATOR,
	EEPROM_PAGE_IDENTIFIER,
	EEPROM_PAGE_TOUCH_BOARD,
	EEPROM_PAGE_ADDITIONALDATA,
}tEEPROM_LocalUserData;


void EEPROM_Init_LocalUserData(void);
void EEPROM_Write_LocalUserData(tEEPROM_LocalUserData PageNum, void* data);
void EEPROM_Write_LocalUserData2(tEEPROM_LocalUserData PageNum, void* data,  uint32_t size_data);
void EEPROM_Read_LocalUserData(tEEPROM_LocalUserData PageNum, void* data);
void EEPROM_Read_LocalUserData2(tEEPROM_LocalUserData PageNum, void* data, uint32_t size_data);
void EEPROM_Sync_LocalUserData(void);
bool EEPROM_Compare_LocalUserData(tEEPROM_LocalUserData PageNum, void* data);
void EEPROM_Erase_LocalUserData();

#endif /* EEPROM_USERDATA_H_ */
