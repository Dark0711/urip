/*
 * cIRQ.hpp
 *
 *  ������: 02 ���� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef LPC_CORE_LPC_BOARD_INC_CIRQ_HPP_
#define LPC_CORE_LPC_BOARD_INC_CIRQ_HPP_

#include "LPC177x_8x.h"
#include <list>

class IRQ_Basic
{
	public:
		virtual void IRQ(void) = 0;
		virtual ~IRQ_Basic(){};
};

class cIRQ_Management
{
	public:
	//	static void TIMER0_IRQHandler(void);//���������� �� �������
		static void DMA_IRQHandler(void);//���������� �� DMA
		//static void UART0_IRQHandler(void);//���������� �� UART
	private:
		static std::list<IRQ_Basic*> TIMER0; //
		static std::list<IRQ_Basic*> DMA; //
	public:
		void RegisterIRQ(IRQn_Type,IRQ_Basic *IRQ);


};



#endif /* LPC_CORE_LPC_BOARD_INC_CIRQ_HPP_ */
