#ifndef __SDRAM_NAND_H_
#define __SDRAM_NAND_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#define SDRAM_BASE_ADDR       0xA0000000
#define SDRAM_BASE_SIZE       0x2000000
#define SDRAM_PERIOD          8.33  // 120MHz
#define P2C(Period)           (((Period<SDRAM_PERIOD)?0:(uint32_t)((float)Period/SDRAM_PERIOD))+1)
#define SDRAM_REFRESH         7813
#define SDRAM_TRP             20
#define SDRAM_TRAS            44
#define SDRAM_TAPR            1
#define SDRAM_TDAL            3
#define SDRAM_TWR             3
#define SDRAM_TRC             66
#define SDRAM_TRFC            66
#define SDRAM_TXSR            75
#define SDRAM_TRRD            15
#define SDRAM_TMRD            2

#define K9F1G_CLE                            ((volatile uint8_t *)0x90100000)
#define K9F1G_ALE                            ((volatile uint8_t *)0x90080000)
#define K9F1G_DATA                           ((volatile uint8_t *)0x90000000)

#define K9FXX_ID                             0xf1009540	/* Byte 3 and 2 only */

#define K9FXX_READ_1                         0x00
#define K9FXX_READ_2                         0x30
#define K9FXX_READ_ID                        0x90
#define K9FXX_RESET                          0xFF
#define K9FXX_BLOCK_PROGRAM_1                0x80
#define K9FXX_BLOCK_PROGRAM_2                0x10
#define K9FXX_BLOCK_ERASE_1                  0x60
#define K9FXX_BLOCK_ERASE_2                  0xD0
#define K9FXX_READ_STATUS                    0x70

#define K9FXX_BUSY                           (1 << 6)
#define K9FXX_OK                             (1 << 0)


#define ERR_RETVAL_OK                        (0)
#define ERR_RETVAL_ERROR                     (-1)
#define ERR_RETVAL_WRONG_INPUT               (-2)

void Init_sdram_nand( void );
//void NandFlash_Reset(void);
//uint32_t NandFlash_ReadId(void);
//bool Tets_sdram(uint32_t TestData);
#ifdef __cplusplus
}
#endif

#endif
