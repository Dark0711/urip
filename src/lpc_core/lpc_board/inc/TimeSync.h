/*************************************************************************
* 		���: TimeSync.h
*
* 		��������
*		������ �� ��������
*
*		�����
*		�������� �.� ���������� �.�
*
****************************************************************************/
#ifndef __TIMESYNC_H__
#define __TIMESYNC_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <time.h>

struct tm * CT_GetTime();
time_t CT_GetDataTime();
void CT_RTC_Init();
void CT_Format(char *TEXT, uint16_t Lenghtchar,char *Format);
uint8_t CT_GetHour();
uint8_t CT_GetMinute();
uint8_t CT_GetSecond();
uint8_t CT_Get_Month();
uint16_t CT_Get_Year();
uint8_t CT_Get_Day();
void CT_SetTime(int Hour,int Minute, int Second);
void CT_SetTimeData(int Hour,int Minute, int Second, int Day, int Month, int Year);
void CT_SetData(int Day, int Month, int Year);
void CT_SetTimeData_t(time_t LocalTime);

#ifdef __cplusplus
}
#endif
//------------------------------------------------------------------------------

#endif // __TIMESYNC_H__
