/*
 * cTIMER.hpp
 *
 *  ������: 29 ��� 2015 �.
 *  �����:  �������� �.�
 */

#ifndef LPC_CORE_LPC_BOARD_INC_CTIMER_HPP_
#define LPC_CORE_LPC_BOARD_INC_CTIMER_HPP_

#include <stdint.h>
#include "LPC177x_8x.h"

class cTIMER
{
	private:
		cTIMER(const cTIMER&);

	public:
		//Interrupt Register
		enum INT_TYPE
		{
			MR0_Interrupt = 0, /*!< interrupt for Match channel 0*/
			MR1_Interrupt = 1, /*!< interrupt for Match channel 1*/
			MR2_Interrupt = 2, /*!< interrupt for Match channel 2*/
			MR3_Interrupt = 3, /*!< interrupt for Match channel 3*/
			CR0_Interrupt = 4, /*!< interrupt for Capture channel 0*/
			CR1_Interrupt = 5, /*!< interrupt for Capture channel 1*/
		};
		//Match Registers
		enum EXTMATCH_OPT
		{
			NOTHING = 0x00, /*!< Do nothing for external output pin if match */
			LOW = 0x01, /*!< Force external output pin to low if match */
			HIGH = 0x02, /*!< Force external output pin to high if match */
			TOGGLE = 0x03 /*!< Toggle external output pin if match */
		};
		enum COUTCONTROL_OPT
		{
			COUTCONTROL_TIMER = 0x00,
			COUTCONTROL_HIGH = 0x01,
			COUTCONTROL_LOW = 0x02,
			COUTCONTROL_TOGGLE = 0x03
		};
		enum MATCHNUM
		{
			MATCH_NUM0 = 0x00,
			MATCH_NUM1 = 0x01,
			MATCH_NUM2 = 0x02,
			MATCH_NUM3 = 0x03
		};
		//Capture Registers
		enum CAPTURENUM
		{
			CAPTURE_NUM0 = 0x00,
			CAPTURE_NUM1 = 0x01,
		};
		private:
		LPC_TIM_TypeDef *TIMx;
		IRQn TIMx_IRQ;
		IRQn GetNumTimerIRQ()
		{
			if (TIMx == LPC_TIM0) return TIMER0_IRQn;
			else if (TIMx == LPC_TIM1) return TIMER1_IRQn;
			else if (TIMx == LPC_TIM2) return TIMER2_IRQn;
			else if (TIMx == LPC_TIM3) return TIMER3_IRQn;
			return NonMaskableInt_IRQn;
		}
	public:
		cTIMER(LPC_TIM_TypeDef *TIMER, COUTCONTROL_OPT CounterMode, CAPTURENUM CaptureNum);
		uint32_t GetTimerCounter() const
		{
			return TIMx->TC;
		}
	public:
		//Timer
		void Enable() const
		{
			TIMx->TCR |= 0x01;
		}
		void Disable() const
		{
			TIMx->TCR &= ~0x01;
		}
		void ResetCounter() const
		{
			TIMx->TCR |= 0x02;
			TIMx->TCR &= ~0x02;
		}
		void EnableIRQ() const
		{
			NVIC_EnableIRQ(TIMx_IRQ);
		}
		void SetPriorityIRQ(uint32_t priority) const
		{
			NVIC_SetPriority(TIMx_IRQ, priority);
		}
	public:
		//Interrupt Register
		bool GetIntStatus(INT_TYPE Interrupt) const
		{
			return TIMx->IR & (1 << Interrupt);
		}
		void ClearIntStatus(INT_TYPE Interrupt) const
		{
			TIMx->IR = 1 << Interrupt;
		}
		void ClearAllIntStatus() const
		{
			TIMx->IR = 0x0f;
		}
	public:
		//Match Registers
		void ConfigMatch(MATCHNUM MatchNum, bool IntOnMatch, bool StopOnMatch, bool ResetOnMatch, EXTMATCH_OPT ExtMatchOutputType, uint32_t MatchValue);
		void UpdateMatchValue(MATCHNUM MatchNum, uint32_t MatchValue) const
		{
			*((uint32_t*) (&TIMx->MR0) + MatchNum) = MatchValue;
		}
	public:
		//Capture Registers
		void ConfigCapture(CAPTURENUM CaptureNum, bool RisingEdge, bool FallingEdge, bool IntOnCaption);
		uint32_t GetCaptureValue(CAPTURENUM CaptureNum) const
		{
			return *((uint32_t*) (&TIMx->CR0) + CaptureNum);
		}
		void InvertRisingFallingEdges(CAPTURENUM CaptureNum) const
		{
			TIMx->CCR ^= (0x03 << (CaptureNum * 3));
		}
		bool GetFallingEdge(CAPTURENUM CaptureNum) const
		{
			return TIMx->CCR & (1 << ((CaptureNum * 3) + 1));
		}
		bool GetRisingEdge(CAPTURENUM CaptureNum) const
		{
			return TIMx->CCR & (1 << (CaptureNum * 3));
		}
};

#endif /* LPC_CORE_LPC_BOARD_INC_CTIMER_HPP_ */
