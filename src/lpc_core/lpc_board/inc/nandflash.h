#ifndef __NANDFLASH_K9F1G08U0A_H_
#define __NANDFLASH_K9F1G08U0A_H_

#include <stdbool.h>
#include <stdint.h>

#define NANDFLASH_USE_OS

#ifdef NANDFLASH_USE_OS
#include "FreeRTOSInclude.h"
#define NANDFLASH_OS_ENTERCRITICAL() vPortEnterCritical()
#define NANDFLASH_OS_EXITCRITICAL()  vPortExitCritical()
#else
#define NANDFLASH_OS_ENTERCRITICAL()
#define NANDFLASH_OS_EXITCRITICAL()
#endif

/*****************************************************************************
 * Defines and typedefs
 ****************************************************************************/
#define EMC_CS0                              0
#define EMC_CS1                              1
#define EMC_CS2                              2
#define EMC_CS3                              3
#define NANDFLASH_CS                         EMC_CS1


#if (NANDFLASH_CS == EMC_CS1)
#define K9F1G_CLE                            ((volatile uint8_t *)0x90100000)
#define K9F1G_ALE                            ((volatile uint8_t *)0x90080000)
#define K9F1G_DATA                           ((volatile uint8_t *)0x90000000)
#endif

#define NANDFLASH_BASE_ADDR                  0x00000000

#define NANDFLASH_INVALIDBLOCK_CHECK_COLUMM  (2048)

// total 1024 blocks in a device
#define NANDFLASH_NUMOF_BLOCK                1024

// total pages in a block
#define NANDFLASH_PAGE_PER_BLOCK             64

#define NANDFLASH_RW_PAGE_SIZE               2048		// 2048 bytes/page

#define NANDFLASH_SPARE_SIZE                 64 			//bytes/page

#define NANDFLASH_PAGE_FSIZE                 (NANDFLASH_RW_PAGE_SIZE + NANDFLASH_SPARE_SIZE)

#define NANDFLASH_BLOCK_RWSIZE               (NANDFLASH_RW_PAGE_SIZE * NANDFLASH_PAGE_PER_BLOCK)
#define NANDFLASH_BLOCK_FSIZE                (NANDFLASH_PAGE_FSIZE * NANDFLASH_PAGE_PER_BLOCK)


#define NANDFLASH_ADDR_COLUMM_POS            0
#define NANDFLASH_ADDR_ROW_POS               16

#define K9FXX_ID                             0xf1009540	/* Byte 3 and 2 only */

#define K9FXX_READ_1                         0x00
#define K9FXX_READ_2                         0x30
#define K9FXX_READ_ID                        0x90
#define K9FXX_RESET                          0xFF
#define K9FXX_BLOCK_PROGRAM_1                0x80
#define K9FXX_BLOCK_PROGRAM_2                0x10
#define K9FXX_BLOCK_ERASE_1                  0x60
#define K9FXX_BLOCK_ERASE_2                  0xD0
#define K9FXX_READ_STATUS                    0x70

#define K9FXX_BUSY                           (1 << 6)
#define K9FXX_OK                             (1 << 0)


#define ERR_RETVAL_OK                        (0)
#define ERR_RETVAL_ERROR                     (-1)
#define ERR_RETVAL_WRONG_INPUT               (-2)


void Init_nandflash(void);
void NandFlash_Reset(void);
uint32_t NandFlash_ReadId(void);
uint32_t NandFlash_BlockErase(const uint16_t blockNum);
uint32_t NandFlash_PageProgram(const uint16_t blockNum, const uint8_t pageNum, const uint8_t *bufPtr/*, uint32_t bSpareProgram*/);
uint32_t NandFlash_PageRead(const uint16_t blockNum, const uint8_t pageNum, uint8_t *bufPtr);
//bool NandFlash_PageProgramCRC(const uint32_t blockNum,const uint32_t pageNum,const uint32_t AddresInPage,const uint8_t* bufPtr ,uint32_t Size,const uint32_t CRC32);
//bool NandFlash_ReadPageAddress(uint16_t blockNum, const uint8_t pageNum, const uint16_t AddressInPage, uint32_t Size, uint8_t *bufPtr);


uint32_t NandFlash_ValidBlockCheck(uint16_t block);
void NandFlash_EraseForValidBlockCheck();

#endif /* __NANDFLASH_K9F1G08U0A_H_ */
