#include "pins.h"
#ifdef __PINS_H_
#include "lpc177x_8x_pinsel.h"

void Init_pins(void)
{
	//UART
	PINSEL_ConfigPin(0, 2, 1); //
	PINSEL_ConfigPin(0, 3, 1); //

	//SSP_0
	PINSEL_ConfigPin(2, 23, 2); //SSP0_SSEL
	PINSEL_ConfigPin(2, 22, 2); //SSP0_SCK
	PINSEL_ConfigPin(2, 27, 2); //SSP0_MOSI
	PINSEL_ConfigPin(2, 26, 2); //SSP0_MISO
	//LCD
#ifndef _TEST_BOARD_
	PINSEL_ConfigPin(3, 24, 0x10);  // LCD_ADJ
	PINSEL_ConfigPin(2, 4, 0x7);  // LCD_DE
	PINSEL_ConfigPin(1, 29, 0x7);  // LCD_VD_23
	PINSEL_ConfigPin(1, 28, 0x7);  // LCD_VD_22
	PINSEL_ConfigPin(1, 27, 0x7);  // LCD_VD_21
	PINSEL_ConfigPin(1, 26, 0x7);  // LCD_VD_20
	PINSEL_ConfigPin(2, 13, 0x7);  // LCD_VD_19
	PINSEL_ConfigPin(2, 12, 0x0);  // LCD_VD_18
	PINSEL_ConfigPin(1, 25, 0x7);  // LCD_VD_15
	PINSEL_ConfigPin(1, 24, 0x7);  // LCD_VD_14
	PINSEL_ConfigPin(1, 23, 0x7);  // LCD_VD_13
	PINSEL_ConfigPin(1, 22, 0x7);  // LCD_VD_12
	PINSEL_ConfigPin(1, 21, 0x7);  // LCD_VD_11
	PINSEL_ConfigPin(1, 20, 0x7);  // LCD_VD_10
	PINSEL_ConfigPin(2, 9, 0x7);  // LCD_VD_7
	PINSEL_ConfigPin(2, 8, 0x7);  // LCD_VD_6
	PINSEL_ConfigPin(2, 7, 0x7);  // LCD_VD_5
	PINSEL_ConfigPin(2, 6, 0x7);  // LCD_VD_4
	PINSEL_ConfigPin(4, 29, 0x7);  // LCD_VD_3
	PINSEL_ConfigPin(4, 28, 0x0);  // LCD_VD_2

	PINSEL_ConfigPin(2, 2, 0xF);  // LCD_DCLK // pull-down, func 7

	PINSEL_ConfigPin(2, 3, 0x7);  // LCD_VS
	PINSEL_ConfigPin(2, 5, 0x7);  // LCD_HS
#else

	PINSEL_ConfigPin(2,12,0x25);// LCD_VD_3
	PINSEL_ConfigPin(2,6,0x27);// LCD_VD_4
	PINSEL_ConfigPin(2,7,0x27);// LCD_VD_5
	PINSEL_ConfigPin(2,8,0x27);// LCD_VD_6
	PINSEL_ConfigPin(4,29,0x25);// LCD_VD_7

	for (unsigned int i = 20; i <= 25; i++) //LCD_VD_10 - LCD_VD_15
	{
		PINSEL_ConfigPin(1,i,0x27);
	}

	PINSEL_ConfigPin(2,13,0x27);// LCD_VD_19
	PINSEL_ConfigPin(1,26,0x27);// LCD_VD_20
	PINSEL_ConfigPin(1,27,0x27);// LCD_VD_21
	PINSEL_ConfigPin(1,28,0x27);// LCD_VD_22
	PINSEL_ConfigPin(1,29,0x27);// LCD_VD_23

	PINSEL_ConfigPin(2,2,0x27);// LCD_DCLK
	PINSEL_ConfigPin(2,5,0x27);// LCD_LP -- HSYNC
	PINSEL_ConfigPin(2,3,0x27);// LCD_FP -- VSYNC
	PINSEL_ConfigPin(2,4,0x27);// LCD_ENAB_M -- LCDDEN

#endif
}
#endif
