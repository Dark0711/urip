#include "sdram_nand.h"

#ifdef __SDRAM_NAND_H_

#include "lpc177x_8x_emc.h"
#include "lpc177x_8x_gpio.h"
#include "lpc177x_8x_pinsel.h"

/***************************************************************************/
/****************************��������������� ���������**********************/
/***************************************************************************/
#define FIO_BASE_ADDR                        0x20098000
#define FIO0DIR                              (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x00))
#define FIO0MASK                             (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x10))
#define FIO0PIN                              (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x14))
#define FIO0SET                              (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x18))
#define FIO0CLR                              (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x1C))
//---------------------------------------------------------------------------
//---------------------------��������� �������-------------------------------
//---------------------------------------------------------------------------

//****************************************************************************
//***************************�������� �������*********************************
//****************************************************************************

void Init_sdram_nand(void)
{
	volatile uint32_t i;
	volatile unsigned long Dummy;

	//EMC
	for (i = 16; i <= 18; i++) //EMC_CAS //EMC_RAS //EMC_CLK0
	{
		PINSEL_ConfigPin(2, i, 0x21);
	}
	PINSEL_ConfigPin(2, 20, 0x21); //EMC_DYCS0
	PINSEL_ConfigPin(2, 24, 0x21); //EMC_CKE0
	PINSEL_ConfigPin(2, 28, 0x21); //EMC_DQM0
	PINSEL_ConfigPin(2, 29, 0x21); //EMC_DQM1
	for (i = 0; i <= 15; i++) //EMC_D0 - EMC_D15
	{
		PINSEL_ConfigPin(3, i, 0x21);
	}
	for (i = 0; i <= 14; i++) //EMC_A0 - EMC_A14
	{
		PINSEL_ConfigPin(4, i, 0x21);
	}
	PINSEL_ConfigPin(4, 25, 0x21); //EMC_WE
	//NAND
	PINSEL_ConfigPin(4, 19, 0x21);	//A19
	PINSEL_ConfigPin(4, 20, 0x21);	//A20
	PINSEL_ConfigPin(4, 24, 0x21);	//nOE
	PINSEL_ConfigPin(4, 31, 0x21);	//nCS1
	PINSEL_ConfigPin(0, 16, 0);	//ReadyBusy pin

	GPIO_SetDir(0, 1 << 16, GPIO_DIRECTION_INPUT);
	/*
	 ************************************
	 init SDRAM CONTROLLER
	 ************************************
	 */
	/* Enable the EMC POWER */
	LPC_SC->PCONP |= 0x00000800;

	/*
	 Delay Control register (EMCDLYCTL - 0x400F C1DC)
	 The EMCDLYCTL register controls on-chip programmable delays that can b used to fine
	 tune timing to external SDRAM memories. Dela ys can be configured in increments of
	 approximately 250 picoseconds up to a maximum of roughly 7.75 ns.
	 */

	/*
	 Programmable delay value for EMC outputs in command delayed mode
	 The delay amount is roughly (CMDDLY+1) * 250 picoseconds
	 */
	LPC_SC->EMCDLYCTL |= (8 << 0);

	/*
	 Programmable delay value for the feedback clock that controls input data sampling
	 The delay amount is roughly (FBCLKDLY+1) * 250 picoseconds
	 */
	LPC_SC->EMCDLYCTL |= (8 << 8);

	/*
	 Programmable delay value for the CLKOUT0 output. This would typically be used in clock
	 delayed mode. The delay amount is roughly (CLKOUT0DLY+1) * 250 picoseconds.
	 */
	LPC_SC->EMCDLYCTL |= (0x08 << 16);

	/*
	 Confige the EMC Register
	 */
	/*
	 EMC Control register (EMCControl - 0x2009 C000)
	 EMC Enable (E) = 1;
	 Address mirror (M) = 0;	Normal memory map.
	 Low-power mode (L) = 0; Normal mode (warm reset value).
	 */
	LPC_EMC->Control = 1;

	/*
	 Dynamic Memory Read Configuration register (EMCDynamicReadConfig - 0x2009 C028)
	 1:0 Read data  strategy (RD)
	 00 Clock out delayed strategy, using CLKOUT (command not delayed, clock out
	 delayed). POR reset value.
	 01 Command delayed strategy, using EMCCLKDELAY (command delayed, clock out
	 not delayed).
	 10 Command delayed strategy plus one clock cycle, using EMCCLKDELAY
	 (command delayed, clock out not delayed).
	 11 Command delayed strategy plus  two clock cycles, using EMCCLKDELAY
	 (command delayed, clock out not delayed)
	 */
	LPC_EMC->DynamicReadConfig = 1;

	/*
	 Dynamic Memory RAS & CAS Delay registers
	 The EMCDynamicRasCas0-3 registers enable you to program the RAS and CAS
	 latencies for the relevant dynamic memory
	 1:0 RAS latency (active to read/write delay) (RAS)
	 00 Reserved. 11
	 01 One CCLK cycle.
	 10 Two CCLK cycles.
	 11 Three CCLK cycles (POR reset value).
	 9:8 CAS latency (CAS)
	 00 Reserved. 11
	 01 One CCLK cycle.
	 10 Two CCLK cycles.
	 11 Three CCLK cycles (POR reset value).
	 */
	LPC_EMC->DynamicRasCas0 = 0;
	LPC_EMC->DynamicRasCas0 |= (3 << 8);
	LPC_EMC->DynamicRasCas0 |= (2 << 0);

	/*
	 Dynamic Memory RAS & CAS Delay registers
	 The EMCDynamicRasCas0-3 registers enable you to program the RAS and CAS
	 latencies for the relevant dynamic memory
	 1:0 RAS latency (active to read/write delay) (RAS)
	 00 Reserved. 11
	 01 One CCLK cycle.
	 10 Two CCLK cycles.
	 11 Three CCLK cycles (POR reset value).
	 9:8 CAS latency (CAS)
	 00 Reserved. 11
	 01 One CCLK cycle.
	 10 Two CCLK cycles.
	 11 Three CCLK cycles (POR reset value).
	 */
	LPC_EMC->DynamicRasCas0 = 0;
	LPC_EMC->DynamicRasCas0 |= (3 << 8);
	LPC_EMC->DynamicRasCas0 |= (3 << 0);

	/*
	 Dynamic Memory Precharge Command Period registe (EMCDynamictRP - 0x2009 C030)
	 The EMCDynamicTRP register enables you to program the precharge command period,
	 tRP.
	 3:0 Precharge command period (tRP)
	 0x0 - 0xE n + 1 clock cycles. The delay is in EMCCLK cycles.
	 0xF 16 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicRP = P2C(SDRAM_TRP);

	/*
	 Dynamic Memory Active to Precharge Command Period register(EMCDynamictRAS - 0x2009 C034)
	 The EMCDynamicTRAS register enables you to program the active to precharge command period, tRAS.
	 3:0 Precharge command period (tRAS)
	 0x0 - 0xE n + 1 clock cycles. The delay is in EMCCLK cycles.
	 0xF 16 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicRAS = P2C(SDRAM_TRAS);

	/*
	 Dynamic Memory Se lf-refresh Exit Time register(EMCDynamictSREX - 0x2009 C038)
	 The EMCDynamicTSREX register enables you  to program the self-refresh exit time
	 3:0 Self-refresh exit time (tSREX)
	 0x0 - 0xE n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF 16 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicSREX = P2C(SDRAM_TXSR);

	/*
	 Dynamic Memory Last Data Out to Active Time register (EMCDynamictAPR - 0x2009 C03C)
	 The EMCDynamicTAPR register enables you to program the last-data-out to active
	 command time, tAPR.
	 3:0 Last-data-out to active command time (tAPR)
	 0x0 - 0xE n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF 16 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicAPR = SDRAM_TAPR;

	/*
	 Dynamic Memory Data-in to  Active Command Time register (EMCDynamictDAL - 0x2009 C040)
	 The EMCDynamicTDAL register enables you to program the data-in to active command time, tDAL
	 3:0 Data-in to active command (tDAL)
	 0x0 - 0xE n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF 16 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicDAL = SDRAM_TDAL + P2C(SDRAM_TRP);

	/*
	 Dynamic Memory Wr ite Recovery Time regist er (EMCDynamictWR - 0x2009 C044)
	 The EMCDynamicTWR register enables you to pr ogram the write recovery time, tWR.
	 3:0 Write recovery time (tWR)
	 0x0 - 0xE n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF 16 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicWR = SDRAM_TWR;

	/*
	 Dynamic Memory Active to  Active Command Period register (EMCDynamictRC - 0x2009 C048)
	 The EMCDynamicTRC register enables you to program the active to active command period, tRC.
	 3:0 Active to active command period (tRC)
	 0x0 - 0x1E n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF 32 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicRC = P2C(SDRAM_TRC);

	/*
	 Dynamic Memory Auto-refresh Period register (EMCDynamictRFC - 0x2009 C04C)
	 The EMCDynamicTRFC register enables you to program the auto-refresh period,
	 and auto-refresh to active command period, tRFC.
	 4:0 Auto-refresh period and auto-refresh to active command period (tRFC)
	 0x0 - 0x1E n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF 32 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicRFC = P2C(SDRAM_TRFC);

	/*
	 Dynamic Memory Exit Self-ref resh register (EMCDynamictXSR - 0x2009 C050)
	 The EMCDynamicTXSR register enables you to program the exit self-refresh to active
	 command time, tXSR.
	 4:0 Exit self-refresh to active command time (tXSR)
	 0x0 - 0x1E n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF 32 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicXSR = P2C(SDRAM_TXSR);

	/*
	 Dynamic Memory Active Bank A to Active Ba nk B Time register(EMCDynamictR RD - 0x2009 C054)
	 The EMCDynamicTRRD register enables you to program the active bank A to active bank B latency, tRRD.
	 3:0 Active bank A to active bank B latency (tRRD)
	 0x0 - 0x1E n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF        16 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicRRD = P2C(SDRAM_TRRD);

	/*
	 Dynamic Memory Load Mode  register to Active Command Time (EMCDynamictMRD - 0x2009 C058)
	 The EMCDynamicTMRD register enables you to program the load mode register to active command time, tMRD.
	 3:0 Load mode register to active command time (tMRD)
	 0x0 - 0x1E n + 1 clock cycles. The delay is in CCLK cycles.
	 0xF        16 clock cycles (POR reset value).
	 */
	LPC_EMC->DynamicMRD = SDRAM_TMRD;

	/*
	 Dynamic Memory Configurati on registers (EMCDy namicConfig0-3 - 0x2009 C100, 120, 140, 160)
	 The EMCDynamicConfig0-3 registers enable you to program the configuration information
	 for the relevant dynamic memory chip select .
	 2:0 - Reserved. Read value is undefined, only zero should be written. NA
	 4:3 Memory device (MD) 00 SDRAM (POR reset value). 00
	 01 Low-power SDRAM.
	 1x Reserved.
	 6:5 - Reserved. Read value is undefined, only zero should be written. NA
	 12:7 Address mapping (AM)
	 000000 = reset value.
	 13 - Reserved. Read value is undefined, only zero should be written. NA
	 14 Address mapping (AM)
	 0 = reset value.
	 18:15 - Reserved. Read value is undefined, only zero should be written. NA
	 19 Buffer enable (B)
	 0 Buffer disabled for accesses to this chip select (POR reset value)
	 1 Buffer enabled for accesses to this chip select.
	 20 Write protect (P)
	 0 Writes not protected (POR reset value)
	 1 Writes protected.

	 cofige para
	 4:3  = 00     = SDRAM
	 12:7 = 01100
	 14	 = 0      = 16bit
	 0100 0110 0000 0000
	 16bit 256 Mb (16Mx16), 4 banks, row length = 13, column length = 9
	 */
	LPC_EMC->DynamicConfig0 = 0x0000680;

	/*
	 Dynamic Memory Control register (EMCDynamicControl - 0x2009 C020)
	 The EMCDynamicControl register controls dynamic memory operation.
	 */
	/* General SDRAM Initialization Sequence NOP command*/
	LPC_EMC->DynamicControl = 0x0183;

	/* DELAY to allow power and clocks to stabilize ~100 us */
	for (i = 200 * 40; i; i--)
		;
	/* SDRAM Initialization Sequence PALL command*/
	LPC_EMC->DynamicControl = 0x0103;

	/*
	 Dynamic Memory  Refresh Timer register  (EMCDynamicRefresh - 0x2009 C024)
	 The EMCDynamicRefresh register configures dynamic memory operation.
	 */
	LPC_EMC->DynamicRefresh = 2;
	for (i = 256; i; --i)
		; // > 128 clk
	LPC_EMC->DynamicRefresh = P2C(SDRAM_REFRESH) >> 4;

	/* SDRAM Initialization Sequence MODE command */
	LPC_EMC->DynamicControl = 0x00000083;
	Dummy = *((volatile uint32_t *) (SDRAM_BASE_ADDR | (0x33 << 12)));
	Dummy++;
	// NORM
	LPC_EMC->DynamicControl = 0x0000;
	LPC_EMC->DynamicConfig0 |= (1 << 19);
	for (i = 100000; i; i--)
		;
	//NAND FLASH
	LPC_EMC->StaticConfig1 &= ~ EMC_STATIC_CFG_MEMWIDTH_BMASK;
	LPC_EMC->StaticConfig1 |= EMC_STATIC_CFG_MW_8BITS;
	LPC_EMC->StaticConfig1 &= ~EMC_STATIC_CFG_PAGEMODE_MASK;
	LPC_EMC->StaticConfig1 |= EMC_CFG_PM_DISABLE & EMC_STATIC_CFG_PAGEMODE_MASK;

	LPC_EMC->StaticConfig1 &= ~EMC_STATIC_CFG_BYTELAND_MASK;
	LPC_EMC->StaticConfig1 |= EMC_CFG_BYTELAND_READ_BITSLOW & EMC_STATIC_CFG_BYTELAND_MASK;

	LPC_EMC->StaticConfig1 &= ~EMC_STATIC_CFG_EXTWAIT_MASK;
	LPC_EMC->StaticConfig1 |= EMC_CFG_EW_DISABLED & EMC_STATIC_CFG_EXTWAIT_MASK;

	LPC_EMC->StaticWaitWen1 = EMC_StaticWaitWen_WAITWEN(2);
	LPC_EMC->StaticWaitOen1 = EMC_StaticWaitOen_WAITOEN(2);
	LPC_EMC->StaticWaitRd1 = EMC_StaticWaitRd_WAITRD(0x1f);
	LPC_EMC->StaticWaitPage1 = EMC_StaticwaitPage_WAITPAGE(0x1f);
	LPC_EMC->StaticWaitWr1 = EMC_StaticWaitwr_WAITWR(0x1f);
	LPC_EMC->StaticWaitTurn1 = EMC_StaticWaitTurn_WAITTURN(0x1f);

	for (i = 100000; i; i--)
		;

	 NandFlash_Reset();
	 if(NandFlash_ReadId() != K9FXX_ID) while(1);
}

/***************************************************************************
 *   	��� �������: NandFlash_Reset
 *   	��������:    ������������ NAND Falsh
 ****************************************************************************/
/*void NandFlash_Reset( void )
 {
 volatile uint8_t *pCLE;


 pCLE = K9F1G_CLE;
 *pCLE = K9FXX_RESET;
 NandFlash_WaitForReady();
 return;
 }*/
/***************************************************************************
 *   	��� �������: NandFlash_ReadId
 *   	��������:    ID - �����
 ****************************************************************************/
/*
 uint32_t NandFlash_ReadId( void )
 {
 uint8_t b, c, d, e;
 volatile uint8_t *pCLE;
 volatile uint8_t *pALE;
 volatile uint8_t *pDATA;

 pCLE  = K9F1G_CLE;
 pALE  = K9F1G_ALE;
 pDATA = K9F1G_DATA;

 *pCLE = K9FXX_READ_ID;
 *pALE = 0;

 b = *pDATA;
 b = *pDATA;
 c = *pDATA;
 d = *pDATA;
 e = *pDATA;

 return ((b << 24) | (c << 16) | (d << 8) | e);
 }*/
/*
 bool Tets_sdram(uint32_t TestData)
 {
 for(uint32_t i = SDRAM_BASE_ADDR; i < SDRAM_BASE_ADDR + SDRAM_BASE_SIZE; i+=4)
 {
 *(uint32_t*)(i) = TestData;
 }
 for(uint32_t i = SDRAM_BASE_ADDR; i < SDRAM_BASE_ADDR + SDRAM_BASE_SIZE; i+=4)
 {
 if(*(uint32_t*)(i) != TestData) return false;
 }
 for(uint32_t i = SDRAM_BASE_ADDR; i < SDRAM_BASE_ADDR + SDRAM_BASE_SIZE; i+=4)
 {
 *(uint32_t*)(i) = 0xffffffff;
 }
 for(uint32_t i = SDRAM_BASE_ADDR; i < SDRAM_BASE_ADDR + SDRAM_BASE_SIZE; i+=4)
 {
 if(*(uint32_t*)(i) != 0xffffffff) return false;
 }
 return true;
 }*/

#endif
