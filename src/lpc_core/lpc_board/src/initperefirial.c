#ifndef WIN32
#include "lpc177x_8x_rtc.h"
#include "lpc177x_8x_eeprom.h"
#include "lpc177x_8x_gpio.h"
#include "lpc177x_8x_gpdma.h"
#include "lpc177x_8x_crc.h"
#include "lpc177x_8x_uart.h"
#include "nandflash.h"
#else
#include "emulation_lpc177x_8x_eeprom.h"
#include "emulation_lpc177x_8x_crc.h"
#endif// !WIN32

#include "initperefirial.h"
#include "TimeSync.h"
#include "eeprom.h"

void init_perefirial()
{

	// Init_nandflash();
	 //Debug_init();
#ifndef WIN32
	 GPIO_Init();
	 GPDMA_Init();
#endif
	 CT_RTC_Init();
	 CRC_Init(CRC_POLY_CRC32);

	 EEPROM_Init();
#ifndef WIN32
#if !defined(_TEST_BOARD_)
	// Init_wtd();
#endif
#endif
}
