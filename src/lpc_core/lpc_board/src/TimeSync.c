/*************************************************************************
 * 		���: TimeSync.�
 *
 * 		��������
 *		������ �� ��������
 *
 *		�����
 *		�������� �.�
 *
 ****************************************************************************/

//---------------------------------------------------------------------------
//---------------------------������------------------------------------------
//---------------------------------------------------------------------------
#include "TimeSync.h"
#ifdef __TIMESYNC_H__
#include <string.h>
#ifndef WIN32
  #include "lpc177x_8x_rtc.h"
#endif
//---------------------------------------------------------------------------
//---------------------------�����-------------------------------------------
//---------------------------------------------------------------------------
static struct tm Time;

/***************************************************************************
 *   	��� �������: RTC_Init
 *   	����������:  void
 *   	��������:    ������������� �������
 ****************************************************************************/
void CT_RTC_Init()
{
//����� �������� � RTC
#ifndef WIN32
 RTC_Init(LPC_RTC);//������������� RTC
 //RTC_ResetClockTickCounter(LPC_RTC);
 RTC_Cmd(LPC_RTC, ENABLE);
//������ ����� ����� ���������
		CT_SetTimeData(12,12, 12, 14, 03, 2015);
#endif //!WIN32
}

void CT_Format(char *TEXT, uint16_t Lenght, char *Format)
{
	strftime(TEXT,Lenght,Format,CT_GetTime());
}

/***************************************************************************
 *   	��� �������: CT_GetTime
 *   	����������:  ��������� �� struct tm
 *   	��������:    ���������� ������� �����
 ****************************************************************************/
struct tm * CT_GetTime()
{
#ifndef WIN32
    Time.tm_year = RTC_GetTime (LPC_RTC, RTC_TIMETYPE_YEAR)-1900;
    Time.tm_mon = RTC_GetTime (LPC_RTC, RTC_TIMETYPE_MONTH)-1;
    Time.tm_mday = RTC_GetTime (LPC_RTC, RTC_TIMETYPE_DAYOFMONTH);
    Time.tm_hour = RTC_GetTime (LPC_RTC, RTC_TIMETYPE_HOUR);
    Time.tm_min = RTC_GetTime (LPC_RTC, RTC_TIMETYPE_MINUTE);
    Time.tm_sec = RTC_GetTime (LPC_RTC, RTC_TIMETYPE_SECOND);
    return &Time;
#else //!WIN32
	time_t timer;
	time (&timer);
	return localtime (&timer);
#endif
}

/***************************************************************************
 *   	��� �������: CT_GetDataTime
 *   	����������:  time_t ����� ����� ������
 *   	��������:    ���������� ������� �����
 ****************************************************************************/
time_t CT_GetDataTime()
{
  return mktime(CT_GetTime());
}

/***************************************************************************
 *   	��� �������: CT_GetHour
 *   	����������:  unsigned char
 *   	��������:    ���������� ������� ���
 ****************************************************************************/
uint8_t CT_GetHour()
{
#ifndef WIN32
	//����� ����� �� ��������
    return RTC_GetTime (LPC_RTC, RTC_TIMETYPE_HOUR);
#else //!WIN32
    struct tm *CurrentTime = CT_GetTime();
    return CurrentTime->tm_hour;
#endif
}
/***************************************************************************
 *   	��� �������: CT_GetMinute
 *   	����������:  unsigned char
 *   	��������:    ���������� ������� ������
 ****************************************************************************/
uint8_t CT_GetMinute()
{
#ifndef WIN32
	//����� ����� �� ��������
      return RTC_GetTime (LPC_RTC, RTC_TIMETYPE_MINUTE);
#else //!WIN32
	struct tm *CurrentTime = CT_GetTime();
    return CurrentTime->tm_min;
#endif
}
/***************************************************************************
 *   	��� �������: CT_GetSecond
 *   	����������:  unsigned char
 *   	��������:    ���������� ������� �������
 ****************************************************************************/
uint8_t CT_GetSecond()
{
#ifndef WIN32
	//����� ����� �� ��������
    return RTC_GetTime (LPC_RTC, RTC_TIMETYPE_SECOND);
#else //!WIN32
	struct tm *CurrentTime = CT_GetTime();
    return CurrentTime->tm_sec;
#endif
}
/***************************************************************************
 *   	��� �������: CT_Get_Month
 *   	����������:  unsigned char
 *   	��������:    ���������� �������  �����
 ****************************************************************************/
uint8_t CT_Get_Month()
{
#ifndef WIN32
	//����� ����� �� ��������
    return RTC_GetTime (LPC_RTC, RTC_TIMETYPE_MONTH);
#else //!WIN32
	struct tm *CurrentTime = CT_GetTime();
    return CurrentTime->tm_mon;
#endif
}

/***************************************************************************
 *   	��� �������: CT_Get_Year
 *   	����������:  unsigned short
 *   	��������:    ���������� �������  ���
 ****************************************************************************/
uint16_t CT_Get_Year()
{
#ifndef WIN32
	//����� ����� �� ��������
  return RTC_GetTime (LPC_RTC, RTC_TIMETYPE_YEAR);
#else //!WIN32
	struct tm *CurrentTime = CT_GetTime();
    return (CurrentTime->tm_year+1900);
#endif
}

/***************************************************************************
 *   	��� �������: CT_Get_Day
 *   	����������:  unsigned char
 *   	��������:    ���������� �������  ����
 ****************************************************************************/
uint8_t CT_Get_Day()
{
#ifndef WIN32
	//����� ����� �� ��������
    return RTC_GetTime (LPC_RTC, RTC_TIMETYPE_DAYOFMONTH);
#else //!WIN32
	struct tm *CurrentTime = CT_GetTime();
    return CurrentTime->tm_mday;
#endif
}

/***************************************************************************
 *   	��� �������: CT_SetTime
 *   	����������:  void
 *   	��������:    ��������� �������� �������
 ****************************************************************************/
void CT_SetTime(int Hour,int Minute, int Second)
{
#ifndef WIN32
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_HOUR, Hour);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MINUTE, Minute);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_SECOND, Second);
#else //!WIN32
#endif
}
/***************************************************************************
 *   	��� �������: CT_SetTimeData
 *   	����������:  void
 *   	��������:    ��������� �������� ������� � ����
 ****************************************************************************/
void CT_SetTimeData_t(time_t LocalTime)
{
	Time = *gmtime(&LocalTime);
#ifndef WIN32
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_HOUR, Time.tm_hour);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MINUTE, Time.tm_min);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_SECOND, Time.tm_sec);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_YEAR, Time.tm_year);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MONTH, Time.tm_mon);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_DAYOFMONTH, Time.tm_mday);
#else //!WIN32
#endif
}
/***************************************************************************
 *   	��� �������: CT_SetTimeData
 *   	����������:  void
 *   	��������:    ��������� �������� ������� � ����
 ****************************************************************************/
void CT_SetTimeData(int Hour,int Minute, int Second, int Day, int Month, int Year)
{

#ifndef WIN32
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_HOUR, Hour);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MINUTE, Minute);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_SECOND, Second);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_YEAR, Year);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MONTH, Month);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_DAYOFMONTH, Day);
#else //!WIN32
#endif
}

/***************************************************************************
 *   	��� �������: CT_SetData
 *   	����������:  void
 *   	��������:    ��������� �������� �������
 ****************************************************************************/
void CT_SetData(int Day, int Month, int Year)
{
#ifndef WIN32
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_YEAR, Year);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MONTH, Month);
	RTC_SetTime (LPC_RTC, RTC_TIMETYPE_DAYOFMONTH, Day);
#else //!WIN32
#endif
}
#endif
