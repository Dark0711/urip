/*
 * eeprom_userdata.c
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: ������
 */
#include "eeprom.h"
#include <string.h>
#ifndef WIN32
#include "lpc177x_8x_eeprom.h"
#else // !WIN32
#include "emulation_lpc177x_8x_eeprom.h"
#endif

//---------------------------------------------------------------------------
//---------------------------��������� ����������----------------------------
//---------------------------------------------------------------------------
typedef struct
{
	uint8_t Data[EEPROM_PAGE_NUM][EEPROM_PAGE_SIZE];
	uint64_t ChangePageFlag;
}tEEPROM_LocalData;
#ifndef WIN32
#pragma location="EEPROM_RAM"
static __no_init tEEPROM_LocalData EEPROM_LocalData; //��������� ��������� EEPROM � SDRAM
#else
static tEEPROM_LocalData EEPROM_LocalData; //��������� ��������� EEPROM � SDRAM
#endif

void EEPROM_Init_LocalUserData(void)
{
	EEPROM_LocalData.ChangePageFlag = 0;
	for (uint8_t PageNum = 0; PageNum < EEPROM_PAGE_NUM; PageNum++)
	{
		memset(EEPROM_LocalData.Data[PageNum], 0, EEPROM_PAGE_SIZE);
		EEPROM_Read(0, PageNum, EEPROM_LocalData.Data[PageNum], MODE_8_BIT, EEPROM_PAGE_SIZE);
	}
}

void EEPROM_Write_LocalUserData(tEEPROM_LocalUserData PageNum, void* data)
{
	EEPROM_LocalData.ChangePageFlag |= (uint64_t)(1 << PageNum);
	memcpy(EEPROM_LocalData.Data[PageNum], data, EEPROM_PAGE_SIZE);
}

void EEPROM_Write_LocalUserData2(tEEPROM_LocalUserData PageNum, void* data, uint32_t size_data)
{
	if (size_data <= EEPROM_PAGE_SIZE)
	{
		EEPROM_LocalData.ChangePageFlag |= (uint64_t)(1 << PageNum);
		memcpy(EEPROM_LocalData.Data[PageNum], data, size_data);
	}
}

void EEPROM_Read_LocalUserData(tEEPROM_LocalUserData PageNum, void* data)
{
	memcpy(data, EEPROM_LocalData.Data[PageNum], EEPROM_PAGE_SIZE);
}

void EEPROM_Read_LocalUserData2(tEEPROM_LocalUserData PageNum, void* data, uint32_t size_data)
{
	if (size_data <= EEPROM_PAGE_SIZE)
		memcpy(data, EEPROM_LocalData.Data[PageNum], size_data);
}

void EEPROM_Sync_LocalUserData(void)
{
	if (EEPROM_LocalData.ChangePageFlag)
	{
		for (uint8_t PageNum = 0; PageNum < EEPROM_PAGE_NUM; PageNum++)
		{
			if (EEPROM_LocalData.ChangePageFlag & ((uint64_t)(1 << PageNum)))
			{
				EEPROM_Erase(PageNum);
				EEPROM_Write(0, PageNum, EEPROM_LocalData.Data[PageNum], MODE_8_BIT, EEPROM_PAGE_SIZE);
			}
		}
		EEPROM_LocalData.ChangePageFlag = 0;
	}
}

bool EEPROM_Compare_LocalUserData(tEEPROM_LocalUserData PageNum, void* data)
{
	return memcmp(data, EEPROM_LocalData.Data[PageNum], EEPROM_PAGE_SIZE);
}

void EEPROM_Erase_LocalUserData()
{
	for (uint8_t PageNum = 0; PageNum < EEPROM_PAGE_NUM; PageNum++)
	{
		EEPROM_Erase(PageNum);
		memset(EEPROM_LocalData.Data[PageNum], 0, EEPROM_PAGE_SIZE);
	}
}

