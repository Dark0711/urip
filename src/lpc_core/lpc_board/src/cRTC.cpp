/*
 * cRTC.cpp
 *
 *  ������: 29 ��� 2015 �.
 *  �����:  �������� �.�
 */
#include "cRTC.hpp"

bool cRTC::InitFlag = true;

cRTC::cRTC() :
		RTCx(LPC_RTC)
{
	if (InitFlag)
	{
		LPC_SC->PCONP |= (1 << 9);
		RTCx->ILR = 0x00;
		RTCx->CCR = 0x00;
		RTCx->CIIR = 0x00;
		RTCx->AMR = 0xFF;
		RTCx->CALIBRATION = 0x00;
		RTCx->CCR |= 0x01;
		InitFlag = false;
	}
}

cRTC::~cRTC()
{
	if (InitFlag)
	{
		RTCx->CCR = 0x00;
		LPC_SC->PCONP &= ~(1 << 9);
		InitFlag = true;
	}
}

void cRTC::SetTime(TIMETYPE Timetype, uint32_t TimeValue)
{
	switch (Timetype)
	{
		case TIMETYPE_SECOND:
			if (TimeValue <= SECOND_MAX)
				RTCx->SEC = TimeValue & SEC_MASK;
			break;

		case TIMETYPE_MINUTE:
			if (TimeValue <= MINUTE_MAX)
				RTCx->MIN = TimeValue & MIN_MASK;
			break;

		case TIMETYPE_HOUR:
			if (TimeValue <= HOUR_MAX)
				RTCx->HOUR = TimeValue & HOUR_MASK;
			break;

		case TIMETYPE_DAYOFWEEK:
			if (TimeValue <= DAYOFWEEK_MAX)
				RTCx->DOW = TimeValue & DOW_MASK;
			break;

		case TIMETYPE_DAYOFMONTH:
			if ((TimeValue >= DAYOFMONTH_MIN) && (TimeValue <= DAYOFMONTH_MAX))
				RTCx->DOM = TimeValue & DOM_MASK;
			break;

		case TIMETYPE_DAYOFYEAR:
			if ((TimeValue >= DAYOFYEAR_MIN) && (TimeValue <= DAYOFYEAR_MAX))
				RTCx->DOY = TimeValue & DOY_MASK;
			break;

		case TIMETYPE_MONTH:
			if ((TimeValue >= MONTH_MIN) && (TimeValue <= MONTH_MAX))
				RTCx->MONTH = TimeValue & MONTH_MASK;
			break;

		case TIMETYPE_YEAR:
			if (TimeValue <= YEAR_MAX)
				RTCx->YEAR = TimeValue & YEAR_MASK;
			break;
	}
}

uint32_t cRTC::GetTime(uint32_t Timetype) const
{
	switch (Timetype)
	{
		case TIMETYPE_SECOND:
			return (RTCx->SEC & SEC_MASK);
		case TIMETYPE_MINUTE:
			return (RTCx->MIN & MIN_MASK);
		case TIMETYPE_HOUR:
			return (RTCx->HOUR & HOUR_MASK);
		case TIMETYPE_DAYOFWEEK:
			return (RTCx->DOW & DOW_MASK);
		case TIMETYPE_DAYOFMONTH:
			return (RTCx->DOM & DOM_MASK);
		case TIMETYPE_DAYOFYEAR:
			return (RTCx->DOY & DOY_MASK);
		case TIMETYPE_MONTH:
			return (RTCx->MONTH & MONTH_MASK);
		case TIMETYPE_YEAR:
			return (RTCx->YEAR & YEAR_MASK);
		default:
			return (0);
	}
}


cTime cRTC::GetTime()
{
	cTime Time;
	Time.Second = GetTime(TIMETYPE_SECOND);
	Time.Minute = GetTime(TIMETYPE_MINUTE);
	Time.Hour = GetTime(TIMETYPE_HOUR);
	Time.Day = GetTime(TIMETYPE_DAYOFMONTH);
	Time.Mount = GetTime(TIMETYPE_MONTH);
	Time.Year = GetTime(TIMETYPE_YEAR);
	return Time;
}

void cRTC::SetTime(cTime &Time)
{
	SetTime(TIMETYPE_SECOND,Time.Second);
	SetTime(TIMETYPE_MINUTE,Time.Minute);
	SetTime(TIMETYPE_HOUR,Time.Hour);
	SetTime(TIMETYPE_DAYOFMONTH,Time.Day);
	SetTime(TIMETYPE_MONTH,Time.Mount);
	SetTime(TIMETYPE_YEAR,Time.Year);
}
