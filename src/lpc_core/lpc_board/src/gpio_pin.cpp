/*
 * gpio_pin.cpp
 *
 *  ������: 28 ��� 2015 �.
 *  �����:  �������� �.�
 */

#include "gpio_pin.hpp"
#include <stddef.h>

template<uint8_t portNum, uint32_t pinNum, uint8_t dir>
gpio_pin<portNum, pinNum, dir>::gpio_pin()
{
	port = portNum;
	pin = pinNum;
	Direction = dir;
	pGPIO = NULL;
	try
	{
		switch (portNum)
		{
			case 0:
			pGPIO = LPC_GPIO0;
			break;

			case 1:
			pGPIO = LPC_GPIO1;
			break;

			case 2:
			pGPIO = LPC_GPIO2;
			break;

			case 3:
			pGPIO = LPC_GPIO3;
			break;

			case 4:
			pGPIO = LPC_GPIO4;
			break;

			case 5:
			pGPIO = LPC_GPIO5;
			break;

			default:
			break;
		}
		if(pGPIO == NULL) throw "NULL GPIO";
	}
	catch(char* ErrorText)
	{
		__disable_interrupt();
		while(1){}
	}
	pGPIO->DIR &= ~(1<<pinNum); //��������
	pGPIO->DIR |= (1 << pinNum); //������ �����������

}
