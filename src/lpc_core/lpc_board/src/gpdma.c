#include "lpc177x_8x_gpdma.h"
#include "gpdma.h"

#define DMA_MAXCH 8
static void(*GPDMA_IRQFUNC[DMA_MAXCH])(void);

void GPDMA_SetIRQAddr(uint8_t NumChanal,void Func(void))//��������� IRQ ��� DMA
{
  GPDMA_IRQFUNC[NumChanal] = Func;
}

/******************************************************************************
**  DMA Handler
******************************************************************************/
void DMA_IRQHandler (void)
{
  for(int i=0;i<DMA_MAXCH;i++)
  {
      if(GPDMA_IRQFUNC[i] != NULL)(GPDMA_IRQFUNC[i])();
  }
}
