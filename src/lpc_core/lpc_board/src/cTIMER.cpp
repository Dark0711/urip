/*
 * cTIMER.cpp
 *
 *  ������: 29 ��� 2015 �.
 *  �����:  �������� �.�
 */
#include "cTIMER.hpp"
cTIMER::cTIMER(LPC_TIM_TypeDef *TIMER, COUTCONTROL_OPT CounterMode, CAPTURENUM CaptureNum)
{
	TIMx = TIMER;
	TIMx->CTCR = CounterMode | (CaptureNum << 2); //����� ��������

	TIMx_IRQ = GetNumTimerIRQ();
}


void cTIMER::ConfigMatch(MATCHNUM MatchNum, bool IntOnMatch,bool StopOnMatch,bool ResetOnMatch,EXTMATCH_OPT ExtMatchOutputType, uint32_t MatchValue)
{
	//������ �������
	TIMx->MCR &= ~((uint32_t)(7<<(MatchNum*3)));
	//����������
	if(IntOnMatch) TIMx->MCR |= ((1<<(MatchNum * 3)));
	//�������� ������� ������� ��� ��������� MatchValue;
	if (ResetOnMatch)TIMx->MCR |= ((1<<((MatchNum * 3) + 1)));
	//��������������� ��� ���������� MatchValue
	if (StopOnMatch)	TIMx->MCR |= ((1<<((MatchNum * 3) + 2)));
	//��������� ������� �����
	TIMx->EMR 	&= ~ ((0x03<<((MatchNum << 1) + 4)));
	TIMx->EMR   |= (((ExtMatchOutputType & 0x03)<<((MatchNum << 1) + 4)));
	//�������
	*((uint32_t*)(&TIMx->MR0) + MatchNum) = MatchValue;

	/*if(MatchNum == TIM_MATCHNUM_0) TIMx->MR0 = MatchValue;
	else if(MatchNum == TIM_MATCHNUM_1) TIMx->MR1 = MatchValue;
	else if(MatchNum == TIM_MATCHNUM_2) TIMx->MR2 = MatchValue;
	else if(MatchNum == TIM_MATCHNUM_3) TIMx->MR3 = MatchValue;*/
}

void cTIMER::ConfigCapture(CAPTURENUM CaptureNum, bool RisingEdge, bool FallingEdge, bool IntOnCaption)
{
	TIMx->CCR &= ((uint32_t)(7<<(CaptureNum*3)));
	if(RisingEdge) TIMx->CCR |= ((1<<(CaptureNum * 3)));
	if(FallingEdge) TIMx->CCR |= ((1<<((CaptureNum * 3) + 1)));
	if(IntOnCaption)TIMx->CCR |= ((1<<((CaptureNum * 3) + 2)));
}


