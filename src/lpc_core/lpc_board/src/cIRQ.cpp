/*
 * cIRQ.cpp
 *
 *  ������: 03 ���� 2015 �.
 *  �����:  �������� �.�
 */
#include "cIRQ.hpp"


cIRQ_Management IRQ_Management;

std::list<IRQ_Basic*> cIRQ_Management::TIMER0;
std::list<IRQ_Basic*> cIRQ_Management::DMA; //

/*
void cIRQ_Management::TIMER0_IRQHandler()
{
	static std::list<IRQ_Basic*>::const_iterator irq_list;
	for(irq_list = TIMER0.begin(); irq_list != TIMER0.end(); ++irq_list)
	{
		(*irq_list)->IRQ();
	}
}*/

void cIRQ_Management::DMA_IRQHandler()
{
	static std::list<IRQ_Basic*>::const_iterator irq_list;
	for(irq_list = DMA.begin(); irq_list != DMA.end(); ++irq_list)
	{
		(*irq_list)->IRQ();
	}
}

void cIRQ_Management::RegisterIRQ(IRQn_Type IRQ_Type,IRQ_Basic *IRQ)
{
	if(IRQ_Type == TIMER0_IRQn)
	{
		TIMER0.push_back(IRQ);
	}
	else if(IRQ_Type == DMA_IRQn)
	{
		DMA.push_back(IRQ);
	}

}

