#include "tft_lcd.h"
#ifdef __LCD_H_
#include "lpc177x_8x_gpio.h"

//uint32_t VRAM_BUFF[800*480]  __attribute__ ((section(".VRAM"))) = { 0 };

//static uint32_t VRAM_BUFF;
static void lcd_setBUSpriorities(void)
{
	LPC_SC->MATRIX_ARB =  0            // Set AHB Matrix priorities [0..3] with 3 being highest priority
			| (1 <<  0)  // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
			| (2 <<  2)  // PRI_DCODE : D-Code bus priority.
			| (0 <<  4)  // PRI_SYS   : System bus priority.
			| (0 <<  6)  // PRI_GPDMA : General Purpose DMA controller priority.
			| (0 <<  8)  // PRI_ETH   : Ethernet: DMA priority.
			| (3 << 10)  // PRI_LCD   : LCD DMA priority.
			| (0 << 12)  // PRI_USB   : USB DMA priority.
			;
		//MATRIX_ARB = 0x00000C09;
}

LCD_RET_CODE Init_lcd(uint32_t LCDRamBuff)
{
	LCD_Config_Type Config;

	Config.panel_clk = (20.0*1000000L);

	Config.hConfig.ppl = LCD_WIDTH;
	Config.hConfig.hbp = 46;
	Config.hConfig.hfp = 210;
	Config.hConfig.hsw = 2;

	Config.vConfig.lpp = LCD_HEIGHT;
	Config.vConfig.vbp = 23;
	Config.vConfig.vfp = 22;
	Config.vConfig.vsw = 2;

	Config.polarity.invert_vsync = 1;
	Config.polarity.invert_hsync = 1;
	Config.polarity.invert_panel_clock = 0;
	Config.polarity.cpl = LCD_WIDTH;
	Config.polarity.active_high = 1;

	Config.lcd_type = LCD_TFT;

	Config.lcd_bpp = LCD_BPP_16_565Mode;

	Config.lcd_bgr = TRUE;

	Config.big_endian_byte = 0;
	Config.big_endian_pixel = 0;

	Config.lcd_palette = 0;

	Config.lcd_panel_lower = LCDRamBuff;
	Config.lcd_panel_upper = LCDRamBuff;
#if defined(_TEST_BOARD_)
	/*LCD PWR enable*/
	GPIO_SetDir(2, 1<<0, GPIO_DIRECTION_OUTPUT);
	GPIO_OutputValue(1, 1<<0, 0);

	/*LCD PWM enable*/
	GPIO_SetDir(1, 1<<3, GPIO_DIRECTION_OUTPUT);
	GPIO_OutputValue(1, 1<<3, 1);
#endif
	lcd_setBUSpriorities();
	return LCD_InitController (&Config);
}

void RGB_Test(uint32_t Color, uint32_t *LCDRamBuff)
{
  uint32_t i,j;
  uint32_t *pPix = NULL;


  pPix = LCDRamBuff;
  for (j = 0; j < LCD_WIDTH; j++) {
    for (i = 0; i < LCD_HEIGHT; i++) {
      *pPix++ = Color;
    }
  }
}



#endif
