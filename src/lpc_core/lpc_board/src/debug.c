/*************************************************************************
* 		���: debug.c
*
* 		�������: ������� �������
*
*		�����
*		�������� �.�
*
****************************************************************************/
#include "debug.h"
#include <string.h>
#include "printf_stdarg.h"


#include "lpc177x_8x_uart.h"
#include "FreeRTOSInclude.h"
#include "FreeRTOS_CLI.h"
#include "UserConfig.h"
#include "RegisterUserCommand.h"
//---------------------------------------------------------------------------
//---------------------------��������� ����������----------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------��������� �������-------------------------------
//---------------------------------------------------------------------------

/***************************************************************************
 *   	��� �������: UART1_IRQHandler
 *   	��������:    ���������� �� UART0
 ****************************************************************************/
//uint32_t CLI::UART0_SendString(char const *txbuf)


void Debug_printf(const char *format, ...)
{
#pragma location="DEBUG_RAM"
	static __no_init char Debug_printf_buff[1024];
	va_list args;
	memset(Debug_printf_buff, 0, sizeof(Debug_printf_buff));
	char *pDebug_buff = Debug_printf_buff;
	va_start(args, format);
	__print(&pDebug_buff, format, args);
	//xSemaphoreTakeRecursive(Debug_Mutex, portMAX_DELAY);
	UART_SendString(UART_0, Debug_printf_buff);
	//xSemaphoreGiveRecursive(Debug_Mutex);
}
