
/*************************************************************************
 * 		���: nandflash_k9f1g08u0c.c
 *
 * 		��������: ������� ��� ������ � NAND Flash
 *
 *		�����
 *		�������� �.� ���������� �.�
 *
 ****************************************************************************/
//---------------------------------------------------------------------------
//---------------------------������------------------------------------------
//---------------------------------------------------------------------------
#include "nandflash.h"

#include "lpc177x_8x_emc.h"
#include "lpc177x_8x_clkpwr.h"
#include "lpc177x_8x_pinsel.h"

#include <string.h>
/***************************************************************************/
/****************************��������������� ���������**********************/
/***************************************************************************/
#define FIO_BASE_ADDR                        0x20098000
#define FIO0DIR                              (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x00))
#define FIO0MASK                             (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x10))
#define FIO0PIN                              (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x14))
#define FIO0SET                              (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x18))
#define FIO0CLR                              (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x1C))
/***************************************************************************/
/****************************������������***********************************/
/***************************************************************************/
typedef enum
{
	NAND_READSTATUS_WRITE = 0x00,
	NAND_READSTATUS_ERASE,
	NAND_READSTATUS_READ,
} NAND_READSTATUS;
//---------------------------------------------------------------------------
//---------------------------��������� �������-------------------------------
//---------------------------------------------------------------------------
static void NandFlash_WaitForReady(void);
static uint32_t NandFlash_ReadStatus(NAND_READSTATUS Cmd);
//****************************************************************************
//***************************�������� �������*********************************
//****************************************************************************
#pragma location="NAND_CHECKRAM"
static __no_init uint8_t NandFlash_FullPageBuff[NANDFLASH_PAGE_FSIZE];
/***************************************************************************
 *   	��� �������: NandFlash_WaitForReady
 *   	��������:    �������� ������� �� NAND Flash
 ****************************************************************************/
static void NandFlash_WaitForReady(void)
{
	while ( FIO0PIN & (1 << 16))
		; /* from high to low once */
	while (!(FIO0PIN & (1 << 16)))
		; /* from low to high once */
}
/***************************************************************************
 *   	��� �������: NandFlash_Init
 *   	��������:    ������������� NAND Flash
 ****************************************************************************/
void Init_nandflash( void )
 {
 uint32_t ID;
 /*EMC_STATIC_MEM_Config_Type config;
 volatile int i=0;

 config.CSn = 1;
 config.AddressMirror = 1;
 config.ByteLane = 1;
 config.DataWidth = 8;
 config.ExtendedWait = 0;
 config.PageMode = 0;
 config.WaitWEn = 2;
 config.WaitOEn = 2;
 config.WaitWr = 0x1f;
 config.WaitPage = 0x1f;
 config.WaitRd = 0x1f;
 config.WaitTurn = 0x1f;
 StaticMem_Init(&config);

 for (i = 100000; i; i--);

 //	for ( i = 0; i < NANDFLASH_NUMOF_BLOCK; i++ ) {
 //		InvalidBlockTable[i] = 0;
 //	}*/
 NandFlash_Reset();
 ID = NandFlash_ReadId();
 if(ID != K9FXX_ID) while(1);

 return;
 }

/***************************************************************************
 *   	��� �������: NandFlash_Reset
 *   	��������:    ������������ NAND Falsh
 ****************************************************************************/
void NandFlash_Reset( void )
 {
 volatile uint8_t *pCLE;

 pCLE = K9F1G_CLE;
 *pCLE = K9FXX_RESET;
 NandFlash_WaitForReady();
 return;
 }

/***************************************************************************
 *   	��� �������: NandFlash_ReadStatus
 *		���������:   uint32_t Cmd - ������ ����� �������� ������
 *   	����������:  uint32_t - ������
 *   	��������:    ������ ������ ������������ �������
 ****************************************************************************/
static uint32_t NandFlash_ReadStatus(NAND_READSTATUS Cmd)
{
	volatile uint8_t *pCLE;
	volatile uint8_t *pDATA;
	uint8_t StatusData;

	pCLE = K9F1G_CLE;
	pDATA = K9F1G_DATA;

	*pCLE = K9FXX_READ_STATUS;

	StatusData = *pDATA;

	switch (Cmd)
	{
		case NAND_READSTATUS_WRITE:
			case NAND_READSTATUS_ERASE:
			{
			if (StatusData & 0x01) return false;/* Erase/Program failure(1) or pass(0) */
			else return true;
		}
		case NAND_READSTATUS_READ:
			{
			if (StatusData & 0xC0) return true; //Bysy (0) Ready(1)
			return false;
		}
		default:
			return false;
	}
}
/***************************************************************************
 *   	��� �������: NandFlash_ReadId
 *   	��������:    ID - �����
 ****************************************************************************/
uint32_t NandFlash_ReadId( void )
 {
 uint8_t b, c, d, e;
 volatile uint8_t *pCLE;
 volatile uint8_t *pALE;
 volatile uint8_t *pDATA;

 pCLE  = K9F1G_CLE;
 pALE  = K9F1G_ALE;
 pDATA = K9F1G_DATA;

 *pCLE = K9FXX_READ_ID;
 *pALE = 0;

 b = *pDATA;
 b = *pDATA;
 c = *pDATA;
 d = *pDATA;
 e = *pDATA;

 return ((b << 24) | (c << 16) | (d << 8) | e);
 }
/***************************************************************************
 *   	��� �������: NandFlash_EraseForValidBlockCheck
 *   	��������:    ������� NAND � ��������� ����� 0xff ��� ����������� �������� ����� ������
 ****************************************************************************/
void NandFlash_EraseForValidBlockCheck()
{
	uint32_t block, page;
	memset(NandFlash_FullPageBuff,0xff,sizeof(NandFlash_FullPageBuff));
	for (block = 0; block < NANDFLASH_NUMOF_BLOCK; block++)
	{
		if (NandFlash_BlockErase(block))
		{
			for (page = 0; page < 2; page++)
			{
				NandFlash_PageProgram(block,page,NandFlash_FullPageBuff);
			}
		}
	}
}
/***************************************************************************
 *   	��� �������: NandFlash_EraseForValidBlockCheck
 *   	��������:    �������� ���������� ����� �� ����������
 ****************************************************************************/
uint32_t NandFlash_ValidBlockCheck(uint16_t block)
{
	uint32_t page;

	for (page = 0; page < 2; page++)
	{
		/* Check column address 2048 at first page and second page */
		//NandFlash_PageReadFromAddr(block, page, NANDFLASH_INVALIDBLOCK_CHECK_COLUMM, &data, 1);
		NandFlash_PageRead(block,page,NandFlash_FullPageBuff);

		if(NandFlash_FullPageBuff[NANDFLASH_INVALIDBLOCK_CHECK_COLUMM] != 0xff)
		{
			return false;
		}
		else
		{
			return true;
		}

	}
	return true;
}
/***************************************************************************
 *   	��� �������: NandFlash_BlockErase
 *		���������:   uint32_t blockNum - ����� �����
 *   	����������:  ������ ��������
 *   	��������:    ������ ��������� ����
 ****************************************************************************/
uint32_t NandFlash_BlockErase(const uint16_t blockNum)
{
	volatile uint8_t *pCLE;
	volatile uint8_t *pALE;
	uint32_t rowAddr;

	pCLE = K9F1G_CLE;
	pALE = K9F1G_ALE;

	rowAddr = blockNum * NANDFLASH_PAGE_PER_BLOCK;

	NANDFLASH_OS_ENTERCRITICAL();

	*pCLE = K9FXX_BLOCK_ERASE_1;

	*pALE = (uint8_t) (rowAddr & 0x00FF);

	*pALE = (uint8_t) ((rowAddr & 0xFF00) >> 8);

	*pCLE = K9FXX_BLOCK_ERASE_2;

	NandFlash_WaitForReady();

	NANDFLASH_OS_EXITCRITICAL();

	return (NandFlash_ReadStatus(NAND_READSTATUS_ERASE));
}
/***************************************************************************
 *   	��� �������: NandFlash_PageProgram
 *		���������:   uint32_t blockNum - ����� �����,pageNum - ����� ��������,*bufPtr - ����� ������ ����� ������
 *   	����������:  ������ ��������
 *   	��������:    ����� ������ �� ������ �� �����
 ****************************************************************************/
uint32_t NandFlash_PageProgram(const uint16_t blockNum, const uint8_t pageNum, const uint8_t *bufPtr/*, uint32_t bSpareProgram*/)
{
	volatile uint8_t *pCLE;
	volatile uint8_t *pALE;
	volatile uint8_t *pDATA;
	uint32_t i, curRow;
	uint16_t programSize = NANDFLASH_PAGE_FSIZE;

	pCLE = K9F1G_CLE;
	pALE = K9F1G_ALE;
	pDATA = K9F1G_DATA;

	curRow = (pageNum + (blockNum * NANDFLASH_PAGE_PER_BLOCK));

	NANDFLASH_OS_ENTERCRITICAL();
	/*if (bSpareProgram)
		programSize = NANDFLASH_PAGE_FSIZE;*/

	*pCLE = K9FXX_BLOCK_PROGRAM_1;                //����� ������
	*pALE = 0; /* column address low */
	*pALE = 0; /* column address high */
	*pALE = (uint8_t) (curRow & 0x00FF); /* row address low */
	*pALE = (uint8_t) ((curRow & 0xFF00) >> 8); /* row address high */
	//������
	for (i = 0; i < programSize; i++)
	{
		*pDATA = *bufPtr++;
	}
	*pCLE = K9FXX_BLOCK_PROGRAM_2;

	NandFlash_WaitForReady();

	NANDFLASH_OS_EXITCRITICAL();

	return (NandFlash_ReadStatus(NAND_READSTATUS_WRITE));
}
/***************************************************************************
 *   	��� �������: NandFlash_ReadPage
 *		���������:   uint32_t blockNum - ����� �����,pageNum - ����� ��������,*bufPtr - ����� ������ ����� ������
 *   	����������:  ������ ��������
 *   	��������:    ������ ������ �� ����� � �����
 ****************************************************************************/
uint32_t NandFlash_PageRead(const uint16_t blockNum, const uint8_t pageNum, uint8_t *bufPtr)
{
	volatile uint8_t *pCLE;
	volatile uint8_t *pALE;
	volatile uint8_t *pDATA;
	uint16_t i, curRow;

	pCLE = K9F1G_CLE;
	pALE = K9F1G_ALE;
	pDATA = K9F1G_DATA;

	curRow = (pageNum + (blockNum * NANDFLASH_PAGE_PER_BLOCK));

	NANDFLASH_OS_ENTERCRITICAL();

	*pCLE = K9FXX_READ_1;
	*pALE = 0; /* column address low */
	*pALE = 0; /* column address high */
	*pALE = (uint8_t) (curRow & 0x00FF); /* row address low */
	*pALE = (uint8_t) ((curRow & 0xFF00) >> 8); /* row address high */
	*pCLE = K9FXX_READ_2;
	NandFlash_WaitForReady();
	//Get data from the current address in the page til the end of the page
	for (i = 0; i < /*NANDFLASH_RW_PAGE_SIZE*/NANDFLASH_PAGE_FSIZE; i++)
	{
		*bufPtr = *pDATA;
		bufPtr++;
	}

	NANDFLASH_OS_EXITCRITICAL();

	return NandFlash_ReadStatus(NAND_READSTATUS_READ);
}
