/**************************************************
 *
 * This file contains an interrupt vector for Cortex-M written in C.
 * The actual interrupt functions must be provided by the application developer.
 *
 * Copyright 2007 IAR Systems. All rights reserved.
 *
 * $Revision: 66254 $
 *
 **************************************************/
#include "UserConfig.h"
#include "LPC177x_8x.h"
#include "lpc177x_8x_gpio.h"
#include "lpc177x_8x_uart.h"
#include "lpc177x_8x_nvic.h"
#include "version.h"
#include "sdram_nand.h"

#define DEBUG_P(arg ...)

#pragma language=extended
#pragma segment="CSTACK"

extern void __iar_program_start(void);

extern void default_isr(void);
static void _reset_s(int s);
static void dump_ctx(unsigned int *ctx);

extern void WDT_IRQHandler(void);
#pragma weak WDT_IRQHandler = default_isr
extern void TIMER0_IRQHandler(void);
#pragma weak TIMER0_IRQHandler = default_isr
extern void TIMER1_IRQHandler(void);
#pragma weak TIMER1_IRQHandler = default_isr
extern void TIMER2_IRQHandler(void);
#pragma weak TIMER2_IRQHandler = default_isr
extern void TIMER3_IRQHandler(void);
#pragma weak TIMER3_IRQHandler = default_isr
extern void UART0_IRQHandler(void);
#pragma weak UART0_IRQHandler = default_isr
extern void UART1_IRQHandler(void);
#pragma weak UART1_IRQHandler = default_isr
extern void UART2_IRQHandler(void);
#pragma weak UART2_IRQHandler = default_isr
extern void UART3_IRQHandler(void);
#pragma weak UART3_IRQHandler = default_isr
extern void PWM1_IRQHandler(void);
#pragma weak PWM1_IRQHandler = default_isr
extern void I2C0_IRQHandler(void);
#pragma weak I2C0_IRQHandler = default_isr
extern void I2C1_IRQHandler(void);
#pragma weak I2C1_IRQHandler = default_isr
extern void I2C2_IRQHandler(void);
#pragma weak I2C2_IRQHandler = default_isr
extern void SPI_IRQHandler(void);
#pragma weak SPI_IRQHandler = default_isr
extern void SSP0_IRQHandler(void);
#pragma weak SSP0_IRQHandler = default_isr
extern void SSP1_IRQHandler(void);
#pragma weak SSP1_IRQHandler = default_isr
extern void PLL0_IRQHandler(void);
#pragma weak PLL0_IRQHandler = default_isr
extern void RTC_IRQHandler(void);
#pragma weak RTC_IRQHandler = default_isr
extern void EINT0_IRQHandler(void);
#pragma weak EINT0_IRQHandler = default_isr
extern void EINT1_IRQHandler(void);
#pragma weak EINT1_IRQHandler = default_isr
extern void EINT2_IRQHandler(void);
#pragma weak EINT2_IRQHandler = default_isr
extern void EINT3_IRQHandler(void);
#pragma weak EINT3_IRQHandler = default_isr
extern void ADC_IRQHandler(void);
#pragma weak ADC_IRQHandler = default_isr
extern void BOD_IRQHandler(void);
#pragma weak BOD_IRQHandler = default_isr
extern void USB_IRQHandler(void);
#pragma weak USB_IRQHandler = default_isr
extern void CAN_IRQHandler(void);
#pragma weak CAN_IRQHandler = default_isr
extern void DMA_IRQHandler(void);
#pragma weak DMA_IRQHandler = default_isr
extern void I2S_IRQHandler(void);
#pragma weak I2S_IRQHandler = default_isr
extern void ENET_IRQHandler(void);
#pragma weak ENET_IRQHandler = default_isr
extern void RIT_IRQHandler(void);
#pragma weak RIT_IRQHandler = default_isr
extern void MCPWM_IRQHandler(void);
#pragma weak MCPWM_IRQHandler = default_isr
extern void QEI_IRQHandler(void);
#pragma weak QEI_IRQHandler = default_isr
extern void PLL1_IRQHandler(void);
#pragma weak PLL1_IRQHandler = default_isr
extern void USBActivity_IRQHandler(void);
#pragma weak USBActivity_IRQHandler = default_isr
extern void CANActivity_IRQHandler(void);
#pragma weak CANActivity_IRQHandler = default_isr
extern void MCI_IRQHandler(void);
#pragma weak MCI_IRQHandler = default_isr
extern void UART4_IRQHandler(void);
#pragma weak UART4_IRQHandler = default_isr
extern void SSP2_IRQHandler(void);
#pragma weak SSP2_IRQHandler = default_isr
extern void LCD_IRQHandler(void);
#pragma weak LCD_IRQHandler = default_isr
extern void GPIO_IRQHandler(void);
#pragma weak GPIO_IRQHandler = default_isr
extern void PWM0_IRQHandler(void);
#pragma weak PWM0_IRQHandler = default_isr
extern void EEPROM_IRQHandler(void);
#pragma weak EEPROM_IRQHandler = default_isr

extern void xPortSysTickHandler(void);
extern void xPortPendSVHandler(void);
extern void vPortSVCHandler(void);

void __cmain(void);
__weak void __iar_init_core(void);
__weak void __iar_init_vfp(void);

typedef void (*intfunc)(void);
typedef union
{
	intfunc __fun;
	void * __ptr;
} intvec_elem;

// The vector table is normally located at address 0.
// When debugging in RAM, it can be located in RAM, aligned to at least 2^6.
// If you need to define interrupt service routines,
// make a copy of this file and include it in your project.
// The name "__vector_table" has special meaning for C-SPY, which
// is where to find the SP start value.
// If vector table is not located at address 0, the user has to initialize
// the  NVIC vector table register (VTOR) before using interrupts.

#pragma location = ".intvec"
const intvec_elem __vector_table[] =
		{
				{ .__ptr = __sfe("CSTACK") },
				__iar_program_start,

				default_isr,							// The NMI handler
				default_isr,						// The hard fault handler
				default_isr,						// The MPU fault handler
				default_isr,						// The bus fault handler
				default_isr,						// The usage fault handler
				0,										// Reserved
				0,										// Reserved
				0,										// Reserved
				0,										// Reserved
				vPortSVCHandler,                        // SVCall handler
				default_isr,						// Debug monitor handler
				0,										// Reserved
				xPortPendSVHandler,                     // The PendSV handler
				xPortSysTickHandler,                    // The SysTick handler

				// Chip Level - LPC17
				WDT_IRQHandler,                     // 16, 0x40 - WDT
				TIMER0_IRQHandler,                  // 17, 0x44 - TIMER0
				TIMER1_IRQHandler,                  // 18, 0x48 - TIMER1
				TIMER2_IRQHandler,                  // 19, 0x4c - TIMER2
				TIMER3_IRQHandler,                  // 20, 0x50 - TIMER3
				UART0_IRQHandler,                   // 21, 0x54 - UART0
				UART1_IRQHandler,                   // 22, 0x58 - UART1
				UART2_IRQHandler,                   // 23, 0x5c - UART2
				UART3_IRQHandler,                   // 24, 0x60 - UART3
				PWM1_IRQHandler,                    // 25, 0x64 - PWM1
				I2C0_IRQHandler,                    // 26, 0x68 - I2C0
				I2C1_IRQHandler,                    // 27, 0x6c - I2C1
				I2C2_IRQHandler,                    // 28, 0x70 - I2C2
				default_isr,                  		// 29, Not used
				SSP0_IRQHandler,                    // 30, 0x78 - SSP0
				SSP1_IRQHandler,                    // 31, 0x7c - SSP1
				PLL0_IRQHandler,                   // 32, 0x80 - PLL0 (Main PLL)
				RTC_IRQHandler,                     // 33, 0x84 - RTC
				EINT0_IRQHandler,                   // 34, 0x88 - EINT0
				EINT1_IRQHandler,                   // 35, 0x8c - EINT1
				EINT2_IRQHandler,                   // 36, 0x90 - EINT2
				EINT3_IRQHandler,                   // 37, 0x94 - EINT3
				ADC_IRQHandler,                     // 38, 0x98 - ADC
				BOD_IRQHandler,                     // 39, 0x9c - BOD
				USB_IRQHandler,                     // 40, 0xA0 - USB
				CAN_IRQHandler,                     // 41, 0xa4 - CAN
				DMA_IRQHandler,                     // 42, 0xa8 - GP DMA
				I2S_IRQHandler,                     // 43, 0xac - I2S
				ENET_IRQHandler,                    // 44, 0xb0 - Ethernet
				MCI_IRQHandler,                    // 45, 0xb4 - SD/MMC card I/F
				MCPWM_IRQHandler,                // 46, 0xb8 - Motor Control PWM
				QEI_IRQHandler,                 // 47, 0xbc - Quadrature Encoder
				PLL1_IRQHandler,                    // 48, 0xc0 - PLL1 (USB PLL)
				USBActivity_IRQHandler, // 49, 0xc4 - USB Activity interrupt to wakeup
				CANActivity_IRQHandler, // 50, 0xc8 - CAN Activity interrupt to wakeup
				UART4_IRQHandler,                   // 51, 0xcc - UART4
				SSP2_IRQHandler,                    // 52, 0xd0 - SSP2
				LCD_IRQHandler,                     // 53, 0xd4 - LCD
				GPIO_IRQHandler,                    // 54, 0xd8 - GPIO
				PWM0_IRQHandler,                    // 55, 0xdc - PWM0
				EEPROM_IRQHandler,                  // 56, 0xe0 - EEPROM
		};

void default_isr(void)
{
	/*
	 * Dump the registers
	 */
	asm("mov r0, sp; bl dump_ctx");

	//reset
	for (;;)
	{
		_reset_s(1);
	}

}

static void dump_ctx(unsigned int *ctx)
{
	static char *regs[] =
	{
			"R0", "R1", "R2", "R3", "R12", "LR", "PC", "PSR"
	};
	static char *exc[] =
	{
			0,
			0,
			"NMI",
			"HARD FAULT",
			"MEMORY MANAGEMENT",
			"BUS FAULT",
			"USAGE FAULT",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"SVCALL",
			"DEBUG MONITOR",
			"RESERVED",
			"PENDSV",
			"SYSTICK",
	};
	unsigned char vec = cortex_m3_irq_vec_get();
	int i;

	DEBUG_P("UNHANDLED EXCEPTION: ");
	if (vec < 16)
	{
		DEBUG_P("%s\n", exc[vec]);
	}
	else
	{
		DEBUG_P("INTISR[%d]\n", vec - 16);
	}
	for (i = 0; i < 8; i++)
	{
		DEBUG_P("  %s\t= %08x", regs[i], ctx[i]);
		if (((i + 1) % 2) == 0)
		{
			DEBUG_P("\n");
		}
	}
}

static void _delay_ms(int ms)
{
	while (ms--)
	{
		volatile int i = 5000;
		while (i--)
			;
	}
}

static void _reset_s(int s)
{
	while (s--)
	{
		//DEBUG_P("\b");
		_delay_ms(1000);
		DEBUG_P("%u", s);
		_delay_ms(1000);
	}
	NVIC_SystemReset();
}

#pragma required=__vector_table
void __iar_program_start(void)
{
	SystemInit();
	__iar_init_core();
	__iar_init_vfp();
	//Init_sdram_nand();
	__cmain();
}

//#pragma call_graph_root = "interrupt"
