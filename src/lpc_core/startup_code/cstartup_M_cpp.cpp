/**************************************************
 *
 * This file contains an interrupt vector for Cortex-M written in C++.
 * Compare it to the <EWARM>\arm\src\lib\thumb\cstartup_M.c written in C.
 * The actual interrupt functions must be provided by the application developer.
 *
 * Copyright 2007 IAR Systems. All rights reserved.
 *
 * $Revision: 41481 $
 *
 **************************************************/
#include <timesync_task.hpp>
#include "sdram_nand.h"
#include "system_LPC177x_8x.h"
#include "cIRQ.hpp"
#include "timesync_task.hpp"
#include "class_cli.hpp"

#pragma language=extended
#pragma segment="CSTACK"

extern "C" void __iar_program_start(void);

class NMI
{
	public:
		static void Handler();
};

class HardFault
{
	public:
		static void Handler();
};

class MemManage
{
	public:
		static void Handler();
};

class BusFault
{
	public:
		static void Handler();
};

class UsageFault
{
	public:
		static void Handler();
};

class DebugMon
{
	public:
		static void Handler();
};

static void default_isr(void)
{
	for (;;)
	{

	}
}

typedef void (*intfunc)(void);
typedef union
{
		intfunc __fun;
		void * __ptr;
} intvec_elem;

// The vector table is normally located at address 0.
// When debugging in RAM, it can be located in RAM, aligned to at least 2^6.
// If you need to define interrupt service routines,
// make a copy of this file and include it in your project.
// The name "__vector_table" has special meaning for C-SPY:
// it is where the SP start value is found, and the NVIC vector
// table register (VTOR) is initialized to this address if != 0.
extern "C" void xPortSysTickHandler(void);
extern "C" void xPortPendSVHandler(void);
extern "C" void vPortSVCHandler(void);
extern "C" void USB_IRQHandler(void);
extern "C" void LCD_IRQHandler(void);

#pragma location = ".intvec"
extern "C" const intvec_elem __vector_table[] =
{
	{	.__ptr = __sfe( "CSTACK" )},
	__iar_program_start,

	NMI::Handler,
	HardFault::Handler,
	MemManage::Handler,
	BusFault::Handler,
	UsageFault::Handler,
	0,
	0,
	0,
	0,
	vPortSVCHandler,
	DebugMon::Handler,
	0,
	xPortPendSVHandler,
	xPortSysTickHandler,
	// Chip Level - LPC17
	default_isr,// 16, 0x40 - WDT
	TimeSyncTask::TIMER0_IRQHandler,// 17, 0x44 - TIMER0
	default_isr,// 18, 0x48 - TIMER1
	default_isr,// 19, 0x4c - TIMER2
	default_isr,// 20, 0x50 - TIMER3
	CLI::UART0_IRQHandler,// 21, 0x54 - UART0
	default_isr,// 22, 0x58 - UART1
	default_isr,// 23, 0x5c - UART2
	default_isr,// 24, 0x60 - UART3
	default_isr,// 25, 0x64 - PWM1
	default_isr,// 26, 0x68 - I2C0
	default_isr,// 27, 0x6c - I2C1
	default_isr,// 28, 0x70 - I2C2
	default_isr,// 29, Not used
	default_isr,// 30, 0x78 - SSP0
	default_isr,// 31, 0x7c - SSP1
	default_isr,// 32, 0x80 - PLL0 (Main PLL)
	default_isr,// 33, 0x84 - RTC
	default_isr,// 34, 0x88 - EINT0
	default_isr,// 35, 0x8c - EINT1
	default_isr,// 36, 0x90 - EINT2
	default_isr,// 37, 0x94 - EINT3
	default_isr,// 38, 0x98 - ADC
	default_isr,// 39, 0x9c - BOD
	USB_IRQHandler,// 40, 0xA0 - USB
	default_isr,// 41, 0xa4 - CAN
	cIRQ_Management::DMA_IRQHandler,// 42, 0xa8 - GP DMA
	default_isr,// 43, 0xac - I2S
	default_isr,// 44, 0xb0 - Ethernet
	default_isr,// 45, 0xb4 - SD/MMC card I/F
	default_isr,// 46, 0xb8 - Motor Control PWM
	default_isr,// 47, 0xbc - Quadrature Encoder
	default_isr,// 48, 0xc0 - PLL1 (USB PLL)
	default_isr,// 49, 0xc4 - USB Activity interrupt to wakeup
	default_isr,// 50, 0xc8 - CAN Activity interrupt to wakeup
	default_isr,// 51, 0xcc - UART4
	default_isr,// 52, 0xd0 - SSP2
#if MULTIBUFF_USE_ISR == 1
	LCD_IRQHandler,// 53, 0xd4 - LCD
#else
	default_isr,// 53, 0xd4 - LCD
#endif
	default_isr,// 54, 0xd8 - GPIO
	default_isr,// 55, 0xdc - PWM0
	default_isr,// 56, 0xe0 - EEPROM
};

__weak void NMI::Handler()
{
	while (1)
	{
	}
}
__weak void HardFault::Handler()
{
	while (1)
	{
	}
}
__weak void MemManage::Handler()
{
	while (1)
	{
	}
}
__weak void BusFault::Handler()
{
	while (1)
	{
	}
}
__weak void UsageFault::Handler()
{
	while (1)
	{
	}
}
__weak void DebugMon::Handler()
{
	while (1)
	{
	}
}

extern "C" void __cmain(void);
extern "C" __weak void __iar_init_core(void);
extern "C" __weak void __iar_init_vfp(void);

#pragma required=__vector_table
void __iar_program_start(void)
{
	__iar_init_core();
	__iar_init_vfp();
	SystemInit();
	Init_sdram_nand();
	__cmain();
}
