/*
 * @brief HAL USB functions for the LPC17xx microcontrollers
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#if defined(__LPC175X_6X__) || defined(__LPC177X_8X__) || defined(__LPC407X_8X__)

#include "../../../../../../LPCUSBLib/Drivers/USB/Core/HAL/HAL.h"
#include "../../../../../../LPCUSBLib/Drivers/USB/Core/USBTask.h"
#include "lpc177x_8x_pinsel.h"
#include "lpc177x_8x_clkpwr.h"

void HAL_USBInit(uint8_t corenum)
{
	CLKPWR_ConfigPPWR(CLKPWR_PCONP_PCUSB,ENABLE);
	//USB
	// Port U2
	PINSEL_ConfigPin(0,31,1);		/* USB_D+2	*/
	PINSEL_ConfigPin(0,14,3);		/* USB_CONNECT2	*/
	PINSEL_ConfigPin(0,13,1);		/* USB_UP_LED2	*/
	PINSEL_ConfigPin(0,12,1);		/* USB_PPWR2	*/
	PINSEL_ConfigPin(1,31,1);		/* USB_OVRCR2	*/

}

void HAL_USBDeInit(uint8_t corenum, uint8_t mode)
{
 //
}

void HAL_EnableUSBInterrupt(uint8_t corenum)
{
	NVIC_EnableIRQ(USB_IRQn);					/* enable USB interrupt */
}

void HAL_DisableUSBInterrupt(uint8_t corenum)
{
	NVIC_DisableIRQ(USB_IRQn);					/* enable USB interrupt */
}

void HAL_USBConnect(uint8_t corenum, uint32_t con)
{
	if (USB_CurrentMode[corenum] == USB_MODE_Device) {
#if defined(USB_CAN_BE_DEVICE)
		HAL17XX_USBConnect(con);
#endif
	}
}

// TODO moving stuff to approriate places
extern void DcdIrqHandler (uint8_t DeviceID);

void USB_IRQHandler(void)
{
	if (USB_CurrentMode[0] == USB_MODE_Host) {
		#if defined(USB_CAN_BE_HOST)
		HcdIrqHandler(0);
		#endif
	}

	if (USB_CurrentMode[0] == USB_MODE_Device) {
		#if defined(USB_CAN_BE_DEVICE)
		DcdIrqHandler(0);
		#endif
	}
}

#endif /*__LPC17XX__ || __LPC40XX__*/
